How to run a project with Jupyter Notebook
==========================================

**What is Jupyter notebook?**
This is the web application linked to the OMEGAlpes project that has
the ability to execute code from the browser, with the results of
computations attached to the code which generated them.
If you followed the standard install instructions, Jupyter is actually running on your
own computer.
It’s your computer acting as the server.

I. How to use Jupyter Notebook remotely on Binder
-------------------------------------------------

#. Go on https://mybinder.org/
#. Type the following repository URL: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks
#. And select 'Git Repository'
#. Click on 'Launch'
#. Click on the Notebooks folder
#. Select the Notebook you want (.ipynb) and run it (see II.2_ if needed)


II. How to use Jupyter Notebook locally
---------------------------------------

#. Install Jupyter  (see II.1.1_ - Anaconda - or II.1.2_ - pip -)
#. Download the following folder: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks
#. Open the command prompt
#. Install OMEGAlpes (ex: on the command prompt type 'pip install omegalpes' or have a look to the documentation
   https://omegalpes.readthedocs.io/en/latest/installation_requirements.html)
#. On the command prompt select the folder of the OMEGAlpes Notebooks
#. On the command prompt type:

            python -m notebook

   or:

            jupyter notebook

#. Click on the Notebooks folder
#. Select the Notebook you want (.ipynb) and run it (see II.2_ if needed)

.. _II.1.1:

II.1.1 Installation through Anaconda
++++++++++++++++++++++++++++++++++++

On Linux
^^^^^^^^

To install Anaconda, follow these steps: 
    #. Download Anaconda 5.2 with Python Distribution Python 3.6 version
    #. Save the folder in your "Downloads" (It is according to the language (ex: Telechargements in French))
    #. Open your command window and install it by using the following command : bash~/Downloads/Anaconda3-5.2.0-Linux-x86_64.sh
    #. Agree all the terms and say yes at the end. You can choose or agree to the saving locations for the folder.
    #. Close and open a new terminal to make the installation effective
    #. To open Jupyter notebook, use the following command : jupyter notebook

On Windows 
^^^^^^^^^^

Just download the package with the latest version of Python 3.
    1. Install  `Anaconda <https://www.anaconda.com/distribution/>`_
    2. Use the following command in Anaconda prompt:
    ``conda create --name worklab``
    and then ``activate worklab``
    3. To use a conda environment in Jupyter notebooks: ``conda install -c anaconda ipykernel``
    and then ``python -m ipykernel install --user --name=worklab``
    4. Before launching the notebook, install the relevant requirements (with the versions used in your notebook):
    ``pip install -r requirements.txt``
    5. Launch the notebook with ``jupyter notebook`` in the correct folder.
    Once the notebook launched, do not forget to select the anaconda Kernel
    with the tab *Kernel-->Change Kernel-->conda_name*.

.. _II.1.2:

II.1.2 Installation through PIP
+++++++++++++++++++++++++++++++

On Linux 
^^^^^^^^

If you have Python already installed on your computer, you can directly install Jupyter Notebook by using pip on the
terminal by using the following command : pip install.

Note that the Python versions 2.7 or 3.3 or higher are required. For upgrading your Python version, type "sudo apt-get
install python[the version you want]" on the terminal (example: "sudo apt-get install python3").

You should also know that, by installing Jupyter with this method, you will not install all the Python libraries that
are included on the Anaconda folder, you will only install Jupyter Notebook. So if later you need a library that you don’t have installed already, you will have to download and install it separately.

To install, follow these steps :

    #. To make sure that you have the latest pip version, type on the terminal the following command line: "pip3 install
       --upgrade pip" (otherwise try "python -m pip install --upgrade pip" or "python -m pip install -U pip setuptools” or
       “sudo apt install python-pip”).
    #. Then install Jupyter Notebook by typing “pip3 install jupyter” (or “pip install jupyter”) (you might need to
       reinitialize the computer after this step if the next step doesn’t work).
    #. Finally, to open Jupyter, type “jupyter notebook” on the terminal. This will start a server and Jupyter Notebook
       will pop-up on a browser, on localhost:8888.  Leave the server running on the terminal until you’re finished.

On Windows
^^^^^^^^^^

Run the following commands on the terminal:

    #. “py -m install --upgrade pip” to make sure you have the latest version of pip.
    #. “py -m pip install jupyter” to install Jupyter Notebook.
    #. “py -m notebook” to launch Jupyter from the terminal.

If  any of these commands don’t work, try replacing ‘py’ with ‘python’ or ‘python3’.


.. _II.2:

II.2 Use Jupyter Notebook locally
++++++++++++++++++++++++++++++++

For complete explanations on how to deal with Jupyter Notebook, go to the following link:
https://www.codecademy.com/articles/how-to-use-jupyter-notebooks

To run :

Choose the case example you want to work in.

.. figure::  images/presentation_dashboard.png
   :align:   center

   *Figure 1: Presentation of the dashboard*


Then, run cells after cells after completing the inputs.

.. figure::  images/howtorun.png
   :align:   center

   *Figure 2: How to run the cells*