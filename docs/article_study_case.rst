OMEGAlpes article study cases
=============================

Please have a look to the following case study:

.. contents::
    :depth: 1
    :local:
    :backlinks: top

| The code is stored at the Gitlab: `OMEGAlpes Examples`_ in the folder "article_case_study".
| Some of them have also been developed on a Jupyter notebook for a better understanding.

.. Note:: To know how to run example python codes or notebooks, see:
            `Help run Jupyter Notebook`_ or `Help run example`_

            | Consider this address to run notebooks with Binder: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes-notebooks
            | Consider this address to run the examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks


Basic example: PV self-consumption
----------------------------------
In this PV self-consuption example, a single-house with roof-integrated photovoltaic
panels (PV) is studied. Specifically, the study case is about demand-side management
in order to maximize the self-consumption, by shifting two household appliances consumption
(clothes washing machine and clothes dryer) and using a water tank for the
domestic hot water consumption.

| The article presenting this example is available at: `PV self-consumption article`_
| You can access to this example via an online Notebook at: `PV self-consumption online notebook`_
| The code is available here: `BS2019 PV self-consumption code`_
| The Notebook is available at: `PV self-consumption online notebook`_

.. figure::  images/example_PV_self_consumption.png
   :align:   center
   :scale:   55%

   *Figure 1: Principle diagram of the PV self-consumption example | Author: Camille Pajot*

| This example leads to a study with
| - 6922 variables (2890 continuous and 4032 binary)
| - 79172 non-zeros
| This optimization problem has been generated within 1.2 seconds on an Intel bicore i5 2.4 GHz CPU.
| An optimal solution was found in 43.6 seconds with the free CBC solver available in the PuLP package, and in the 2.5s with the commercial Gurobi solver.

Multi-actor modelling for MILP energy system optimisation: application to collective self consumption
-----------------------------------------------------------------------------------------------------
The shape of an energy project depends on the available technologies but also on
stakeholders’ decisions although most energy-support-decision tools only focus on
technical issues.

We aim to propose a multi-actor modelling based on actors’ objectives and constraints
and to apply it on the model generation tool for optimization OMEGAlpes. This modelling
aims to help stakeholders to formalize their constraints and objectives and to negotiate
them in a multi-actor design process.
This modelling has been applied to a simplified collective self-consumption project.

| The code is available here: `BS2019 multi-actor Modelling code`_
| The article presenting this example is available at: `BS2019 multi-actor Modelling article`_
| You can access to this example via an online Notebook at: `BS2019 multi-actor Modelling notebook`_
| The code is available here: `BS2019 multi-actor Modelling code`_

.. figure::  images/2019BS_Multi-actor_modelling.PNG
   :align:   center
   :scale:   55%

   *Figure 2: Representation of the collective self-consumption project | Lou Morriet*


Other case studies
------------------

**Camille Pajot's PhD will be available here in French:**
`OMEGAlpes Outil d'aide a la decision pour une planification energetique multi-fluides optimale à l'echelle des quartiers`_

**Please, find other scientific study cases based on OMEGAlpes :**
Pajot C., Delinchant B., Marechal Y., Wurtz F., Morriet L., Vincent B. and Debray F. (2018). `Industrial Optimal Operation Planning with Financial and Ecological Objectives`_ In Proceedings of the 7th International Conference on Smart Cities and Green ICT Systems - Volume 1: SMARTGREENS, ISBN 978-989-758-292-9, pages 214-222. DOI: 10.5220/0006705202140222

Pajot, C.; Delinchant, B.; Marechal, Y.; Fresier, D. Impact of Heat Pump Flexibility in a French Residential Eco-District. Buildings 2018, 8, 145. DOI:10.3390/buildings8100145

Camille Pajot, Quang Hung Nguyen, Benoit Delinchant, Frederic Wurtz, Yves Marechal, Stephane Robin, Benjamin Vincent, François Debray, "Databased Modeling of Building Consumption Profile for Optimal Flexibility: Application to Energy Intensive Industry", BS19, Building Simulation Conference, Roma in September 2-4, 2019.

Camille Pajot, Nils Artiges, Benoit Delinchant, Yves Marechal "Optimal Heat Pumps Operation For Demand Response of Residential Buildings At District Scale", BS19, Building Simulation Conference, Roma in September 24, 2019.


.. _OMEGAlpes Examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples
.. _PV self-consumption online notebook: https://tinyurl.com/OMEGAlpes-Basic-Example
.. _BS2019 PV self-consumption code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/article_case_study/article_2019_BS_PV_self_consumption.py
.. _PV self-consumption notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/article_2019_BS_PV_self_consumption.ipynb
.. _PV self-consumption article: http://hal.univ-grenoble-alpes.fr/hal-02285954v1
.. _BS2019 multi-actor Modelling code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/article_case_study/article_2019_BS_multi-actor_modelling.py
.. _BS2019 multi-actor Modelling article: http://hal.univ-grenoble-alpes.fr/hal-02285965v1
.. _BS2019 multi-actor Modelling notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/article_2019_BS_multi_actor_modelling.ipynb
.. _Help run Jupyter Notebook: https://omegalpes-examples.readthedocs.io/en/latest/jupyter.html
.. _Help run example: https://omegalpes-examples.readthedocs.io/en/latest/examples_run.html
.. _Industrial Optimal Operation Planning with Financial and Ecological Objectives: http://hal.univ-grenoble-alpes.fr/hal-02331208v1
.. _OMEGAlpes Outil d'aide a la decision pour une planification energetique multi-fluides optimale à l'echelle des quartiers: https://www.theses.fr/s162247