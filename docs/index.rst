Welcome to the OMEGAlpes documentation for examples and article case studies
============================================================================

**OMEGAlpes** stands for Generation of Optimization Models As Linear Programming for Energy Systems.
It is an **Open Source** energy systems modelling tool for linear optimisation (LP, MILP).

| Various examples and study cases have been developed on OMEGAlpes.
| They are stored in the following Gitlab: `OMEGAlpes Examples`_
| They are using a specified graph representation described here: `OMEGAlpes Representation`_

| **Examples** are developed to help new omegalpes users.
| **Article study cases** are developed for scientific concerns.


| To run both, you will first need to install **OMEGAlpes** library.
| To do so, please, have a look to the documentation: `OMEGAlpes Installation`_
| Or to the README.md of `OMEGAlpes Gitlab`_

| Some examples are also developed on Jupyter Notebook for a better understanding.
| To know how to run example python codes or notebooks, see run_and_notebook_help_


.. Note:: The examples may be updated with the last **developer** version which
   may be different from the OMEGAlpes **user** (Pypi) version.
   Thus, you may have to run the examples with the developer version.
   Otherwise you have to select the example version corresponding to the
   current Pypi version. The version used is indicated at the beginning
   of the example module.


**OMEGAlpes Examples and article study cases**

.. toctree::
   :maxdepth: 2

   examples

.. toctree::
   :maxdepth: 2

   article_study_case

.. _run_and_notebook_help:

**If needed, have a look to the help on:**

.. toctree::
   :maxdepth: 2

   examples_run

.. toctree::
   :maxdepth: 2

   jupyter

.. _OMEGAlpes Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes
.. _OMEGAlpes Documentation: https://omegalpes.readthedocs.io/
.. _OMEGAlpes Installation: https://omegalpes.readthedocs.io/en/stable/installation_requirements.html
.. _OMEGAlpes Examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples
.. _OMEGAlpes Representation: https://omegalpes.readthedocs.io/en/latest/OMEGAlpes_graph_representation.html