OMEGAlpes Examples
==================

Please have a look to the following examples:

.. contents::
    :depth: 1
    :local:
    :backlinks: top

| The code is stored at the Gitlab: `OMEGAlpes Examples`_ in the folder "beginner_examples" or "examples".
| Some of them have also been developed on a Jupyter notebook for a better understanding.

.. Note:: To know how to run the example python codes or the notebooks, see:
            `Help run Jupyter Notebook`_ or `Help run example`_

            | Consider this address to run notebooks with Binder: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes-notebooks
            | Consider this address to run the examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks


Basic example: PV self-consumption
----------------------------------
In this PV self-consuption example, a single-house with roof-integrated photovoltaic
panels (PV) is studied. Specifically, the study case is about demand-side management
in order to maximize the self-consumption, by shifting two household appliances consumption
(clothes washing machine and clothes dryer) and using a water tank for the
domestic hot water consumption.

| The article presenting this example is available at: `PV self-consumption article`_
| You can access to this example via an online Notebook at: `PV self-consumption online notebook`_
| The code is available here: `PV self-consumption code`_
| The Notebook is available at: `PV self-consumption notebook`_

.. figure::  images/example_PV_self_consumption.png
   :align:   center
   :scale:   55%

   *Figure 1: Principle diagram of the PV self-consumption example | Author: Camille Pajot*

This example leads to a study with :
    - 6922 variables (2890 continuous and 4032 binary)
    - 79172 non-zeros

This optimization problem has been generated within 1.2 seconds on an Intel bicore i5 2.4 GHz CPU.

An optimal solution was found in 43.6 seconds with the free CBC solver available in the PuLP package, and in the 2.5s with the commercial Gurobi solver.

Electrical system operation
---------------------------
This first module is an example of decision support for electrical system operations.
The electrical system operator needs to decide whether to provide electricity from the grid_production A or B
depending on their operating costs. The two grid productions are providing energy to a dwelling with a fixed
electricity consumption profile.

| The code is available here: `Electrical system operation code`_
| The Notebook is available at: `Electrical system operation notebook`_

.. figure::  images/systemoperation.png
   :align:   center
   :scale:   80%

   *Figure 2: Principle diagram of the electrical system operation example | Author: Lou Morriet*

Storage design
--------------
The storage_design module is an example of storage capacity optimization. A production unit and a storage system power a
load with a fixed consumption profile. The production unit has a maximum power value and the storage system has maximum
charging and discharging power values. The objective is to minimize the capacity of the storage system while meeting
the load during the whole time horizon.

| The code is available here: `Storage design code`_
| The Notebook is available at: `Storage design notebook`_

.. figure::  images/schema_stockage.png
   :align:   center

   *Figure 3: principle diagram of the storage design example | Author: Sacha Hodencq*

Waste heat recovery
-------------------
In the waste_heat_recovery module, an electro-intensive industrial process consumes electricity and
rejects heat. This waste heat is recovered by a system composed of a heat pump in order to increase
the heat temperature, and a thermal storage that is used to recover more energy and have a more
constant use of the heat pump. This way, the waste heat is whether recovered or dissipated depending
on the waste heat recovery system sizing. The heat is then injected on a district heat network to
provide energy to a district heat load. A production unit of the district heat network provides the extra
heat.

| The code is available here: `Waste heat recovery code`_
| The Notebook is available at: `Waste heat recovery notebook`_

.. figure::  images/wasteHeatRecovery.PNG
   :align:   center

   *Figure 4: principle diagram of the waste heat recovery example | Author : Camille Pajot*

Technical and decision constraints and objectives can be added to the project. This leads to the following
Figure 5.

.. figure::  images/wasteHeatRecovery_withConstraints.PNG
   :align:   center
   :scale:   80%

   *Figure 6: principle diagram of the waste heat recovery example with constraints | Author: Camille Pajot*

Applying, multi-stakeholder vision on the waste heat recovery project leads to the Figure 6.
One central point is the governance of the storage and heat pump. Who's financing it? which actor
will operate it? This governance needs to be discuss and mutually agreed to be able to go further on the project.

.. figure::  images/wasteHeatRecovery_multiStakeholders.PNG
   :align:   center
   :scale:   80%

   *Figure 6: principle diagram of the waste heat recovery example with multi-stakeholder vision | Author: Lou Morriet from Camille Pajot work*


A technical optimisation over one year on a hourly time step can lead to a study with
    - 228k variables (158k continuous et 70k binaires)
    - 316k constraints
It has been solved in 13h with Gurobi, which can be considered as correct considering the high number of variables
and constraints.

Considering the 20MWh / 6.7MW storage this can of study can calculate that
60% of the annual needs could be covered by the LNCMI waste heat
(which corresponds to 60% reduction in CO2 emissions)
This outputs should be consider **regarding the constraints and objectives of the model**,
which are not totally detailed here, as the goal of this part is to show the possibilities of OMEGAlpes.

Graphics like the following one can also be produced:

.. figure::  images/wasteHeatRecovery_study.PNG
   :align:   center
   :scale:   80%

   *Figure 7: heat provider of the district over a year | Author: Camille Pajot*


Various studies could be carried out:
    - Balancing between CO2 emissions from the LNCMI and district heating, free profile
    - Using HP according to the electricity price, typical profiles
    - Study of operational performances under constraints, fixed profile



.. _OMEGAlpes Examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/tree/master
.. _PV self-consumption online notebook: https://tinyurl.com/OMEGAlpes-Basic-Example
.. _PV self-consumption notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/article_2019_BS_PV_self_consumption.ipynb
.. _PV self-consumption code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/beginner_examples/PV_self_consumption.py
.. _PV self-consumption article: http://hal.univ-grenoble-alpes.fr/hal-02285954v1
.. _Electrical system operation code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/beginner_examples/electrical_system_operation.py
.. _Electrical system operation notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/electrical_system_operation.ipynb
.. _Storage design code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/beginner_examples/storage_design.py
.. _Storage design notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/storage_design.ipynb
.. _Waste heat recovery code: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/blob/master/beginner_examples/waste_heat_recovery.py
.. _Waste heat recovery notebook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_notebooks/blob/master/notebooks/waste_heat_recovery.ipynb
.. _NoteBook: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes-notebooks
.. _Help run Jupyter Notebook: https://omegalpes-examples.readthedocs.io/en/latest/jupyter.html
.. _Help run example: https://omegalpes-examples.readthedocs.io/en/latest/examples_run.html
