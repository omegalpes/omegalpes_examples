How to run an example
=====================

You first need to download (or clone) the OMEGAlpes Examples folder (repository) at :
`OMEGAlpes Examples`_.
In fact, it is better to download the whole folder as most of the examples
or article case studies use data located outside the code file.
Thus they need to be download as well.

Then, open your development environment, select the example file you want (.py)
and run it.

| **Do not forget:**
| To run your example, you first need to install **OMEGAlpes** library.
| To do so, please, have a look to the documentation: `OMEGAlpes Installation`_
| Or to the README.md of `OMEGAlpes Gitlab`_

.. _OMEGAlpes Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes
.. _OMEGAlpes Installation: https://omegalpes.readthedocs.io/en/stable/installation_requirements.html
.. _OMEGAlpes Examples: https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples