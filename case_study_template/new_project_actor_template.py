#! usr/bin/env python3
#  coding=utf-8 #

"""
    ** This Module is a draft in order to help modeling new projects

"""

import os
from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.actor.operator_actors.consumer_producer_actors import \
    Consumer, Producer, Supplier, Prosumer
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_quantity, \
    plot_node_energetic_flows
from omegalpes.general.time import TimeUnit

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)  # to adapt
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------

def main(work_path):
    # # Create an empty model # #
    time = TimeUnit(periods=24, dt=1)  # here study on a day (24h), dt= 1h
    model = OptimisationModel(time=time, name='study_case_name')

    # # Create the load - The load profile is known (1) or (2) # #
    # load_profile = open(file_name, "r")  # (1) from a file
    # load_profile = [4, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, 20, 15,
    #                 17, 21, 25, 23, 18, 16, 13, 4]  # (2) to write
    # load = FixedConsumptionUnit(time=time, name='load', p=load_profile)

    # # Create the load - The load profile is unknown # #
    # load = VariableConsumptionUnit(time, 'load')

    # # Create the production unit - The production profile is unknown # #
    # production = VariableProductionUnit(time=time, name='production',
    #                                     p_max=100000)

    # # Create the production unit - production profile is known (1) or (2) # #
    # prod_profile = open(file_name, "r")  # (1) from a file
    # prod_profile = [3, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, 20, 15,
    #                 17, 21, 25, 23, 18, 16, 13, 4]  # (2) to write
    # production = FixedProductionUnit(time=time, name='production',
    #                                     p=prod_profile)

    # # Create the energy node and connect units # #
    node = EnergyNode(time=time, name='energy_node')
    node.connect_units(load, production)

    # # Create the actors # #
    consumer = Consumer(name='consumer', operated_unit_list=[load])
    producer = Producer(name='producer', operated_unit_list=[production])
    # producer.energy_production_maximum(max_e_tot=23) # ! Warning, constraints
    # may entail an unfeasible problem !

    # # Add the energy node to the model # #
    model.add_nodes_and_actors(node, consumer,
                               producer)  # Add node to the model

    # Optimisation process
    model.writeLP(work_path + r'\optim_models\study_case_name.lp')
    model.solve_and_update()

    return model, time, load, production, node


def print_results():
    """
        This function prints the optimisation result:
    """

    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        print("the total consumption is {} Wh".format(LOAD.e_tot))
        print("the total production is {} Wh".format(PRODUCTION.e_tot))
        # print("blabla {}".format(accurate_variable))

        # # Show the graph # #
        # Power curves
        plot_node_energetic_flows(NODE)
        plt.show()

        # # Plot the figures # #
        # fig1 = plt.figure(1)
        # ax1 = plt.axes()
        # legend1 = []
        #
        # plot_quantity(TIME, LOAD.p, fig1, ax1)
        # legend1 += ['Load']
        #
        # plot_quantity(TIME, PRODUCTION.p, fig1, ax1)
        # legend1 += ['production']
        #
        # plt.legend(legend1)
        # plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == '__main__':
    # OPTIMIZATION PARAMETERS #
    WORK_PATH = os.getcwd()

    # Run main
    MODEL, TIME, LOAD, PRODUCTION, NODE = \
        main(work_path=WORK_PATH)

    # Show results
    print_results()
