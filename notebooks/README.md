# OMEGAlpes Notebooks
This repository gathers OMEGAlpes notebooks. 

Three kinds of notebooks can be differentiated:
1. Notebooks linked to articles, that detail the context 
   - article_2019_BS_multi_actor_modelling
   - article_2019_BS_PV_self_consumption
   - article_2021_ORUCE
   - article_2021_MPDI_waste_heat   
2. Notebooks linked to the tutorial
   - To_Modify__PV_self_consumption_eng
3. Beginner examples
   - electrical_system_operation
   - storage_design
   - waste_heat_recovery
4. Notebooks used for interacting with energy stakeholders
   - oniri_exp_fr
   - LNCMI

You can run OMEGAlpes notebooks:

A. remotely on Binder  
    1. Follow the Binder link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fomegalpes%2Fomegalpes_examples/master "Link to the OMEGAlpes examples repository through mybinder")  
    2. Click on the Notebooks folder   
    3. Select the Notebook you want (.ipynb) and run it  
    

B. locally thanks to Jupyter Notebook  
    1. install Jupyter  
    2. Download the following folder: 
    https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples   
    3. Open the command prompt  
    4. Install OMEGAlpes (ex: on the command prompt type 'pip install omegalpes'
    or have a look at [the documentation](https://omegalpes.readthedocs.io/en/latest/installation_requirements.html "OMEGAlpes documentation - installation requirements")  
    5. On the command prompt select the folder of the OMEGAlpes Examples   
    6. On the command prompt type: python -m notebook  
    7. Click on the notebooks folder    
    8. Select the Notebook you want (.ipynb) and run it

C. The latest Jupyter Notebooks include a direct link to mybinder for the specific use of the notebook, by clicking on the mybinder icon at the top of the notebook.
