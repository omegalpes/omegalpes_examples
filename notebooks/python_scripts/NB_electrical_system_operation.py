#! usr/bin/env python3
#  -*- coding: utf-8 -*-

import os
import sys
sys.path.append('../')  # in order for the path to be at the
# omegalpes_examples folder level, to get various_examples.
from omegalpes_examples.various_examples.beginner_examples.electrical_system_operation import \
    elec_syst_op, print_results_elec
from ipywidgets import widgets, Layout, Button, Box, Textarea, \
    Label, HTML
from IPython.display import clear_output

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def dashboard():  # elec_syst_op, print_results_elec
    def updateData(click):
        clear_output()
        i = 1

        CONSUMPTION_PROFILE = form.children[i].children[1].value
        i += 1

        OPERATING_COSTS_A_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        OPERATING_COSTS_A = [float(s) for s in OPERATING_COSTS_A_str]
        i += 1

        OPERATING_COSTS_B_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        OPERATING_COSTS_B = [float(s) for s in OPERATING_COSTS_B_str]
        i += 1

        global MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A,\
            GRID_PRODUCTION_B

        MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A, \
        GRID_PRODUCTION_B = elec_syst_op(work_path=os.getcwd(),
                                         op_cost_a=OPERATING_COSTS_A,
                                         op_cost_b=OPERATING_COSTS_B,
                                         consumption_file=CONSUMPTION_PROFILE,
                                         write_lp=False)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    def runplot(click):
        clear_output()
        print_results_elec(MODEL, TIME, DWELLING_CONSUMPTION,
                           GRID_PRODUCTION_A, GRID_PRODUCTION_B)
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between',
        width='100%'
    )

    form_items = [
        Box([HTML(value=('Here you update the inputs. Then, press '
                         '<b>Update</b> to run the optimisation.\
                 If the there is a solution you will be able to <b>Plot '
                         'results</b>.\
                  In case of no solution, it will show a warning.'),
                  placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(
            value='File with hourly data for the consumption profile during '
                  'the day'),
            Textarea(placeholder='./data/Building_consumption_day.txt',
                     value='./data/Building_consumption_day.txt')],
            layout=form_item_layout, description_width='initial'),
        Box([Label(
            value='Hourly operating costs for the production unit A (0h '
                  'to 23h) separated by ","'),
            Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                     value='41.1, 41.295, 43.125, 51.96, 58.275, 62.955, '
                           '58.08,\
57.705, 59.94, 52.8, 53.865, 46.545, 41.4, 39,\
36.87, 36.6, 39.15, 43.71, 45.195, 47.04, 44.28,\
39.975, 34.815, 28.38', layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(
            value='Hourly operating costs for the production unit B (0h '
                  'to 23h) separated by ","'),
            Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                     value='58.82, 58.23, 51.95, 47.27, 45.49, 44.5, 44.5,\
 44.72, 44.22, 42.06, 45.7, 47.91, 49.57, 48.69,\
 46.91, 46.51, 46.52, 51.59, 59.07, 62.1, 56.26, 55,\
 56.02, 52', layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Button(
            description='Update',
            disabled=False,
            button_style='info',
            # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
