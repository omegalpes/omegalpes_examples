#! usr/bin/env python3
#  -*- coding: utf-8 -*-

import os
import sys

sys.path.append('../')  # in order for the path to be at the
# omegalpes_examples folder level, to get various_examples.
from omegalpes_examples.various_examples.beginner_examples.waste_heat_recovery import \
    waste_heat_rec, print_results_waste_heat

from ipywidgets import widgets, Layout, Button, Box, Label, IntSlider, \
    BoundedIntText, HTML
from IPython.display import clear_output

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------

def dashboard():
    def runplot(click):
        clear_output()
        print_results_waste_heat(MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD,
                                 HEAT_PUMP, HEAT_PRODUCTION,
                                 THERMAL_STORAGE, HEAT_NODE_BEF_VALVE,
                                 HEAT_NODE_AFT_VALVE, HEAT_NODE_AFT_HP)
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1
        ELEC_TO_THERM_RATIO = form.children[i].children[1].value / 100
        i += 1
        PC_MAX_STORAGE = PD_MAX_STORAGE = form.children[i].children[1].value
        i += 1
        PC_MIN_STORAGE = PD_MIN_STORAGE = \
            form.children[i].children[1].value * PC_MAX_STORAGE / 100
        i += 1
        CAPA_STORAGE = form.children[i].children[1].value
        i += 1
        # Storage capacity of 20MWh
        SOC_0_STORAGE = form.children[i].children[1].value / 100
        i += 1  # Initial state of charge of 25%
        COP = form.children[i].children[1].value
        i += 1  # The coefficient of performance equals 3
        P_MAX_HP = form.children[i].children[1].value
        i += 1  # The heat pump has a electrical power limit of 1 MW
        global MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD, HEAT_PUMP, \
            HEAT_PRODUCTION, DISSIPATION, THERMAL_STORAGE, HEAT_NODE_BEF_VALVE,\
            HEAT_NODE_AFT_VALVE, HEAT_NODE_AFT_HP

        MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD, HEAT_PUMP, HEAT_PRODUCTION, \
        DISSIPATION, THERMAL_STORAGE, HEAT_NODE_BEF_VALVE, HEAT_NODE_AFT_VALVE, \
        HEAT_NODE_AFT_HP = waste_heat_rec(work_path=os.getcwd(),
                                          elec2therm_ratio=ELEC_TO_THERM_RATIO,
                                          pc_max=PC_MAX_STORAGE,
                                          pd_max=PD_MAX_STORAGE,
                                          pc_min=PC_MIN_STORAGE,
                                          pd_min=PD_MIN_STORAGE,
                                          capa=CAPA_STORAGE, cop_hp=COP,
                                          pmax_elec_hp=P_MAX_HP,
                                          storage_soc_0=SOC_0_STORAGE,
                                          write_lp=False)
        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between'
    )

    form_items = [
        Box([HTML(
            value="Here you update the inputs. Then, press <b>Update</b> to "
                  "run the optimisation. If the there is a solution you will "
                  "be able to <b>Plot results</b>. In case of no sulition, "
                  "it will show a warning.",
            placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(value='Electrical-heat convertion ratio (%)'),
             IntSlider(min=0, max=100, step=1, value=85)],
            layout=form_item_layout),
        Box([Label(value='Maximal charging and discharging powers (kW)'),
             BoundedIntText(min=1000, max=100000, step=100, value=6700)],
            layout=form_item_layout, width='1'),
        Box([Label(value='Minimal charging and discharging powers (% of max)'),
             IntSlider(min=0, max=20, step=1, value=10)],
            layout=form_item_layout),
        Box([Label(value='Storage capacity  (kWh)'),
             BoundedIntText(min=1000, max=100000, step=1000, value=20000)],
            layout=form_item_layout, width='1'),
        Box([Label(value='Initial state of charge (%)'),
             IntSlider(min=0, max=100, step=1, value=20)],
            layout=form_item_layout),
        Box([Label(value='The coefficient of performance'),
             BoundedIntText(min=0, max=10, step=1, value=3)],
            layout=form_item_layout),
        Box([Label(value='Pump electrical power limit  (kW)'),
             BoundedIntText(min=0, max=10000, step=10, value=1260)],
            layout=form_item_layout, width='1'),
        Button(
            description='Update',
            disabled=False,
            button_style='info',
            # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        #  align_items='stretch',
        width='70%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
