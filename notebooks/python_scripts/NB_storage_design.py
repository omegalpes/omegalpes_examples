#! usr/bin/env python3
#  -*- coding: utf-8 -*-

# imports for storage design
import os
import sys

sys.path.append('../')  # in order for the path to be at the
# omegalpes_examples folder level, to get various_examples.
from omegalpes_examples.various_examples.beginner_examples import \
    print_results_storage
from omegalpes_examples.various_examples.beginner_examples.storage_design import \
    storage_design
from ipywidgets import widgets, Layout, Button, Box, Textarea, \
    Label, IntSlider, HTML
from IPython.display import clear_output

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def dashboard():
    def runplot(click):
        clear_output()
        print_results_storage(MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE)
        display(form)
        form.children[len(form_items) - 1].on_click(updateData)

    def updateData(click):
        clear_output()
        i = 1

        LOAD_PROFILE_str = form.children[i].children[1].value.split(",")
        str = form.children[i].children[1].value
        LOAD_PROFILE = [float(s) for s in LOAD_PROFILE_str]
        i += 1

        PRODUCTION_P_MAX = form.children[i].children[1].value
        i += 1

        STORAGE_PC_MAX = form.children[i].children[1].value
        i += 1

        STORAGE_PD_MAX = form.children[i].children[1].value
        i += 1

        global MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE

        MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE = \
            storage_design(work_path=os.getcwd(), load_profile=LOAD_PROFILE,
                           production_pmax=PRODUCTION_P_MAX,
                           storage_pcharge_max=STORAGE_PC_MAX,
                           storage_pdischarge_max=STORAGE_PD_MAX,
                           write_lp=False)

        display(runPlotButton)
        runPlotButton.on_click(runplot)

    form_item_layout = Layout(
        display='flex',
        flex_flow='row',
        justify_content='space-between'
    )

    form_items = [
        Box([HTML(value=('Here you update the inputs. Then, press '
                         '<b>Update</b> to run the optimisation.\
                 If the there is a solution you will be able to <b>Plot '
                         'results</b>.\
                  In case of no solution, it will show a warning.'),
                  placeholder='', description='')],
            layout=Layout(width='auto', grid_area='header')),
        Box([Label(
            value='Load dynamic profile - one value per hour during a day (0h '
                  'to 23h) separated by ", "'),
             Textarea(placeholder='00h, 01h, 02h, 03h, 04h, ..., 23h',
                      value='4, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, '
                            '20, 15, 17, 21, 25, 23, 18, 16, 13, 4',
                      layout=Layout(width='40%', height='100px'))],
            layout=form_item_layout),
        Box([Label(value='Maximal power deliveried (KW)'),
             IntSlider(min=0, max=50, step=1, value=15)],
            layout=form_item_layout),
        Box([Label(value='Maximal charging power (KW)'),
             IntSlider(min=0, max=50, step=1, value=20)],
            layout=form_item_layout),
        Box([Label(value='Maximal discharging power (KW)'),
             IntSlider(min=0, max=50, step=1, value=20)],
            layout=form_item_layout),
        Button(
            description='Update',
            disabled=False,
            button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

    ]

    form = Box(form_items, layout=Layout(
        display='flex',
        flex_flow='column',
        border='solid 2px',
        width='100%'
    ))

    runPlotButton = widgets.Button(
        description='Plot',
        disabled=False,
        button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Click me',
        icon='check')

    display(form)
    form.children[len(form_items) - 1].on_click(updateData)
