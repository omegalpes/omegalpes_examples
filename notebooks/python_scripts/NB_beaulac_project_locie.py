#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.units.conversion_units import *
from omegalpes.energy.units.storage_units import *
from omegalpes.energy.energy_nodes import *
from omegalpes.energy.exergy import *
from omegalpes.general.time import *
from omegalpes.general.optimisation.model import *
from omegalpes.general.utils.plots import *
from omegalpes.general.utils.output_data import save_energy_flows
from pulp import LpStatus, GUROBI_CMD
import time as timer
# from tabulate import tabulate

def beaulac_project_init(work_path, time, exergy_analysis=False, time_lapse=None,
          temp_dead_state=20,
         pv_area=1, pv_efficiency=0, st_area=1, st_efficiency=0,
         st_temp_out=100, elec_batt_pc_max=1, elec_batt_pd_max=1,
         elec_batt_capa=1, elec_batt_soc_min=1, elec_batt_soc_max=1,
         elec_batt_self_disch=1, elec_batt_soc_ini=1, therm_stor_pc_max=1,
         therm_stor_pd_max=1, therm_stor_capa=1, therm_stor_soc_min=1,
         therm_stor_soc_max=1, therm_stor_self_disch=1, therm_stor_soc_ini=1,
         therm_stor_temp=65, p_max_lake=100, conduit_max_cooling=100,
         hp_heating_cop=2, hp_heating_pmax_elec=1, hp_heating_temp_in=25,
         hp_heating_temp_out=50, hp_dhw_cop=2, hp_dhw_pmax_elec=1,
         hp_dhw_temp_in=25, hp_dhw_temp_out=50, hp_cooling_cop=2,
         hp_cooling_pmax_elec=1, hp_cooling_temp_in=25, hp_cooling_temp_out=50,
         fluid_pumps_exergy_eff=1, high_heat_diss_temp=70,
         medium_heat_diss_temp=35, low_heat_diss_temp=15,
         heat_diss_exergy_eff=0):
    if time_lapse == 24:
        hameau_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_dhw_cons_1_day.txt", "r")
        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]
        hameau_elec_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_elec_cons_1_day.txt", "r")
        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]
        hameau_heating_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_heating_cons_1_day.txt", "r")
        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]
        ines_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/ines_cooling_cons_1_day.txt", "r")
        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]
        solar_irradiation_file = open(
            work_path + "/data/beaulac_data_profiles/solar_irradiation_1_day.txt", "r")
        solar_pv_input = [c * pv_area for c in map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(
            work_path + "/data/beaulac_data_profiles/solar_irradiation_1_day.txt", "r")
        solar_thermal_input = [c * st_area for c in map(float, solar_irradiation_file2)]
        zac_2_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_cooling_cons_1_day.txt", "r")
        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]
        zac_2_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_dhw_cons_1_day.txt", "r")
        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]
        zac_2_elec_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_elec_cons_1_day.txt", "r")
        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]
        zac_2_heating_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_heating_cons_1_day.txt", "r")
        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]
        zac_3_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_cooling_cons_1_day.txt", "r")
        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]
        zac_3_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_dhw_cons_1_day.txt", "r")
        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]
        zac_3_elec_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_elec_cons_1_day.txt", "r")
        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]
        zac_3_heating_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_heating_cons_1_day.txt", "r")
        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]
    elif time_lapse == 8760:
        hameau_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_dhw_cons_1_year.txt", "r")
        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]
        hameau_elec_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_elec_cons_1_year.txt", "r")
        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]
        hameau_heating_file = open(
            work_path + "/data/beaulac_data_profiles/hameau_heating_cons_1_year.txt", "r")
        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]
        ines_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/ines_cooling_cons_1_year.txt", "r")
        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]
        solar_irradiation_file = open(
            work_path + "/data/beaulac_data_profiles/solar_irradiation_1_year.txt", "r")
        solar_pv_input = [c * pv_area for c in map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(
            work_path + "/data/beaulac_data_profiles/solar_irradiation_1_year.txt", "r")
        solar_thermal_input = [c * st_area for c in map(float, solar_irradiation_file2)]
        zac_2_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_cooling_cons_1_year.txt", "r")
        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]
        zac_2_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_dhw_cons_1_year.txt", "r")
        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]
        zac_2_elec_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_elec_cons_1_year.txt", "r")
        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]
        zac_2_heating_file = open(
            work_path + "/data/beaulac_data_profiles/zac_2_heating_cons_1_year.txt", "r")
        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]
        zac_3_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_cooling_cons_1_year.txt", "r")
        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]
        zac_3_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_dhw_cons_1_year.txt", "r")
        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]
        zac_3_elec_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_elec_cons_1_year.txt", "r")
        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]
        zac_3_heating_file = open(
            work_path + "/data/beaulac_data_profiles/zac_3_heating_cons_1_year.txt", "r")
        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]

    lake = VariableProductionUnit(
        time, name='lake', energy_type='Thermal', p_max=p_max_lake)
    buy_elec_from_grid = VariableProductionUnit(
        time, name='buy_elec_from_grid', energy_type='Electrical')
    sell_elec_to_grid = VariableConsumptionUnit(
        time, name='sell_elec_to_grid', energy_type='Electrical')
    zac_2_elec = FixedConsumptionUnit(
        time, name='zac_2_elec', p=zac_2_elec_cons, energy_type='Electrical')
    zac_3_elec = FixedConsumptionUnit(
        time, name='zac_3_elec', p=zac_3_elec_cons, energy_type='Electrical')
    hameau_elec = FixedConsumptionUnit(
        time, name='hameau_elec', p=hameau_elec_cons, energy_type='Electrical')
    conduit_hydraulics = FixedConsumptionUnit(
        time, name='conduit_hydraulics', p=[0] * time_lapse, energy_type='Electrical')

    zac_2_dhw = FixedConsumptionUnit(
        time, name='zac_2_dhw', p=zac_2_dhw_cons, energy_type='Thermal')
    zac_3_dhw = FixedConsumptionUnit(
        time, name='zac_3_dhw', p=zac_3_dhw_cons, energy_type='Thermal')
    hameau_dhw = FixedConsumptionUnit(
        time, name='hameau_dhw', p=hameau_dhw_cons, energy_type='Thermal')

    zac_2_heating = FixedConsumptionUnit(
        time, name='zac_2_heating', p=zac_2_heating_cons, energy_type='Thermal')
    zac_3_heating = FixedConsumptionUnit(
        time, name='zac_3_heating', p=zac_3_heating_cons, energy_type='Thermal')
    hameau_heating = FixedConsumptionUnit(
        time, name='hameau_heating', p=hameau_heating_cons, energy_type='Thermal')

    zac_2_cooling = FixedProductionUnit(
        time, name='zac_2_cooling', p=zac_2_cooling_prod, energy_type='Thermal')
    zac_3_cooling = FixedProductionUnit(
        time, name='zac_3_cooling', p=zac_3_cooling_prod, energy_type='Thermal')
    ines_cooling = FixedProductionUnit(
        time, name='ines_cooling', p=ines_cooling_prod, energy_type='Thermal')
    solar_irradiation_pv = FixedProductionUnit(
        time, name='solar_irradiation_pv', p=solar_pv_input, energy_type='Electrical')

    pv_panels = ElectricalConversionUnit(
        time, name='pv_panels', elec_to_elec_ratio=pv_efficiency)

    solar_irradiation_st = FixedProductionUnit(
        time, name='solar_irradiation_st', p=solar_thermal_input, energy_type='Electrical')

    solar_thermal_collectors = ElectricalToThermalConversionUnit(
        time, name='solar_thermal_collectors', elec_to_therm_ratio=st_efficiency)
    elec_batteries = StorageUnit(time, name='elec_batteries',
                                 pc_max=elec_batt_pc_max,
                                 pc_min = 0,
                                 pd_max=elec_batt_pd_max,
                                 pd_min = 0,
                                 capacity=elec_batt_capa,
                                 soc_min=elec_batt_soc_min,
                                 soc_max=elec_batt_soc_max,
                                 self_disch_t=elec_batt_self_disch,
                                 ef_is_e0=True,
                                 energy_type='Electrical')

    thermal_storage = StorageUnit(time, name='thermal_storage',
                                  pc_max=therm_stor_pc_max,
                                 pc_min = 0,
                                  pd_max=therm_stor_pd_max,
                                 pd_min = 0,
                                  capacity=therm_stor_capa,
                                  soc_min=therm_stor_soc_min,
                                  soc_max=therm_stor_soc_max,
                                  self_disch_t=therm_stor_self_disch,
                                  ef_is_e0=True,
                                  energy_type='Thermal')
    high_temp_heat_dissipation = VariableConsumptionUnit(
        time, p_min = 0, name='high_temp_heat_dissipation',
        energy_type='Thermal')

    medium_temp_heat_dissipation = VariableConsumptionUnit(
        time, p_min = 0, name='medium_temp_heat_dissipation', energy_type='Thermal')

    low_temp_heat_dissipation = VariableConsumptionUnit(
        time, p_min = 0, name='low_temp_heat_dissipation', energy_type='Thermal')
    heat_pump_heating = HeatPump(time, name='heat_pump_heating',
                                 pmin_in_elec = 0,
                                 pmin_in_therm = 0,
                                 pmin_out_therm = 0,
                                 cop=hp_heating_cop,
                                 pmax_in_elec=hp_heating_pmax_elec)

    heat_pump_dhw = HeatPump(time, name='heat_pump_dhw',
                             cop=hp_dhw_cop,
                                 pmin_in_elec = 0,
                                 pmin_in_therm = 0,
                                 pmin_out_therm = 0, pmax_in_elec=hp_dhw_pmax_elec)

    heat_pump_cooling = HeatPump(time, name='heat_pump_cooling',
                                 cop = hp_cooling_cop,
                                 pmin_in_elec = 0,
                                 pmin_in_therm = 0,
                                 pmin_out_therm = 0,
                                 pmax_in_elec=hp_cooling_pmax_elec)

    imaginary_elec_prod_unit = VariableProductionUnit(time,
                                                      p_min=0,
    name='imaginary_elec_prod_unit', energy_type='Electrical')

    from_high_temp_to_medium_temp = HeatPump(time,
                                 pmin_in_elec = 0,
                                 pmin_in_therm = 0,
                                 pmin_out_therm = 0,
                                             name='from_high_temp_to_medium_temp', cop=1e10, pmax_out_therm=1e4)

    return lake, buy_elec_from_grid, sell_elec_to_grid, zac_2_elec, zac_3_elec, hameau_elec, conduit_hydraulics, zac_2_dhw, \
           zac_3_dhw, hameau_dhw, zac_2_heating, zac_3_heating, hameau_heating, solar_irradiation_pv, solar_irradiation_st, zac_2_cooling, \
           zac_3_cooling, ines_cooling, elec_batteries, high_temp_heat_dissipation, medium_temp_heat_dissipation, low_temp_heat_dissipation, \
           thermal_storage, pv_panels, solar_thermal_collectors, heat_pump_dhw, heat_pump_heating, heat_pump_cooling, from_high_temp_to_medium_temp, \
           imaginary_elec_prod_unit  # hp_dhw_elec_inlet_node, hp_heating_elec_inlet_node, hp_cooling_elec_inlet_node, from_cooling_to_conduit,

def beaulac_project_solving(work_path, time, objective_function = None,
                            time_lapse=None,
                            temp_dead_state=20,
                            pv_area=1, pv_efficiency=0, st_area=1, st_efficiency=0,
                            st_temp_out=100, elec_batt_pc_max=1, elec_batt_pd_max=1,
                            elec_batt_capa=1, elec_batt_soc_min=1, elec_batt_soc_max=1,
                            elec_batt_self_disch=1, elec_batt_soc_ini=1, therm_stor_pc_max=1,
                            therm_stor_pd_max=1, therm_stor_capa=1, therm_stor_soc_min=1,
                            therm_stor_soc_max=1, therm_stor_self_disch=1, therm_stor_soc_ini=1,
                            therm_stor_temp=65, p_max_lake=100, conduit_max_cooling=100,
                            hp_heating_cop=2, hp_heating_pmax_elec=1, hp_heating_temp_in=25,
                            hp_heating_temp_out=50, hp_dhw_cop=2, hp_dhw_pmax_elec=1,
                            hp_dhw_temp_in=25, hp_dhw_temp_out=50, hp_cooling_cop=2,
                            hp_cooling_pmax_elec=1, hp_cooling_temp_in=25, hp_cooling_temp_out=50,
                            fluid_pumps_exergy_eff=1, high_heat_diss_temp=70,
                            medium_heat_diss_temp=35, low_heat_diss_temp=15,
                            heat_diss_exergy_eff=0):

    lake, buy_elec_from_grid, sell_elec_to_grid, zac_2_elec, zac_3_elec, hameau_elec, conduit_hydraulics, zac_2_dhw, \
    zac_3_dhw, hameau_dhw, zac_2_heating, zac_3_heating, hameau_heating, solar_irradiation_pv, solar_irradiation_st, zac_2_cooling, \
    zac_3_cooling, ines_cooling, elec_batteries, high_temp_heat_dissipation, medium_temp_heat_dissipation, low_temp_heat_dissipation, \
    thermal_storage, pv_panels, solar_thermal_collectors, \
    heat_pump_dhw, heat_pump_heating, heat_pump_cooling, from_high_temp_to_medium_temp, imaginary_elec_prod_unit = beaulac_project_init(work_path, time = time,
                                                                                                                                        exergy_analysis=False, time_lapse=time_lapse,
                                                                                                                                        temp_dead_state=20,
         pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area, st_efficiency=st_efficiency,
         st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max, elec_batt_pd_max=elec_batt_pd_max,
         elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min, elec_batt_soc_max=elec_batt_soc_max,
         elec_batt_self_disch=elec_batt_self_disch, elec_batt_soc_ini=elec_batt_soc_ini, therm_stor_pc_max=therm_stor_pc_max,
         therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa, therm_stor_soc_min=therm_stor_soc_min,
         therm_stor_soc_max=therm_stor_soc_max, therm_stor_self_disch=therm_stor_self_disch, therm_stor_soc_ini=therm_stor_soc_ini,
         therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake, conduit_max_cooling=conduit_max_cooling,
         hp_heating_cop=hp_heating_cop, hp_heating_pmax_elec=hp_heating_pmax_elec, hp_heating_temp_in=hp_heating_temp_in,
         hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop, hp_dhw_pmax_elec=hp_dhw_pmax_elec,
         hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out, hp_cooling_cop=hp_cooling_cop,
         hp_cooling_pmax_elec=hp_cooling_pmax_elec, hp_cooling_temp_in=hp_cooling_temp_in, hp_cooling_temp_out=hp_cooling_temp_out,
         fluid_pumps_exergy_eff=fluid_pumps_exergy_eff, high_heat_diss_temp=high_heat_diss_temp,
         medium_heat_diss_temp=medium_heat_diss_temp, low_heat_diss_temp=low_heat_diss_temp,
         heat_diss_exergy_eff=heat_diss_exergy_eff)

    if objective_function == "exergy":
        ElectricalExergy(energy_unit=pv_panels.elec_consumption_unit)
        ElectricalExergy(energy_unit=pv_panels.elec_production_unit)
        ExergyDestruction(energy_unit=pv_panels)
        ElectricalExergy(energy_unit=solar_thermal_collectors.elec_consumption_unit)
        ThermalExergy(energy_unit=solar_thermal_collectors.thermal_production_unit,
                      temp_heat=st_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=solar_thermal_collectors)
        ElectricalExergy(energy_unit=conduit_hydraulics)
        ExergyDestruction(energy_unit=conduit_hydraulics,
                          exergy_eff=fluid_pumps_exergy_eff)
        ElectricalExergy(energy_unit=elec_batteries)
        ExergyDestruction(energy_unit=elec_batteries, temp_heat=50000,
                          temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=thermal_storage, temp_heat=therm_stor_temp,
                      temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=thermal_storage,
                          temp_heat=therm_stor_temp,
                          temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=high_temp_heat_dissipation,
                      temp_heat=high_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=high_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)
        ThermalExergy(energy_unit=medium_temp_heat_dissipation,
                      temp_heat=medium_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=medium_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)
        ThermalExergy(energy_unit=low_temp_heat_dissipation,
                      temp_heat=low_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=low_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)

        ElectricalExergy(energy_unit=heat_pump_dhw.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_dhw.thermal_consumption_unit,
                      temp_heat=hp_dhw_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_dhw.thermal_production_unit,
                      temp_heat=hp_dhw_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_dhw)

        ElectricalExergy(energy_unit=heat_pump_heating.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_heating.thermal_consumption_unit,
                      temp_heat=hp_heating_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_heating.thermal_production_unit,
                      temp_heat=hp_heating_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_heating)

        # - Cooling heat pump -
        ElectricalExergy(energy_unit=heat_pump_cooling.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_cooling.thermal_consumption_unit,
                      temp_heat=hp_cooling_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_cooling.thermal_production_unit,
                      temp_heat=hp_cooling_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_cooling)

        " Imaginary heat pump representing use of high temp heat for medium temp needs "
        ElectricalExergy(energy_unit=from_high_temp_to_medium_temp.elec_consumption_unit)
        ThermalExergy(energy_unit=from_high_temp_to_medium_temp.thermal_consumption_unit,
                      temp_heat=hp_dhw_temp_out, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=from_high_temp_to_medium_temp.thermal_production_unit,
                      temp_heat=hp_heating_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=from_high_temp_to_medium_temp)

    elec_node = EnergyNode(time, name='elec_node', energy_type='Electrical')
    pv_outlet_node = EnergyNode(time, name='pv_outlet_node', energy_type='Electrical')
    solar_pv_node = EnergyNode(time, name='solar_pv_node', energy_type='Electrical')
    solar_st_node = EnergyNode(time, name='solar_st_node', energy_type='Electrical')
    thermal_collectors_node = EnergyNode(
        time, name='thermal_collectors_node', energy_type='Thermal')
    lake_node = EnergyNode(time, name='lake_node', energy_type='Thermal')
    cooling_node = EnergyNode(time, name='cooling_node', energy_type='Thermal')
    medium_temp_dissipation_node = EnergyNode(
        time, name='medium_temp_dissipation_node', energy_type='Thermal')
    heating_node = EnergyNode(
        time, name='heating_node', energy_type='Thermal')
    dhw_node = EnergyNode(time, name='dhw_node', energy_type='Thermal')
    imaginary_elec_node = EnergyNode(time, name='imaginary_elec_node', energy_type='Electrical')

    if elec_batt_capa == 0:
        elec_node.connect_units(buy_elec_from_grid, zac_2_elec, zac_3_elec, hameau_elec,
                                conduit_hydraulics,
                                heat_pump_dhw.elec_consumption_unit,
                                heat_pump_heating.elec_consumption_unit,
                                heat_pump_cooling.elec_consumption_unit)
    else:
        elec_node.connect_units(buy_elec_from_grid, zac_2_elec, zac_3_elec, hameau_elec,
                                conduit_hydraulics, elec_batteries,
                                heat_pump_dhw.elec_consumption_unit,
                                heat_pump_heating.elec_consumption_unit,
                                heat_pump_cooling.elec_consumption_unit)
    pv_outlet_node.connect_units(pv_panels.elec_production_unit, sell_elec_to_grid)
    pv_outlet_node.export_to_node(elec_node)

    solar_pv_node.connect_units(solar_irradiation_pv,
                                pv_panels.elec_consumption_unit)
    solar_st_node.connect_units(solar_irradiation_st,
                                solar_thermal_collectors.elec_consumption_unit)
    thermal_collectors_node.connect_units(solar_thermal_collectors.thermal_production_unit,
                                          high_temp_heat_dissipation)
    thermal_collectors_node.export_to_node(dhw_node)

    lake_node.connect_units(lake, heat_pump_dhw.thermal_consumption_unit,
                            heat_pump_heating.thermal_consumption_unit
                            )

    cooling_node.connect_units(zac_2_cooling, zac_3_cooling, ines_cooling,
                               heat_pump_cooling.thermal_consumption_unit,
                               low_temp_heat_dissipation,
                               )
    medium_temp_dissipation_node.connect_units(medium_temp_heat_dissipation,
                                               heat_pump_cooling.thermal_production_unit)
    medium_temp_dissipation_node.export_to_node(heating_node)

    heating_node.connect_units(heat_pump_heating.thermal_production_unit,
                               zac_2_heating, zac_3_heating, hameau_heating,
                               from_high_temp_to_medium_temp.thermal_production_unit)

    if therm_stor_capa == 0:
        dhw_node.connect_units(zac_2_dhw, zac_3_dhw,
                           hameau_dhw, heat_pump_dhw.thermal_production_unit,
                           from_high_temp_to_medium_temp.thermal_consumption_unit)
    else:
        dhw_node.connect_units(thermal_storage, zac_2_dhw, zac_3_dhw,
                           hameau_dhw, heat_pump_dhw.thermal_production_unit,
                           from_high_temp_to_medium_temp.thermal_consumption_unit)

    imaginary_elec_node.connect_units(imaginary_elec_prod_unit, from_high_temp_to_medium_temp.elec_consumption_unit
                                      )

    if objective_function == "local_production":
        buy_elec_from_grid.minimize_production()
        sell_elec_to_grid.minimize_consumption()
    elif objective_function == "lake_production":
        lake.maximize_production()
    elif objective_function == "heat_dissipation":
        high_temp_heat_dissipation.minimize_consumption()
        medium_temp_heat_dissipation.minimize_consumption()
        low_temp_heat_dissipation.minimize_consumption()
    elif objective_function == "minimize_thermal_production":
        heat_pump_dhw.elec_consumption_unit.minimize_consumption()
        heat_pump_heating.elec_consumption_unit.minimize_consumption()
        heat_pump_cooling.elec_consumption_unit.minimize_consumption()

    if objective_function == "exergy":
        """ EXERGY-FOCUSED OPTIMIZATION OBJECTIVES """
        heat_pump_dhw.minimize_exergy_destruction()
        heat_pump_heating.minimize_exergy_destruction()
        heat_pump_cooling.minimize_exergy_destruction()
        elec_batteries.minimize_exergy_destruction()
        thermal_storage.minimize_exergy_destruction()
        high_temp_heat_dissipation.minimize_exergy_destruction()
        medium_temp_heat_dissipation.minimize_exergy_destruction()
        low_temp_heat_dissipation.minimize_exergy_destruction()
        from_high_temp_to_medium_temp.minimize_exergy_destruction()

    model = OptimisationModel(time, name='optimization_model')
    #  model.verbose = 0
    model.add_nodes(elec_node, pv_outlet_node,
                    solar_pv_node, solar_st_node, lake_node, cooling_node,
                    medium_temp_dissipation_node, heating_node, dhw_node,
                    thermal_collectors_node, imaginary_elec_node)
    model.solve_and_update(GUROBI_CMD())

    if LpStatus[model.status] == 'Optimal':
        # Electrical part
        grid = [a for a in buy_elec_from_grid.p.value.values()]
        pv = [b for b in pv_panels.elec_production_unit.p.value.values()]
        disch = [c for c in elec_batteries.discharge.p.value.values()]
        ch = [c for c in elec_batteries.charge.p.value.values()]
        zac2 = [d for d in zac_2_elec.p.value]
        zac3 = [e for e in zac_3_elec.p.value]
        ham = [f for f in hameau_elec.p.value]
        hpco = [g for g in heat_pump_cooling.elec_consumption_unit.p.value.values()]
        hphe = [h for h in heat_pump_heating.elec_consumption_unit.p.value.values()]
        hpdw = [i for i in heat_pump_dhw.elec_consumption_unit.p.value.values()]

        total_resid_elec_demands = [d + e + f for d, e, f in zip(zac2, zac3, ham)]
        total_hp_elec_demands = [d + e + f for d, e, f in zip(hpco, hphe, hpdw)]
        if elec_batt_capa == 0:
            total_renewable_elec_prod = pv
        else:
            total_renewable_elec_prod = [b + c - d for b, c, d in zip(pv, disch,
                                                                     ch)]

        elec_self_prod = 0

        for t in range(len(total_resid_elec_demands)):
            if total_renewable_elec_prod[t] > total_resid_elec_demands[t] + total_hp_elec_demands[t]:
                elec_self_prod += total_resid_elec_demands[t] + total_hp_elec_demands[t]
            else:
                elec_self_prod += total_renewable_elec_prod[t]

        ESS = elec_self_prod / (sum(total_resid_elec_demands) + sum(total_hp_elec_demands))
        ESC = elec_self_prod / sum(total_renewable_elec_prod)

        # Thermal part
        ht_zac2 = [h for h in zac_2_heating.p.value]
        ht_zac3 = [h for h in zac_3_heating.p.value]
        ht_ham = [h for h in hameau_heating.p.value]
        dhw_zac2 = [h for h in zac_2_dhw.p.value]
        dhw_zac3 = [h for h in zac_3_dhw.p.value]
        dhw_ham = [h for h in hameau_dhw.p.value]
        cool_zac2 = [h for h in zac_2_cooling.p.value]
        cool_zac3 = [h for h in zac_3_cooling.p.value]

        th_col = [g for g in solar_thermal_collectors.thermal_production_unit.p.value.values()]
        th_st_disch = [h for h in thermal_storage.discharge.p.value.values()]
        th_st_ch = [h for h in thermal_storage.charge.p.value.values()]
        lake_p = [l for l in lake.p.value.values()]
        hpco_th = [i for i in heat_pump_cooling.thermal_production_unit.p.value.values()]
        hphe_th = [i for i in heat_pump_heating.thermal_production_unit.p.value.values()]
        hpdw_th = [i for i in heat_pump_dhw.thermal_production_unit.p.value.values()]

        total_resid_thermal_demands = [a + b + c + e + f + g + h + i for a, b, c, e, f, g, h, i in
                                       zip(ht_zac2, ht_zac3, ht_ham, dhw_zac2, dhw_zac3, dhw_ham, cool_zac2, cool_zac3)]
        if therm_stor_capa == 0:
            total_renewable_thermal_prod = [b for b in th_col]
        else:
            total_renewable_thermal_prod = [b + c - d + e for b, c, d, e in zip(
                th_col,
                                                                     th_st_disch,
                                                                     th_st_ch,
                                                                         lake_p)]

        th_self_prod = 0

        for t in range(len(total_resid_thermal_demands)):
            if total_renewable_thermal_prod[t] > total_resid_thermal_demands[t]:
                th_self_prod += total_resid_thermal_demands[t]
            else:
                th_self_prod += total_renewable_thermal_prod[t]

        ThSS = th_self_prod / sum(total_resid_thermal_demands)
        ThSC = th_self_prod / sum(total_renewable_thermal_prod)

        TSS = (elec_self_prod + th_self_prod) / (sum(
            total_resid_thermal_demands) + sum(total_resid_elec_demands)  + sum(
            total_hp_elec_demands))
        TSC = (elec_self_prod + th_self_prod)  / (sum(
            total_renewable_thermal_prod) + sum(total_renewable_elec_prod))

    else:
        compute_gurobi_IIS(gurobi_exe_path=r'C:/Users/brugerma/gurobi950/win64/bin', opt_model=model)

        ESS, ESC, ThSS, ThSC, TSS, TSC = None, None, None, None, None, None

    return ESS, ESC, ThSS, ThSC, TSS, TSC

def beaulac_project_5_obj_solving(work_path, time, parameter_designed = None,
                            collect_data = None,
                            time_lapse=None,
                            temp_dead_state=20,
                            pv_area=1, pv_efficiency=0, st_area=1, st_efficiency=0,
                            st_temp_out=100, elec_batt_pc_max=1, elec_batt_pd_max=1,
                            elec_batt_capa=1, elec_batt_soc_min=1, elec_batt_soc_max=1,
                            elec_batt_self_disch=1, elec_batt_soc_ini=1, therm_stor_pc_max=1,
                            therm_stor_pd_max=1, therm_stor_capa=1, therm_stor_soc_min=1,
                            therm_stor_soc_max=1, therm_stor_self_disch=1, therm_stor_soc_ini=1,
                            therm_stor_temp=65, p_max_lake=100, conduit_max_cooling=100,
                            hp_heating_cop=2, hp_heating_pmax_elec=1, hp_heating_temp_in=25,
                            hp_heating_temp_out=50, hp_dhw_cop=2, hp_dhw_pmax_elec=1,
                            hp_dhw_temp_in=25, hp_dhw_temp_out=50, hp_cooling_cop=2,
                            hp_cooling_pmax_elec=1, hp_cooling_temp_in=25, hp_cooling_temp_out=50,
                            fluid_pumps_exergy_eff=1, high_heat_diss_temp=70,
                            medium_heat_diss_temp=35, low_heat_diss_temp=15,
                            heat_diss_exergy_eff=0):

    objective_function = "local_production"
    ESS, ESC, ThSS, ThSC, TSS, TSC = beaulac_project_solving(work_path, time=time, objective_function=objective_function,
                                                 time_lapse=time_lapse,
                                                 temp_dead_state=20,
                                                 pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area,
                                                 st_efficiency=st_efficiency,
                                                 st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max,
                                                 elec_batt_pd_max=elec_batt_pd_max,
                                                 elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min,
                                                 elec_batt_soc_max=elec_batt_soc_max,
                                                 elec_batt_self_disch=elec_batt_self_disch,
                                                 elec_batt_soc_ini=elec_batt_soc_ini,
                                                 therm_stor_pc_max=therm_stor_pc_max,
                                                 therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa,
                                                 therm_stor_soc_min=therm_stor_soc_min,
                                                 therm_stor_soc_max=therm_stor_soc_max,
                                                 therm_stor_self_disch=therm_stor_self_disch,
                                                 therm_stor_soc_ini=therm_stor_soc_ini,
                                                 therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake,
                                                 conduit_max_cooling=conduit_max_cooling,
                                                 hp_heating_cop=hp_heating_cop,
                                                 hp_heating_pmax_elec=hp_heating_pmax_elec,
                                                 hp_heating_temp_in=hp_heating_temp_in,
                                                 hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop,
                                                 hp_dhw_pmax_elec=hp_dhw_pmax_elec,
                                                 hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out,
                                                 hp_cooling_cop=hp_cooling_cop,
                                                 hp_cooling_pmax_elec=hp_cooling_pmax_elec,
                                                 hp_cooling_temp_in=hp_cooling_temp_in,
                                                 hp_cooling_temp_out=hp_cooling_temp_out,
                                                 fluid_pumps_exergy_eff=fluid_pumps_exergy_eff,
                                                 high_heat_diss_temp=high_heat_diss_temp,
                                                 medium_heat_diss_temp=medium_heat_diss_temp,
                                                 low_heat_diss_temp=low_heat_diss_temp,
                                                 heat_diss_exergy_eff=heat_diss_exergy_eff)

    collected_data_dict = {'parameter_designed': [parameter_designed],
                           'objective_function': [objective_function],
                           'pv_area': [pv_area],
                           'e_capa': [elec_batt_capa],
                           'st_area': [st_area],
                           'th_capa': [therm_stor_capa],
                           'TSC': [TSC],
                           'TSS': [TSS],
                           'ThSC': [ThSC],
                           'ThSS': [ThSS],
                           'ESC': [ESC],
                           'ESS': [ESS]}

    collected_data = pd.DataFrame.from_dict(collected_data_dict)
    collect_data = pd.concat([collect_data, collected_data])
    print(collect_data)

    objective_function = "lake_production"
    ESS, ESC, ThSS, ThSC, TSS, TSC = beaulac_project_solving(work_path, time=time, objective_function=objective_function,
                                                 time_lapse=time_lapse,
                                                 temp_dead_state=20,
                                                 pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area,
                                                 st_efficiency=st_efficiency,
                                                 st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max,
                                                 elec_batt_pd_max=elec_batt_pd_max,
                                                 elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min,
                                                 elec_batt_soc_max=elec_batt_soc_max,
                                                 elec_batt_self_disch=elec_batt_self_disch,
                                                 elec_batt_soc_ini=elec_batt_soc_ini,
                                                 therm_stor_pc_max=therm_stor_pc_max,
                                                 therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa,
                                                 therm_stor_soc_min=therm_stor_soc_min,
                                                 therm_stor_soc_max=therm_stor_soc_max,
                                                 therm_stor_self_disch=therm_stor_self_disch,
                                                 therm_stor_soc_ini=therm_stor_soc_ini,
                                                 therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake,
                                                 conduit_max_cooling=conduit_max_cooling,
                                                 hp_heating_cop=hp_heating_cop,
                                                 hp_heating_pmax_elec=hp_heating_pmax_elec,
                                                 hp_heating_temp_in=hp_heating_temp_in,
                                                 hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop,
                                                 hp_dhw_pmax_elec=hp_dhw_pmax_elec,
                                                 hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out,
                                                 hp_cooling_cop=hp_cooling_cop,
                                                 hp_cooling_pmax_elec=hp_cooling_pmax_elec,
                                                 hp_cooling_temp_in=hp_cooling_temp_in,
                                                 hp_cooling_temp_out=hp_cooling_temp_out,
                                                 fluid_pumps_exergy_eff=fluid_pumps_exergy_eff,
                                                 high_heat_diss_temp=high_heat_diss_temp,
                                                 medium_heat_diss_temp=medium_heat_diss_temp,
                                                 low_heat_diss_temp=low_heat_diss_temp,
                                                 heat_diss_exergy_eff=heat_diss_exergy_eff)

    collected_data_dict = {'parameter_designed': [parameter_designed],
                           'objective_function': [objective_function],
                           'pv_area': [pv_area],
                           'e_capa': [elec_batt_capa],
                           'st_area': [st_area],
                           'th_capa': [therm_stor_capa],
                           'TSC': [TSC],
                           'TSS': [TSS],
                           'ThSC': [ThSC],
                           'ThSS': [ThSS],
                           'ESC': [ESC],
                           'ESS': [ESS]}

    collected_data = pd.DataFrame.from_dict(collected_data_dict)
    collect_data = pd.concat([collect_data, collected_data])
    print(collect_data)

    objective_function = "heat_dissipation"
    ESS, ESC, ThSS, ThSC, TSS, TSC = beaulac_project_solving(work_path, time=time, objective_function=objective_function,
                                                 time_lapse=time_lapse,
                                                 temp_dead_state=20,
                                                 pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area,
                                                 st_efficiency=st_efficiency,
                                                 st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max,
                                                 elec_batt_pd_max=elec_batt_pd_max,
                                                 elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min,
                                                 elec_batt_soc_max=elec_batt_soc_max,
                                                 elec_batt_self_disch=elec_batt_self_disch,
                                                 elec_batt_soc_ini=elec_batt_soc_ini,
                                                 therm_stor_pc_max=therm_stor_pc_max,
                                                 therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa,
                                                 therm_stor_soc_min=therm_stor_soc_min,
                                                 therm_stor_soc_max=therm_stor_soc_max,
                                                 therm_stor_self_disch=therm_stor_self_disch,
                                                 therm_stor_soc_ini=therm_stor_soc_ini,
                                                 therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake,
                                                 conduit_max_cooling=conduit_max_cooling,
                                                 hp_heating_cop=hp_heating_cop,
                                                 hp_heating_pmax_elec=hp_heating_pmax_elec,
                                                 hp_heating_temp_in=hp_heating_temp_in,
                                                 hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop,
                                                 hp_dhw_pmax_elec=hp_dhw_pmax_elec,
                                                 hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out,
                                                 hp_cooling_cop=hp_cooling_cop,
                                                 hp_cooling_pmax_elec=hp_cooling_pmax_elec,
                                                 hp_cooling_temp_in=hp_cooling_temp_in,
                                                 hp_cooling_temp_out=hp_cooling_temp_out,
                                                 fluid_pumps_exergy_eff=fluid_pumps_exergy_eff,
                                                 high_heat_diss_temp=high_heat_diss_temp,
                                                 medium_heat_diss_temp=medium_heat_diss_temp,
                                                 low_heat_diss_temp=low_heat_diss_temp,
                                                 heat_diss_exergy_eff=heat_diss_exergy_eff)

    collected_data_dict = {'parameter_designed': [parameter_designed],
                           'objective_function': [objective_function],
                           'pv_area': [pv_area],
                           'e_capa': [elec_batt_capa],
                           'st_area': [st_area],
                           'th_capa': [therm_stor_capa],
                           'TSC': [TSC],
                           'TSS': [TSS],
                           'ThSC': [ThSC],
                           'ThSS': [ThSS],
                           'ESC': [ESC],
                           'ESS': [ESS]}

    collected_data = pd.DataFrame.from_dict(collected_data_dict)
    collect_data = pd.concat([collect_data, collected_data])
    print(collect_data)

    objective_function = "minimize_thermal_production"
    ESS, ESC, ThSS, ThSC, TSS, TSC = beaulac_project_solving(work_path, time=time, objective_function=objective_function,
                                                 time_lapse=time_lapse,
                                                 temp_dead_state=20,
                                                 pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area,
                                                 st_efficiency=st_efficiency,
                                                 st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max,
                                                 elec_batt_pd_max=elec_batt_pd_max,
                                                 elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min,
                                                 elec_batt_soc_max=elec_batt_soc_max,
                                                 elec_batt_self_disch=elec_batt_self_disch,
                                                 elec_batt_soc_ini=elec_batt_soc_ini,
                                                 therm_stor_pc_max=therm_stor_pc_max,
                                                 therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa,
                                                 therm_stor_soc_min=therm_stor_soc_min,
                                                 therm_stor_soc_max=therm_stor_soc_max,
                                                 therm_stor_self_disch=therm_stor_self_disch,
                                                 therm_stor_soc_ini=therm_stor_soc_ini,
                                                 therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake,
                                                 conduit_max_cooling=conduit_max_cooling,
                                                 hp_heating_cop=hp_heating_cop,
                                                 hp_heating_pmax_elec=hp_heating_pmax_elec,
                                                 hp_heating_temp_in=hp_heating_temp_in,
                                                 hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop,
                                                 hp_dhw_pmax_elec=hp_dhw_pmax_elec,
                                                 hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out,
                                                 hp_cooling_cop=hp_cooling_cop,
                                                 hp_cooling_pmax_elec=hp_cooling_pmax_elec,
                                                 hp_cooling_temp_in=hp_cooling_temp_in,
                                                 hp_cooling_temp_out=hp_cooling_temp_out,
                                                 fluid_pumps_exergy_eff=fluid_pumps_exergy_eff,
                                                 high_heat_diss_temp=high_heat_diss_temp,
                                                 medium_heat_diss_temp=medium_heat_diss_temp,
                                                 low_heat_diss_temp=low_heat_diss_temp,
                                                 heat_diss_exergy_eff=heat_diss_exergy_eff)

    collected_data_dict = {'parameter_designed': [parameter_designed],
                           'objective_function': [objective_function],
                           'pv_area': [pv_area],
                           'e_capa': [elec_batt_capa],
                           'st_area': [st_area],
                           'th_capa': [therm_stor_capa],
                           'TSC': [TSC],
                           'TSS': [TSS],
                           'ThSC': [ThSC],
                           'ThSS': [ThSS],
                           'ESC': [ESC],
                           'ESS': [ESS]}

    collected_data = pd.DataFrame.from_dict(collected_data_dict)
    collect_data = pd.concat([collect_data, collected_data])
    print(collect_data)

    objective_function = "exergy"
    ESS, ESC, ThSS, ThSC, TSS, TSC = beaulac_project_solving(work_path,
                                                           time=time, objective_function=objective_function,
                                                 time_lapse=time_lapse,
                                                 temp_dead_state=20,
                                                 pv_area=pv_area, pv_efficiency=pv_efficiency, st_area=st_area,
                                                 st_efficiency=st_efficiency,
                                                 st_temp_out=st_temp_out, elec_batt_pc_max=elec_batt_pc_max,
                                                 elec_batt_pd_max=elec_batt_pd_max,
                                                 elec_batt_capa=elec_batt_capa, elec_batt_soc_min=elec_batt_soc_min,
                                                 elec_batt_soc_max=elec_batt_soc_max,
                                                 elec_batt_self_disch=elec_batt_self_disch,
                                                 elec_batt_soc_ini=elec_batt_soc_ini,
                                                 therm_stor_pc_max=therm_stor_pc_max,
                                                 therm_stor_pd_max=therm_stor_pd_max, therm_stor_capa=therm_stor_capa,
                                                 therm_stor_soc_min=therm_stor_soc_min,
                                                 therm_stor_soc_max=therm_stor_soc_max,
                                                 therm_stor_self_disch=therm_stor_self_disch,
                                                 therm_stor_soc_ini=therm_stor_soc_ini,
                                                 therm_stor_temp=therm_stor_temp, p_max_lake=p_max_lake,
                                                 conduit_max_cooling=conduit_max_cooling,
                                                 hp_heating_cop=hp_heating_cop,
                                                 hp_heating_pmax_elec=hp_heating_pmax_elec,
                                                 hp_heating_temp_in=hp_heating_temp_in,
                                                 hp_heating_temp_out=hp_heating_temp_out, hp_dhw_cop=hp_dhw_cop,
                                                 hp_dhw_pmax_elec=hp_dhw_pmax_elec,
                                                 hp_dhw_temp_in=hp_dhw_temp_in, hp_dhw_temp_out=hp_dhw_temp_out,
                                                 hp_cooling_cop=hp_cooling_cop,
                                                 hp_cooling_pmax_elec=hp_cooling_pmax_elec,
                                                 hp_cooling_temp_in=hp_cooling_temp_in,
                                                 hp_cooling_temp_out=hp_cooling_temp_out,
                                                 fluid_pumps_exergy_eff=fluid_pumps_exergy_eff,
                                                 high_heat_diss_temp=high_heat_diss_temp,
                                                 medium_heat_diss_temp=medium_heat_diss_temp,
                                                 low_heat_diss_temp=low_heat_diss_temp,
                                                 heat_diss_exergy_eff=heat_diss_exergy_eff)

    collected_data_dict = {'parameter_designed': [parameter_designed],
                           'objective_function': [objective_function],
                           'pv_area': [pv_area],
                           'e_capa': [elec_batt_capa],
                           'st_area': [st_area],
                           'th_capa': [therm_stor_capa],
                           'TSC': [TSC],
                           'TSS': [TSS],
                           'ThSC': [ThSC],
                           'ThSS': [ThSS],
                           'ESC': [ESC],
                           'ESS': [ESS]}

    collected_data = pd.DataFrame.from_dict(collected_data_dict)

    # print(tabulate(data_test,
     #              headers=["Objective Function", "TSC", "TSS", "ESC",
    #              "ESS"]))

    collect_data = pd.concat([collect_data, collected_data])
    return collect_data

