from python_scripts.NB_PV_self_consumption_utils import *
from omegalpes.general.utils.plots import *

from pulp import LpStatus

import numpy as np
import pandas as pd
from python_scripts.NB_ORUCE_utils import *
from omegalpes.general.optimisation.elements import Objective

def consumption_profile(path):
    # Data
    yearly_data = read_data(
        file_name='./data/irradiance_and_consumption_data.csv')
    yearly_consumption = yearly_data[1]
    yearly_cons_values = yearly_consumption.values[:, 0]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(yearly_cons_values, label='Consommation')
    ax.set_xlabel('Temps (h)')
    ax.set_ylabel('Puissance (W)')
    ax.set_title('Consommation annuelle sur le cas présent')
    ax.legend()

    plt.show()

    print('Consommation annuelle du logement = {} '
          'kWh'.format(round(sum(yearly_cons_values)/1e3)))

def solar_profile(path, inclinaison, azimuth, surface):
    # Using date index in order to determine the sun trajectory
    yearly_data = read_data(
        file_name='./data/irradiance_and_consumption_data.csv')

    yearly_irradiance = yearly_data[0]
    yearly_consumption = yearly_data[1]

    solar = PVSolar(yearly_irradiance.index.to_series())

    yearly_DNI = np.array(yearly_irradiance['DNI'])
    yearly_DHI = np.array(yearly_irradiance['DHI'])

    # determining pv_power and energy from the given parameters, DNI and DHI:
    yearly_pv_power_values = solar.pv_power(inclinaison, azimuth, surface, yearly_DNI,
                                            yearly_DHI)
    yearly_pv_power = pd.DataFrame({'pv_power': yearly_pv_power_values},
                                   index=yearly_consumption.index)

    pv_power_values = yearly_pv_power.values[:, 0]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(pv_power_values, label='Production solaire')
    ax.set_xlabel('Temps (h)')
    ax.set_ylabel('Puissance (W)')
    ax.set_title('Production photovoltaïque annuelle sur le cas présent')
    ax.legend()

    plt.show()

    print('Production photovoltaïque annuelle du logement = {} '
          'kWh'.format(round(sum(pv_power_values)/1e3)))

    return None


def solar_and_consumption_profile(path, inclinaison, azimuth, surface, date_debut,
                                  date_fin):
    # Using date index in order to determine the sun trajectory
    yearly_data = read_data(
        file_name='./data/irradiance_and_consumption_data.csv')

    yearly_irradiance = yearly_data[0]
    yearly_consumption = yearly_data[1]

    solar = PVSolar(yearly_irradiance.index.to_series())

    yearly_DNI = np.array(yearly_irradiance['DNI'])
    yearly_DHI = np.array(yearly_irradiance['DHI'])

    # determining pv_power and energy from the given parameters, DNI and DHI:
    yearly_pv_power_values = solar.pv_power(inclinaison, azimuth, surface, yearly_DNI,
                                            yearly_DHI)
    yearly_pv_power = pd.DataFrame({'pv_power': yearly_pv_power_values},
                                   index=yearly_consumption.index)

    consumption = yearly_consumption.loc[pd.to_datetime(date_debut):pd.to_datetime(date_fin)]
    pv_power = yearly_pv_power.loc[pd.to_datetime(date_debut):pd.to_datetime(date_fin)]

    cons_values = consumption.values[:, 0]
    pv_power_values = pv_power.values[:, 0]

    # plot
    fig3, ax3 = plt.subplots()
    ax3.plot(cons_values, label='Consommation')
    ax3.plot(pv_power_values, label='Production solaire')
    ax3.set_xlabel('Temps(h)')
    ax3.set_ylabel('Puissance (W)')
    ax3.set_title('Profil de production solaire et de consommation du '
                  'logement sur la journée choisie ')
    ax3.legend()

    plt.show()

    return None


def optimize_EC_AC(path, inclinaison, azimuth, surface, date_debut, date_fin,
                     pas_de_temps, objectif):

    time = TimeUnit(start=date_debut, end=date_fin, dt=pas_de_temps)

    yearly_data = read_data(
        file_name='./data/irradiance_and_consumption_data.csv')

    yearly_irradiance = yearly_data[0]
    yearly_consumption = yearly_data[1]

    solar = PVSolar(yearly_irradiance.index.to_series())

    yearly_DNI = np.array(yearly_irradiance['DNI'])
    yearly_DHI = np.array(yearly_irradiance['DHI'])

    # determining pv_power and energy from the given parameters, DNI and DHI:
    yearly_pv_power_values = solar.pv_power(inclinaison, azimuth, surface, yearly_DNI,
                                            yearly_DHI)
    yearly_pv_power = pd.DataFrame({'pv_power': yearly_pv_power_values},
                                   index=yearly_consumption.index)

    consumption = yearly_consumption.loc[pd.to_datetime(date_debut):pd.to_datetime(date_fin)]
    pv_power = yearly_pv_power.loc[pd.to_datetime(date_debut):pd.to_datetime(date_fin)]

    cons_values = consumption.values[:, 0]
    pv_power_values = pv_power.values[:, 0]

    # The maximal power values of the grid are chosen based on the maximal consumption and PV production values
    max_grid_sizing = (max(max(pv_power_values), max(cons_values))) * 2

    hc_cost = 0.12190
    hp_cost = 0.14940

    hc_time = [22, 23, 0, 1, 2, 3, 4, 5]
    operating_cost = np.zeros(24)
    for t in range(len(operating_cost)):
        if t in hc_time:
            operating_cost[t] = hc_cost
        else:
            operating_cost[t] = hp_cost

    operating_cost = operating_cost.tolist()

    # ENERGY UNITS
    power_grid_imports = VariableProductionUnit(time=time,
                                               name='power_grid_imports',
                                               energy_type='Electrical',
                                               operating_cost=operating_cost)

    power_grid_exports = VariableConsumptionUnit(time=time,
                                               name='power_grid_exports',
                                               energy_type='Electrical')

    pv_pannels = FixedProductionUnit(time=time, name='PV_pannels',
                                     p=pv_power_values.tolist(),
                                     energy_type=elec)

    dwelling_consumption = FixedConsumptionUnit(time=time,
                                                name='dwelling_consumption',
                                                p=cons_values.tolist(),
                                                energy_type=elec)

    washer_load_5_min = import_clothes_washer_and_dryer_load_profiles(path)[0]
    washer_load = np.zeros(2)
    for t in range(len(washer_load)):
        washer_load[t] = sum(washer_load_5_min[6 * t:6 * (t + 1)]) / 12
    washer_load = washer_load.tolist()

    washer = ShiftableConsumptionUnit(time, name="washer",
                                      power_values=washer_load,
                                      energy_type=elec)
    washer.add_operating_time_range([['12:00', '14:00'], ['18:00', '23:00']])

    dryer_load_5_min = import_clothes_washer_and_dryer_load_profiles(path)[1]
    dryer_load = np.zeros(2)
    for t in range(len(dryer_load)):
        dryer_load[t] = sum(dryer_load_5_min[6 * t:6 * (t + 1)]) / 12
    dryer_load = dryer_load.tolist()

    dryer = ShiftableConsumptionUnit(time, name="dryer",
                                      power_values=dryer_load,
                                      energy_type=elec)

    water_heater = ElectricalToThermalConversionUnit(
        time, name="water_heater", elec_to_therm_ratio=0.9)

    dhw_load_5_min = import_domestic_hot_water_load_profile(path)
    dhw_load = np.zeros(24)
    for t in range(len(dhw_load)):
        dhw_load[t] = sum(dhw_load_5_min[6 * t:6 * (t + 1)]) / 12
    dhw_load = dhw_load.tolist()

    dhw = FixedConsumptionUnit(time, name="dhw", p=dhw_load,
                               energy_type=thermal)

    water_tank = StorageUnit(time, name="water_tank", self_disch_t=0.05,
                             pc_max=6000, pd_max=6000,
                             soc_min=0.2, energy_type=thermal,
                             ef_is_e0=True, capacity=6000)

    elec_node = EnergyNode(time, name="elec_node",
                           energy_type=elec)

    heat_node = EnergyNode(time, name="heat_node",
                           energy_type=thermal)

    # OBJECTIVES
    # Already available in OMEGAlpes
    if objectif == 'local':
        power_grid_imports.minimize_production()
        power_grid_exports.minimize_consumption()
    elif objectif == 'economique':
        power_grid_imports.minimize_operating_cost()

    elec_node.connect_units(power_grid_imports, power_grid_exports,
                            pv_pannels, dwelling_consumption,
                            washer, dryer,
                            water_heater.elec_consumption_unit)

    heat_node.connect_units(dhw, water_tank,
                            water_heater.thermal_production_unit)

    # MODEL BUILT AND RUN
    model = OptimisationModel(time, name="ORUCE")
    model.add_nodes(elec_node, heat_node)

    model.solve_and_update()

    return time, elec_node, heat_node, dwelling_consumption, washer, dryer, \
           power_grid_imports, power_grid_exports, water_tank, pv_pannels, \
           water_heater, dhw, 

def plot_nodes(elec_node, heat_node):
    plot_node_energetic_flows(elec_node)
    plot_node_energetic_flows(heat_node)

    plt.show()

    return None

def plot_consumption(time, dwelling_consumption):

    plot_quantity(time, dwelling_consumption.p, title = "Profil de consommation sur la journée étudiée")

    plt.show()

    return None

def plot_equipments(time, washer, dryer):
    plot_quantity(time, washer.p, title = "Profil de consommation du "
                                          "lave-linge "
                                          "sur la journée étudiée")
    plot_quantity(time, dryer.p, title = "Profil de consommation du sèche-linge "
                                          "sur la journée étudiée")

    plt.show()

    return None

def plot_grid(time,power_grid_imports):
    plot_quantity(time, power_grid_imports.p, title = "Profil de l'énergie "
                                                      "fournie par le réseau")

    plt.show()

    return None

def plot_dhw(time, dhw,water_heater, water_tank):
    plot_quantity(time, dhw.p, title = "Profil de consommation de l'eau "
                                       "chaude sanitaire")
    plot_quantity(time, water_heater.elec_consumption_unit.p, title =
    "Profil de la production de chaleur du chauffe-eau électrique")
    plot_quantity(time, water_tank.e, title =
    "Profil de la charge du stockage thermique")

    plt.show()

    return None

def determine_AC_outputs(pv_pannels, power_grid_exports, power_grid_imports,
                         dwelling_consumption, washer, dryer, water_heater):

    self_prod = sum(pv_pannels.p.get_value()) - sum(power_grid_exports.p.get_value())
    consumption = sum(dwelling_consumption.p.get_value()) + sum(
        washer.p.get_value()) + sum(dryer.p.get_value()) +sum(
        water_heater.elec_consumption_unit.p.get_value())


    ESS = self_prod / sum(pv_pannels.p.get_value())
    ESC = self_prod / consumption

    print("Le taux d'autoconsommation est de {0} %".format(round(100*ESC)))

    print("Le taux d'autoproduction est de {0} %".format(round(100*ESS)))

    print("Le coût de l'énergie fournie par le réseau est de {0} €".format(
        round(sum(
        power_grid_imports.operating_cost.value.values())/1000, 2)))

    return None


