from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump, ReversibleUnit
from omegalpes.energy.units.production_units import ProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit

def create_energy_model_and_energy_units(work_path, elec2therm_ratio=0.9, pc_max=5000, pd_max=5000,
                   pc_min=1000, pd_min=1000, capa=20000, ramp_limit=20000,
                   self_disch_t=0.01 / 24, cop_hp=3, pmax_elec_hp=1000, pmin_elec_hp = 117,
                   storage_soc_0=0.2, temp_wh=35, obj='exergy', scenario="B",
                   write_lp=True, pareto=False):

    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 7, dt=1)

    # Creating an empty model
    model = OptimisationModel(time=time, name='waste_heat_recovery_model')
    model.verbose = 0

    # Exergy reference temperature

    # Importing time-dependent data from files
    indus_cons_file = open(work_path + "/data/indus_cons_week.txt", "r")
    heat_load_file = open(work_path +
                          "/data/District_heat_load_consumption.txt", "r")

    # Creating the electro-intensive industry unit
    indus_cons = [c for c in map(float, indus_cons_file)]
    indus = ElectricalToThermalConversionUnit(time, 'indus',
                                              elec_to_therm_ratio=
                                              elec2therm_ratio,
                                              p_in_elec=indus_cons)

    # Creating unit for heat dissipation from the industrial process
    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          energy_type='Thermal')

    # Creating the thermal storage
    if obj == "energy_model":
        thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pc_max,
                                      energy_type='Thermal', pd_max=pd_max, capacity = capa,
                                      pc_min=pc_min, pd_min=pd_min, ef_is_e0=True,
                                      self_disch_t=self_disch_t)
    else:
        thermal_storage = StorageUnit(time, 'thermal_storage',
                                      ef_is_e0=True)

    # Creating the heat pump
    heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp, pmin_in_elec=pmin_elec_hp,
                         pmax_in_elec=pmax_elec_hp)

    # Creating the district heat load
    heat_load = [c for c in map(float, heat_load_file)]
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_load,
                                              energy_type='Thermal')

    # Creating the heat production plants
    heat_production = ProductionUnit(time, name='heat_production',
                                     energy_type='Thermal')

    # Creating the electric grid
    electric_grid = ReversibleUnit(time, name = 'electric_grid')

    return model, time, indus, dissipation, thermal_storage, heat_pump, district_heat_load, heat_production, electric_grid