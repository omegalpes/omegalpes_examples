# Notebooks requirements
Please run the following command in order to get the proper environment for 
the notebooks: 
- In anaconda or any local environment  
`pip install -r .\notebooks\specific_requirements\xxxx_requirements.txt`

- In mybinder  
`import os`  
`os.system('pip install -r specific_requirements/xxxx_requirements.txt')`