## Description générale
Dossier comprenant les différentes ressources mobilisées dans le [thèse de Sacha Hodencq](http://www.theses.fr/s225905) (2022).

## Utilisation des notebooks
Un [pad](https://pad.lescommuns.org/ORUCE_fr?view#Travailler-dans-un-environnement-%E2%80%A6) donne des indications quant à l'utilisation des notebooks, en ligne ou en local.

## Données
Irradiance (basée sur l'[OEMetadata](https://github.com/OpenEnergyPlatform/oemetadata/blob/develop/metadata/latest/metadata_key_description.md)) : 

|#|Key |Parameter |
|---|---|---|
| 1 | name |irradiance_data.csv| 
| 2 | title | Données d'irradiance à Grenoble en 2019| 
| 3 | id | |
| 4 | description | Données d'irradiance à Grenoble comprenant le DHI et DNI |
| 5 | language | |
| 6 | subject |  |
| 7 | keywords | |
| 8 | publicationDate |2020 |
| 9 | context | |
| 10 | spatial |Grenoble, 21 avenue des Martyrs| 
| 11 | temporal | 2019, pas de temps horaire|
| 12 | source |http://mhi-srv.g2elab.grenoble-inp.fr/API/ |
| 13 | licenses | Public Domain Dedication and License version v1.0. |
| 14 | contributors | Delinchant B. and Laranjeira T. |
| 15 | resources |Delinchant B., Wurtz F., Ploix S., Schanen J.-L. and Marechal Y. (2016). "GreEn-ER Living Lab - A Green Building with Energy Aware Occupants". SmartGreen'16, In proceedings of the 5th International Conference on Smart Cities and Green ICT Systems. ISBN 978-989-758-184-7, pages 316-323. DOI: 10.5220/0005795303160323 |

Consommation (basée sur l'[OEMetadata](https://github.com/OpenEnergyPlatform/oemetadata/blob/develop/metadata/latest/metadata_key_description.md)) : 

|#|Key |Parameter |
|---|---|---|
| 1 | name | consumption_data.csv|
| 2 | title |Données RES1 | 
| 3 | id | |
| 4 | description | Données de profil type de consommation résidentielle en France en 2019 |
| 5 | language | |
| 6 | subject |  |
| 7 | keywords | |
| 8 | publicationDate | 2020|
| 9 | context | |
| 10 | spatial |France, échelle résidentielle| 
| 11 | temporal | 2019, pas de temps horaire|
| 12 | source |https://data.enedis.fr/pages/coefficients-des-profils/ |
| 13 | licenses |  licence ouverte v2.0 (Etalab) |
| 14 | contributors | [Enedis](https://data.enedis.fr/pages/accueil) |
| 15 | resources |[Démarche open data Enedis](https://www.enedis.fr/open-data) |

## Notebook & scripts 
### Licence
[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)

### Fiche descriptive
| Nom                          | Autoconsommation photovoltaïque d'un foyer                                                                                           |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------ |
| Description                  | Cas d'étude d'autoconsommation PV développé comme un scénario type pour l'ouverture du processus de modélisation avec la méthode ORUCE |
| Objectif                     | L'objectif sur ce cas d'étude est la gestion optimale des flux de puissance est appliquée à la conception du système énergétique obtenu.                                                               |
| Laboratoire                  | G2Elab                                                      |
| Contact                      | Sacha Hodencq - sacha.hodencq@g2elab.grenoble-inp.fr                                                                 |
| Lien                         | *à venir*                                                        |
| Documents & données associés | S. Hodencq, B. Delinchant, and F. Wurtz, ‘Open and Reproducible Use Cases for Energy (ORUCE) methodology in systems design and operation: a dwelling photovoltaic self-consumption example’, Bruges, Belgium, Sep. 2021. Accessed: Sep. 16, 2021. Available: https://hal.archives-ouvertes.fr/hal-03341883 . Données : ensoleillement Grenoble, données de consommation RES1 Enedis                                              |
| Outils associés              | [OMEGAlpes](https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes)                                                                  |
| Licence                      | Apache 2.0                                                            |

| Paramètre                              | Paramétrage proposé                                                                                                                                 | Description (plus exhaustif, langage de vulgarisation nécessaire)                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| -------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Acteurs                                | G2Elab (laboratoire de recherche), secondaires : Enedis pour la fourniture de données (gestionnaire de réseau de distribution)                                                                                         | Le cas d'étude est fictif, aussi il y a peu d'interaction entre acteurs.                                                                                                                                                                                              |
| Terrain                                | Grenoble, France                                                                                                                          |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Enjeux                                 | Autoconsommation individuelle                                                                              |                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Secteurs                               | Electricité                                                                                                        |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Analyses                               | Énergétique                                                                                      | solaire PV, stockage batterie, réseau infini, consommation résidentielle                                                                                                                                                                                                                                                                                                                  |
| Hypothèses                             | Consommation et émissions moyennes estimées                                                                                                           | La consommation moyenne d'un logement français est basée sur les profils [RES1](https://www.enedis.fr/coefficients-des-profils) Enedis, c'est-à-dire les profils de consommation des logements raccordés en basse tension avec une puissance nominale inférieure à 6 kVA. On considère une consommation annuelle de 4453 kWh par foyer, en appliquant une consommation annuelle de 4586 kWh aux profils RES1. Cette consommation est calculée à partir du nombre de foyers raccordés au réseau selon un [rapport de la CRE](https://www.cre.fr/Documents/Publications/Rapports-thematiques/Etat-des-lieux-des-marches-de-detail-de-l-electricite-et-du-gaz-naturel-en-2017), page 19 (32,4 millions de consommateurs résidentiels en 2017), et la [consommation d'électricité finale du secteur résidentiel](https://opendata.reseaux-energies.fr/explore/dataset/consommation-annuelle-nette-typologie/table/?disjunctive.segment&sort=annee) de 148,6 TWh en 2019. On considère les émissions de CO2 euroépnnes (source: [EEA](https://www.eea.europa.eu/data-and-maps/daviz/co2-emission-intensity-5#tab-googlechartid_chart_11_filters=%7B%22rowFilters%22%3A%7B%7D%3B%22columnFilters%22%3A%7B%22pre_config_ugeo%22%3A%5B%22European%20Union%20(current%20composition%22%5D%7D%7D).                                                                                                                                                                                                                                                                                                                                                             |
| Nature du cas d'étude                  | Cas applicatif de la méthode ORUCE pour une thèse|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Comment le cas d'étude est établi ?    | Optimisation MILP                                            |                                                                                                                                                                                                                                        |
| Pourquoi le cas d'étude est-il établi? | Aide à la compréhension, Étude de solutions                                  | Aide à la compréhension de la méthode ORUCE et de l'autoconsommation, étude d'un dimensionnement donné pour le système d'autoconsommation.                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Finesse modèle / représentation        | Modèles macroscopiques de bilans de puissances                                                                                                               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Représentativité / Transposabilité     | Cas d'étude type                                                                                                                            | Données de localités fixes, mais cas d'étude pour un cas d'autoconsommation. Le modèle énergétique ainsi que la prise en compte de LCF et SCF sont proches de travaux de recherche répandus (voir Salom et al. - DOI: 10.18777/ieashc-task40-2014-0001).                                                                                                                                                                                                                                                                                                                                                                                                                     |
| Phase d’utilisation                    | pré-dimensionnement                                                |  |
| Échelle temporelle                     | Horizon : mois à année / Pas de temps : heure                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Échelle spatiale                       |bâtiment                             |    
| Résultats            | Il ne faut pas déduire de conclusions générales du cas d'étude simplifié présenté ici, car beaucoup d'hypothèses sont faites et des valeurs incertaines sont utilisées. L'objectif de l'étude est de souligner l'importance du processus ouvert de modélisation énergétique. L'étude fournit des modèles simples pour adapter l'étude de cas à différentes situations, par exemple en faisant varier les technologies des batteries ou des panneaux photovoltaïques, ou en sélectionnant les données d'entrée en fonction des nouvelles caractéristiques de l'étude et en tenant compte des incertitudes. Elle permet également de comparer différents modèles énergétiques, méthodes de réduction de la période d'étude ou du modèle, et techniques d'optimisation. ||
| Analyse incertitudes |Aucune analyse d'incertitude n'a été réalisée dans cette étude. ||
