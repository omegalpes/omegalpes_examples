# Specific requirements for OMEGAlpes notebooks
Please run the following command in order to get the proper environment for 
the notebooks: 
- In anaconda or any local environment  
`pip install -r .\notebooks\PhD_2022_Sacha_Hodencq\NB_requirements\xxxx_requirements.txt`

- In mybinder  
`import os`  
`os.system('pip install -r NB_requirements/xxxx_requirements.txt')`