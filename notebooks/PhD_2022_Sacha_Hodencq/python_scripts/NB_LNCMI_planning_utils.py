#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Authors : S. Hodencq, J. Fito
"""

# import sys
#
# sys.path.append("..")

import os
import pandas as pd
import matplotlib as plt
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import ShiftableProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import ThermoclineStorage
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import \
    TechnicalDynamicConstraint, Objective, Quantity
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.utils.input_data import \
    select_csv_file_between_dates

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 4, 1)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def power_to_energy_profile_lncmi(lncmi_profile_raw: list,
                                  dh_profile_raw: list,
                                  time_step: float,
                                  elec_to_heat_ratio: float,
                                  p_thresh_reco: float, p_max_recov: float):
    """
    Intermediate tool for switching between power profiles and energy planning
    :param lncmi_profile_raw:
    :param dh_profile_raw:
    :param time_step:
    :param elec_to_heat_ratio:
    :param p_thresh_reco:
    :param p_max_recov:
    :return:
    - lncmi_recoverable_heat,
    - dh_profile_rescaled,
    - dict_lncmi
    """
    dict_lncmi = {}
    lncmi_recoverable_heat = []  # depending on the threshold recovery power
    # value
    dh_profile_rescaled = []  # Adapt target profile to time step

    start = 0
    stop = 0
    marker = 0

    while stop < len(lncmi_profile_raw):
        # a marker is put in place in order to differentiate power profiles
        # extract with same energy values, especially for the last days of
        # the year.
        # TODO: take the markers into account in the reconstruction
        marker += 0.001
        stop += time_step
        run_lncmi_profile = lncmi_profile_raw[start:stop]
        run_dh_profile = dh_profile_raw[start:stop]
        dh_profile_rescaled.append(sum(run_dh_profile))
        run_waste_profile = [elec_to_heat_ratio * p for p in run_lncmi_profile]
        run_recoverable_profile = []

        for w in run_waste_profile:
            if w < p_thresh_reco:
                recoverable = 0
            elif w > p_max_recov:
                recoverable = p_max_recov
            else:
                recoverable = w
            run_recoverable_profile.append(recoverable)

        run_recoverable_energy = round(sum(run_recoverable_profile) + marker,
                                       3)

        dict_lncmi[run_recoverable_energy] = run_lncmi_profile
        lncmi_recoverable_heat.append(run_recoverable_energy)

        start += time_step
    return lncmi_recoverable_heat, dh_profile_rescaled, dict_lncmi


def rearrange_profile(time, profile: list):
    rearranged_profile = []
    index = 1
    for value in profile:
        name = "block_nb_{}".format(index)
        cmd = name + " = VariableProductionUnit(time, '" + name + \
              "', p_min=value/time.DT, p_max=value/time.DT, " \
              "e_min=value, e_max=value)"
        try:
            exec(cmd)
            add_to_rearranged_profile = "rearranged_profile.append" \
                                        "(" + name + ")"
            exec(add_to_rearranged_profile)
        except ValueError:
            print(name + " wasn't added to the model.")
        index += 1
    return rearranged_profile


def rebuild_profile(energy_profile: list, dictionnary: dict):
    lncmi_profile_opt = []
    for ener_tot in energy_profile:
        one_step = dictionnary[ener_tot]
        for p in one_step:
            lncmi_profile_opt.append(p)
    return lncmi_profile_opt


def lncmi_model(dt: float = 1, start_date='01/01/2018 00:00',
                end_date='31/12/2018 23:50',
                lncmi_cons_config: str = 'fixed', lncmi_cons=[], heat_cons=[],
                water_logic_values=[],
                elec_to_heat_ratio: float = 0.85, storage_capa: float = 30,
                e_lncmi: float = 35430000, cciag_pmax: float = 2e4,
                p_thresh_reco: float = 8500, lncmi_power: float = 30,
                e_0_sto: float = None, winter_only: bool = False,
                water_logic: bool = False):
    """
    :param dt: time step 1 = 1 hour, 1/6 = 10 min
    :type dt: float
    :param start_date : start date of the study horizon
    :type start_date: string date 'DD/MM/YYY HH:MM'
    :param end_date : end date of the study horizon
    :type end_date: string date 'DD/MM/YYY HH:MM'
    :param lncmi_cons_config: LNCMI consumption, can be: 'free', 'fixed' (
    i.e. power profiles for a given year) or 'rearranged' (i.e. managing the
    planning at a given time step with the OMEGAlpes model lncmi_simplified
    :type lncmi_cons_config: str
    :param storage_capa: storage capacity (MWh)
    :type storage_capa: int
    :param e_lncmi: Energy which has to be consumed by the LNCMI during the
    time range
    :type e_lncmi: int or float
    :param cciag_pmax: Maximal heating power delivered by the CCIAG to the
    district
    :type cciag_pmax: int or float
    :param p_thresh_reco: Minimal heating power required for the LNCMI to
    ensure constant temperature for the magnet heat
    :type p_thresh_reco: int or float
    :param lncmi_power: max power of the LNCMI magnets, either 24, 30 or 36
    (MW)
    :type lncmi_power: int
    :param e_0_sto: initial storage state of charge (kWh)
    :type e_0_sto: int or float
    :param winter_only: if True, the heat injection on the network is only
    possible during the winter heating period
    :type winter_only: bool
    :param water_logic: water logic of the district heating network taken
    into account for the heat injection from the heat pump
    :type water_logic: bool

    """

    global time, lncmi, district_heat_load, heat_pump, heat_production, \
        heat_node_lncmi, heat_node_network, heat_node_recovery, \
        thermocline_storage, model, dissipation, elec_node, elec_prod

    # --- OPTIMIZATION MODEL CREATION ---
    # Creating the TimeUnit dedicated to time management
    time = TimeUnit(start=start_date, end=end_date, dt=dt)

    # Creating an empty model
    model = OptimisationModel(name='lncmi_model', time=time)

    # --- LNCMI CREATION ---
    # The elec to therm ratio represents the part of the electricity
    # consumed by the LNCMI that is dissipated as heat. This ratio actually
    # represents the part of the electricity of the LNCMI facility that is
    # consumed in the magnets, because all of the electricity in the magnets
    #  is dissipated as heat. The rest of the facility electricity
    # consumption comes from the pumps used in the cooling system as well as
    #  auxiliary electricity consumptions.

    # Ratio between the LNCMI facility electricity consumption, and the
    # magnets' consumption, i.e. the heat dissipated.
    lncmi_power_ratio = lncmi_power / 24  # in order to translate the
    # consumption from 24 MW to 30 or 36 MW for instance

    if lncmi_cons_config == 'fixed':
        lncmi_cons = [lncmi_power_ratio * c for c in lncmi_cons]
        lncmi = ElectricalToThermalConversionUnit(time=time, name='lncmi',
                                                  elec_to_therm_ratio
                                                  =elec_to_heat_ratio,
                                                  p_in_elec=lncmi_cons)
    elif lncmi_cons_config == 'free':
        # Only the annual consumed energy is fixed : this is a fictitious
        # scenario, as if the LNCMI electricity consumption was only used
        # for district heating, without any constraint.
        lncmi = ElectricalToThermalConversionUnit(time, 'lncmi',
                                                  pmax_in_elec=lncmi_power *
                                                               1e3
                                                               + 2e3,
                                                  elec_to_therm_ratio=0.85)
        lncmi.elec_consumption_unit.add_energy_limits_on_time_period(
            e_min=e_lncmi)

    else:
        raise ValueError(
            "The lncmi consumption configuration should be free or fixed and "
            "is set to {0}".format(lncmi_cons_config))

    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          p_max=lncmi_power * 1e3 + 2e3,
                                          energy_type='Thermal')

    # --- STORAGE CREATION ---
    # Storage configurations
    if storage_capa == 0 or None:
        pass
    else:
        capacity = storage_capa * 1e3
        # pc_max = 4e4
        pc_max = round(capacity / 3, 3)
        pd_max = pc_max

        # Creating the thermocline storage
        thermocline_storage = ThermoclineStorage(time, 'thermocline_storage',
                                                 pc_max=pc_max, pd_max=pd_max,
                                                 pc_min=0.001 * pc_max,
                                                 pd_min=0.001 * pd_max,
                                                 capacity=capacity,
                                                 e_0=e_0_sto, Tcycl=120)

        # --- DISTRICT HEAT LOAD CREATION ---
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_cons,
                                              energy_type='Thermal')

    # Carrier LST heat pump
    cop_hp = 3
    t_hp = 85  # District heating injection temperature
    # losses_hp  = 0.016
    pmax_elec = 1260
    pmin_elec = 0.17 * pmax_elec
    # storage_costs = [900, 1800, 2700, 3600]
    # hp_cost = 810362

    # --- WATER LOGIC---
    # Depending on the network temperature, the injection of heat from the
    # heat pumps can be limited. As a matter of fact, if the network
    # temperature is higher than the heat pump output, the LNCMI heat will
    # not be able to fully heat the district network water. This partial
    # heating is considered through the temp_influence_ratio, taking into
    # account the heat_pump outlet temperature and the network two ways
    # temperatures
    if water_logic:
        t_return = 70  # supposed return temperature on the heating network
        pmax_injection = water_logic_constraint(t_return=t_return, t_hp=t_hp,
                                                t_heat_net=water_logic_values,
                                                p_heat_load=heat_cons)
        # Constraint for waste heat recovery in winter only can be considered
        if winter_only:
            for summer_time in range(2161 * int(1 / dt),
                                     7296 * int(1 / dt) + 1):
                # April  -->  October
                pmax_injection[summer_time] = 0
        # Creating the heat pump
        heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                             pmin_in_elec=pmin_elec,
                             pmax_in_elec=pmax_elec,
                             pmax_out_therm=pmax_injection)
    # , losses=losses_hp)
    else:
        pmax_injection = [pmax_elec * cop_hp] * time.LEN

        if winter_only:
            for summer_time in range(2161 * int(1 / dt),
                                     7296 * int(1 / dt) + 1):
                # April  -->  October
                pmax_injection[summer_time] = 0
        # Creating the heat pump
        heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                             pmin_in_elec=pmin_elec,
                             pmax_in_elec=pmax_elec,
                             pmax_out_therm=pmax_injection)

    # Creating the source of electricity for heat pump
    elec_prod = VariableProductionUnit(time, 'elec_prod',
                                       energy_type='Electrical')

    # --- CCIAG PRODUCTION CREATION ---
    #  heat production plants
    heat_production = VariableProductionUnit(time, name='heat_production',
                                             energy_type='Thermal',
                                             p_max=cciag_pmax)  # Creating the

    # --- HEAT NODES CREATION ---
    # Creating the heat node for the energy flows
    # Heat node for the LNCMI current system
    heat_node_lncmi = EnergyNode(time, 'heat_node_lncmi',
                                 energy_type='Thermal')
    # Heat node for the Heat Pump and thermal storage recovery system
    heat_node_recovery = EnergyNode(time, 'heat_node_recovery',
                                    energy_type='Thermal')
    # Heat node for the district heating network
    heat_node_network = EnergyNode(time, 'heat_node_network',
                                   energy_type='Thermal')
    # Electricity node for the electrical consumption from the LNCMI and the
    #  heat pump
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')

    # Connecting units to the nodes
    heat_node_lncmi.connect_units(lncmi.thermal_production_unit,
                                  dissipation)
    heat_node_lncmi.export_to_node(
        heat_node_recovery)  # Export the heat to the recovery system
    if storage_capa == 0 or None:
        heat_node_recovery.connect_units(
            heat_pump.thermal_consumption_unit)
    else:
        heat_node_recovery.connect_units(
            heat_pump.thermal_consumption_unit,
            thermocline_storage)
    elec_node.connect_units(elec_prod, heat_pump.elec_consumption_unit)
    heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                    heat_production, district_heat_load)

    # --- P_THRESH RECOVERY CONSTRAINT ---
    # A constant temperature magnet outlet temperature, necessary for the
    # recovery system, can only be reached above a certain power threshold
    p_thresh_rec_cst_exp = '{0}_therm_prod_p[t] >= {1} * ' \
                           'heat_node_lncmi_is' \
                           '_exporting_to_heat_node_recovery[t]' \
        .format(lncmi.name, p_thresh_reco)
    heat_node_lncmi.export_min = \
        TechnicalDynamicConstraint(name='p_thresh_recovery',
                                   exp_t=p_thresh_rec_cst_exp,
                                   t_range='for t in time.I',
                                   parent=heat_node_lncmi)

    # --- OBJECTIVE CREATION ---
    # Objective : Minimizing the part of the heat load covered by the heat
    # production plants
    heat_production.minimize_production()

    # --- ADDING ALL UNITS TO THE OPTIMIZATION MODEL ---
    model.add_nodes(heat_node_lncmi, heat_node_recovery,
                    heat_node_network, elec_node)
    #
    # # Writing into lp file
    # model.writeLP('optim_models\lncmi_study_case.lp')

    # --- SOLVING AND UPDATING ---
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update
    # values, either with CBC as a default solver, or with GUROBI

    return lncmi, heat_pump, heat_production, district_heat_load, \
           heat_node_lncmi, heat_node_recovery, heat_node_network


def print_results(dt, lncmi, heat_pump, heat_production, district_heat_load,
                  heat_node_lncmi):
    """
        *** This function print the optimisation results:
                - The district heat consumption during the year
                - The LNCMI electrical consumption during the year
                - The CCIAG production during the year
                - The heat from the LNCMI injected on the district heating
                network
                - The heat pump electricity & heat consumptions
                - The percentage of the heat load covererd by the heat pump
                - The percentage of the LNCMI heat that is recovered

            And plot the power curves :
                - Figure 1 : The energy flows between the LNCMI heat
                production, the heat dissipation and the export to the waste
                heat recovery system
                - Figure 2 : The energy flows between the heat imported from
                the LNCMI, the thermocline storage (when there is one) and the
                heat pump heat consumption
                - Figure 3 : The energy flows between the heat pump heat
                production, the CCIAG heat production, and the district heat
                load

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} GWh.'.format(
            round(district_heat_load.e_tot.value / 1e6, 2)))
        print('LNMCI electrical consumption (GWh)= ',
              round(lncmi.elec_consumption_unit.e_tot.value / 1e6, 2))
        print('CCIAG production (GWh)= ', round(heat_production.e_tot.value
                                                / 1e6, 2))
        print('LNMCI heat injected on the district heating network by the '
              'Heat Pump (GWh)= ', round(
            heat_pump.thermal_production_unit.e_tot.value / 1e6, 2))
        print('Heat_pump heat consumption (GWh)= ',
              round(heat_pump.thermal_consumption_unit.e_tot.value / 1e6, 2))
        print('Heat pump electricity consumption (GWh)= ',
              round(heat_pump.elec_consumption_unit.e_tot.value / 1e6, 2))
        print("{0} % of the load coming from the heat pump".format(
            round(heat_pump.thermal_production_unit.e_tot.value /
                  district_heat_load.e_tot.value * 100)))
        # value is a dict, with time as a key, and power levels as values.

        print("{0} % of the LNCMI heat is recovered".format(
            round(sum(
                heat_node_lncmi.energy_export_to_heat_node_recovery
                    .value.values()) * dt /
                  lncmi.thermal_production_unit.e_tot.value * 100)))
        # value is a results_dict, with time as a key, and power levels as
        # values.

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


def plot_results(heat_node_lncmi, heat_node_recovery, heat_node_network):
    plot_node_energetic_flows(heat_node_lncmi)
    plot_node_energetic_flows(heat_node_recovery)
    plot_node_energetic_flows(heat_node_network)
    plt.show()


def water_logic_constraint(t_return: float, t_hp: float, t_heat_net: list,
                           p_heat_load: list):
    """

    :param t_return: return temperature of the district heating network
    :param t_hp: heat pump outlet temperature
    :param t_heat_net: considered heating network temperature
    :param p_heat_load: heat load power profile
    :return: pmax_injection
    """
    temp_influence_ratio = []
    for t_net in t_heat_net:
        temp_influence_ratio += [(t_hp - t_return) / (t_net - t_return)]

    pmax_injection = [round(h_l * t_i_r, 2) for h_l, t_i_r
                      in zip(p_heat_load, temp_influence_ratio)]
    return pmax_injection


def exergy_assessment_heat_recovery(strat, stor_capa):
    """ This is the main function used for the exergy assessmenent. Two input parameters are required: the strategy of
     waste heat management being used (strat), and the capacity of the storage unit in MWh (stor_capa). """

    work_path = os.getcwd()  # get current work path

    # IMPORTING DATA FROM CSV FILES AND GENERATING DATAFRAMES
    if strat == "FLEXENER" or strat == "DOUBLEFLEX":  # annual profiles are different if schedule flexibility is applied
        data_lncmi = "results\lncmi_flows_rearranged_storage_capa_" + stor_capa
    else:
        data_lncmi = "results\lncmi_flows_fixed_storage_capa_" + stor_capa

    df1 = pd.read_csv(work_path + "\\" + data_lncmi + ".csv", sep=';')
    df2 = pd.read_csv(work_path + "\\data\waste_heat_overcool_temperature.csv", sep=';', header=None)

    # ----------------------------------------------------------------------------------------------------------------
    # EXTRACTING THE VARIABLES AS LISTS FROM THE DATAFRAMES  ---------------------------------------------------------
    # ----------------------------------------------------------------------------------------------------------------
    # (Many are unused but kept here for the sake of completeness)

    dissipation = list(df1.dissipation)
    lncmi_therm_prod = list(df1.lncmi_therm_prod)
    heat_pump_therm_cons = list(df1.heat_pump_therm_cons)
    heat_recovery = list(df1.heat_node_lncmi)
    wh_overcool_temp = list(df2[0])

    if hasattr(df1, 'thermocline_storage'):
        storage = list(df1.thermocline_storage)
        # Value of 'storage' is positive when charging the storage, negative when discharging
    else:
        storage = [0] * len(dissipation)

    # ----------------------------------------------------------------------------------------------------------------
    # ASSESSING TOTAL HEAT RECOVERY AND EXERGY DESTRUCTION IN EACH SCENARIO  -----------------------------------------
    # ----------------------------------------------------------------------------------------------------------------

    if strat == "REFERENCE":  # No heat is recovered in the reference scenario
        heat_rejection_temperature = wh_overcool_temp  # Loop is overcooled (< 35 °C) in the reference scenario
        exergy_destruction_by_dissipation = [wh * (1 - (-11 + 273.15) / (wht + 273.15)) for wh, wht in
                                             zip(lncmi_therm_prod, heat_rejection_temperature)]
        # Dead state temperature for exergy analysis is -11 °C (design T in the French RT2012 standard for buildings)
    elif strat == "RECOVERY" or strat == "FLEXENER":  # Heat recovery at 35 °C constant (flexible schedule or not)
        heat_rejection_temperature = [35] * 8760
        exergy_destruction_by_dissipation = [diss * (1 - (-11 + 273.15) / (wht + 273.15)) for diss, wht in
                                             zip(dissipation, heat_rejection_temperature)]
    elif strat == "FLEXTEMP" or strat == "DOUBLEFLEX":  # Heat recovery with intelligent adjustment of loop temperature
        exergy_destruction_by_dissipation = []
        heat_rejection_temperature = []
        for diss, reco, wht in zip(dissipation, heat_recovery, wh_overcool_temp):
            if reco > 0:
                heat_rejection_temperature.append(35)  # Loop is at 35 °C whenever heat recovery is on
                exd_diss = diss * (1 - (-11 + 273.15) / (35 + 273.15))
            else:
                heat_rejection_temperature.append(wht)
                exd_diss = diss * (1 - (-11 + 273.15) / (wht + 273.15))  # Loop is overcooling when no heat recovery
            exergy_destruction_by_dissipation.append(exd_diss)
    else:
        raise ValueError("Invalid scenario name. Scenarios allowed are 'REFERENCE', 'RECOVERY', 'FLEXENER', 'FLEXTEMP'"
                          " or 'DOUBLEFLEX'. You asked for a scenario named '{}'.".format(strat))

    total_heat_recovered = 0 if strat == "REFERENCE" else sum(heat_pump_therm_cons)
    total_exergy_destruction_by_dissipation = sum(exergy_destruction_by_dissipation)

    print("------")
    print("Scenario: {}-{}".format(strat, stor_capa))
    print("Total heat recovered = {} kWh".format(total_heat_recovered))
    print("Total exergy destruction by dissipation = {} kWh".format(total_exergy_destruction_by_dissipation))
    print("------")

    return total_heat_recovered, total_exergy_destruction_by_dissipation