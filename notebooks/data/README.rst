====================================
 Open Data for OMEGAlpes' use cases
====================================

About
-----

Here are data that is used for the OMEGAlpes' use cases presented as notebooks.
See the DATASET_METADATA file for more information.

License
-------

`Open Data Commons Attribution License (ODC-By) v1.0 <LICENSE>`_