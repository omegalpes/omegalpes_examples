#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
     ** This Module is an example of waste energy recovery **

    An electro-intensive industrial process consumes electricity and rejects
    heat. This waste energy is whether recovered or dissipated depending on
    the waste recovery system sizing. A storage system and a heat pump are
    used in order to recover the waste energy, which is then injected on a
    district heat network to provide heat to a district heat load. The
    missing heat will be provided by a district heat network production unit.

    The system includes :
     - An electro-intensive industry
     - A dissipation load
     - A thermal storage system
     - A heat pump
     - A district heat network load
     - A district heat network production unit

     The objective consists in maximizing the recovered waste energy

..

    Copyright 2021 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pulp import LpStatus
import os

__docformat__ = "restructuredtext en"

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 4, 1)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"

# Install corresponding omegalpes version
os.system('pip install omegalpes==' + __version__)

# -----------------------------------------------------------------------------

import omegalpes as omg
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.exergy import *
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import ProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plt, plot_quantity, \
    plot_quantity_bar, plot_node_energetic_flows, plot_pareto2D
from omegalpes.actor.operator_actors.prosumer_actors import Prosumer
from omegalpes.actor.regulator_actors.regulator_actors import RegulatorActor
from omegalpes.actor.operator_actors.grid_operator_actors import HeatGridOperator
from lpfics.lpfics import find_infeasible_constraint_set, find_definition_and_actor_infeasible_constraints_set
from pulp import PULP_CBC_CMD

# -----------------------------------------------------------------------------

# TODO: write docstring ; explain exergy : ref temp and coding parts and values
def waste_heat_rec(work_path, elec2therm_ratio=0.9, pc_max=5000, pd_max=5000,
                   pc_min=1000, pd_min=1000, capa=20000, ramp_limit=20000,
                   self_disch_t=0.01 / 24, cop_hp=3, pmax_elec_hp=1000, pmin_elec_hp = 117,
                   storage_soc_0=0.2, temp_wh=35, obj='exergy', scenario="B",
                   write_lp=True, pareto=False):
    # OPTIMIZATION MODEL
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24 * 7, dt=1)

    # Creating an empty model
    model = OptimisationModel(time=time, name='waste_heat_recovery_model')

    # Exergy reference temperature
    temp_ref_ex = 8
    # Importing time-dependent data from files
    indus_cons_file = open(work_path + "/data/indus_cons_week.txt", "r")
    heat_load_file = open(work_path +
                          "/data/District_heat_load_consumption.txt", "r")

    # Creating the electro-intensive industry unit
    indus_cons = [c for c in map(float, indus_cons_file)]
    indus = ElectricalToThermalConversionUnit(time, 'indus',
                                              elec_to_therm_ratio=
                                              elec2therm_ratio,
                                              p_in_elec=indus_cons)

    # Creating unit for heat dissipation from the industrial process
    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          energy_type='Thermal')

    # Creating the thermal storage
    if obj == "energy" or "actors":
        thermal_storage = StorageUnit(time, 'thermal_storage',
                                      energy_type='Thermal',
                                      ef_is_e0=True)
    else:
        thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pc_max,
                                      energy_type='Thermal', pd_max=pd_max,
                                      pc_min=pc_min, pd_min=pd_min, ef_is_e0=True,
                                      self_disch_t=self_disch_t)

    if obj != "actors":
        # Creating the heat pump
        heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp, pmin_in_elec= pmin_elec_hp,
                             pmax_in_elec=pmax_elec_hp)
    else:
        if scenario == "actor_interaction":
            # Creating the heat pump
            heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp, pmin_in_elec = pmin_elec_hp,
                                 pmax_in_elec=pmax_elec_hp, min_time_on = 2)
        else:
            # Creating the heat pump
            heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp, pmin_in_elec = pmin_elec_hp,
                                 pmax_in_elec=pmax_elec_hp)

    # Creating the district heat load
    heat_load = [c for c in map(float, heat_load_file)]
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_load,
                                              energy_type='Thermal')

    # Creating the heat production plants
    heat_production = ProductionUnit(time,
                                     name='heat_production',
                                     energy_type='Thermal')
    # If you want to assign a ramp limit you can consider the max_ramp_up attribute
    # heat_production = ProductionUnit(time, name='heat_production',
    #                                 max_ramp_up = ramp_limit,
    #                                 energy_type='Thermal')

    # Creating the heat node for the energy flows
    heat_node_indus = EnergyNode(time, 'heat_node_indus',
                                 energy_type='Thermal')
    heat_node_recovery = EnergyNode(time, 'heat_node_recovery',
                                    energy_type='Thermal')
    heat_node_network = EnergyNode(time, 'heat_node_network',
                                   energy_type='Thermal')

    # OBJECTIVE CREATION
    # ENERGY OBJECTIVES
    if obj == 'energy':
        # Connecting units to the nodes
        heat_node_indus.connect_units(indus.thermal_production_unit, dissipation)
        heat_node_recovery.connect_units(thermal_storage,
                                         heat_pump.thermal_consumption_unit)
        heat_node_indus.export_to_node(heat_node_recovery)
        heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                        heat_production, district_heat_load)
        # Minimizing the part of the heat load covered by the heat
        # production plant
        heat_production.minimize_production(weight=1.0,
                                            pareto=pareto)

        # to link it to the power value

        # Minimising the storage capacity. A ratio of 1/3 is chosen between the
        # storage capacity and both the charging and discharging powers, i.e.,
        # for a storage capacity of 3 MWh, the maximal storage power value is
        # 1 MW, or in other word, the minimum charging or discharging time of
        # the storage is 3 hours.
        thermal_storage.minimize_capacity(weight=1.0,
                                          pc_max_ratio=1 / 3,
                                          pd_max_ratio=1 / 3)

        # Adding all nodes (and connected units) to the optimization model
        model.add_nodes(heat_node_indus, heat_node_recovery, heat_node_network)

        # EXERGY OBJECTIVES
        # Minimising the total exergy destruction
    elif obj == "exergy":
        # Exergy formulation
        # dissipation
        ThermalExergy(energy_unit=dissipation, temp_ref=temp_ref_ex,
                      temp_heat=temp_wh)
        ExergyDestruction(energy_unit=dissipation, exergy_eff=0)
        # storage
        ThermalExergy(energy_unit=thermal_storage, temp_heat=temp_wh, temp_ref=8)
        ExergyDestruction(energy_unit=thermal_storage, temp_heat=temp_wh,
                          temp_ref=8)
        # heat pump
        ElectricalExergy(energy_unit=heat_pump.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump.thermal_consumption_unit,
                      temp_heat=temp_wh, temp_ref=8)
        ThermalExergy(energy_unit=heat_pump.thermal_production_unit,
                      temp_heat=85, temp_ref=8)
        ExergyDestruction(energy_unit=heat_pump)
        # heat load
        ThermalExergy(energy_unit=district_heat_load, temp_heat=85, temp_ref=8)
        ExergyDestruction(energy_unit=district_heat_load, exergy_eff=0.726)
        # heat prod
        ThermalExergy(energy_unit=heat_production, temp_heat=120, temp_ref=8)
        ExergyDestruction(energy_unit=heat_production, exergy_eff=0.4)

        # Exergy objectives
        dissipation.minimize_exergy_destruction()
        thermal_storage.minimize_exergy_destruction()
        heat_pump.minimize_exergy_destruction()
        heat_production.minimize_exergy_destruction()
        district_heat_load.minimize_exergy_destruction()

        thermal_storage.minimize_capacity(weight=1.0,
                                          pc_max_ratio=1 / 3,
                                          pd_max_ratio=1 / 3)

        # Connecting units to the nodes
        heat_node_indus.connect_units(indus.thermal_production_unit, dissipation)
        heat_node_indus.export_to_node(heat_node_recovery)
        heat_node_recovery.connect_units(thermal_storage,
                                         heat_pump.thermal_consumption_unit)

        heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                        heat_production, district_heat_load)

        # Adding all nodes (and connected units) to the optimization model
        model.add_nodes(heat_node_indus, heat_node_recovery, heat_node_network)

        # ACTORS OBJECTIVES
        # TODO
    elif obj == "actors":
        # Creating actors for scenario A:
        if scenario[0] == "A" or scenario == "actor_interaction":

            if scenario[2:len(scenario)] == "dissipation":
                dissipation = VariableConsumptionUnit(time, 'dissipation',
                                                      p_max=1500,
                                                      energy_type='Thermal')
            elif scenario[2:len(scenario)] == "CO2":
                dissipation.minimize_co2_emissions(weight=1.00, pareto=False)

            prosumer_indus = Prosumer(name='prosumer_indus',
                                      operated_consumption_unit_list=[dissipation, indus.elec_consumption_unit,
                                                                      heat_pump.thermal_consumption_unit,
                                                                      district_heat_load],
                                      operated_production_unit_list=[indus.thermal_production_unit],
                                      operated_storage_unit_list=[thermal_storage],
                                      operated_node_list=[heat_node_indus, heat_node_recovery])

            # TODO : rampe de démarrage sur le heatproduction (réel) -> contrainte avec marge de sécurité
            heat_network_manager = HeatGridOperator('heat_grid_operator',
                                                    operated_unit_list=[heat_pump.thermal_production_unit,
                                                                        heat_production],
                                                    operated_node_list=[heat_node_network])
            heat_production.minimize_production(weight=0.4,
                                                pareto=pareto)

            prosumer_indus.minimize_capacity(weight=0.6,
                                             pc_max_ratio=1 / 3,
                                             pd_max_ratio=1 / 3,
                                             max_capacity=[CAPA_STORAGE],
                                             pareto=pareto)

            # Connecting units to the nodes
            heat_node_indus.connect_units(indus.thermal_production_unit,
                                          dissipation)
            heat_node_recovery.connect_units(thermal_storage,
                                          heat_pump.thermal_consumption_unit)

            heat_node_indus.export_to_node(heat_node_recovery)  # Export after the valve

            heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                            heat_production, district_heat_load)

        elif scenario[0] == "B":
            prosumer_indus = Prosumer(name='prosumer_indus',
                                      operated_consumption_unit_list=[dissipation, indus.elec_consumption_unit,
                                                                      heat_pump.thermal_consumption_unit,
                                                                      district_heat_load],
                                      operated_production_unit_list=[indus.thermal_production_unit],
                                      operated_storage_unit_list=[],
                                      operated_node_list=[heat_node_indus, heat_node_recovery])

            # TODO : rampe de démarrage sur le heatproduction (réel) -> contrainte avec marge de sécurité
            heat_network_manager = HeatGridOperator('heat_grid_operator',
                                                    operated_unit_list=[heat_pump.thermal_production_unit,
                                                                        heat_production, thermal_storage],
                                                    operated_node_list=[heat_node_network])

            # Connecting units to the nodes
            heat_node_indus.connect_units(indus.thermal_production_unit,
                                          dissipation)
            heat_node_recovery.connect_units(heat_pump.thermal_consumption_unit)

            heat_node_indus.export_to_node(heat_node_recovery)  # Export after the valve

            heat_node_network.connect_units(thermal_storage,
                                            heat_pump.thermal_production_unit,
                                            heat_production, district_heat_load)

            dissipation.minimize_consumption(weight=1.00, pareto=False)

            heat_production.minimize_production(weight=1.0,
                                                pareto=pareto)

            heat_network_manager.minimize_capacity(weight=1.000,
                                                   pc_max_ratio=1 / 3,
                                                   pd_max_ratio=1 / 3,
                                                   max_capacity=[CAPA_STORAGE],

                                                   pareto=pareto)

            # TODO: A discter pour le rajouter dans l'objectif minimize_capacity (si None, min_capacity normal, si défini rajout des deux lignes en dessous
            # operator_storage_cst = ActorConstraint(exp = "{0}_capacity <= {1}".format(thermal_storage.name, CAPA_STORAGE),
            #                                       name = "def_min_capaciry_prosumer",
            #                                       parent = thermal_storage)

            # setattr(thermal_storage, "def_min_capaciry_prosumer", operator_storage_cst)

        # Adding all nodes (and connected units) to the optimization model
        model.add_nodes_and_actors(heat_node_indus, heat_node_recovery, heat_node_network,
                                   prosumer_indus, heat_network_manager)
    else:
        raise ValueError("obj should either be \"energy\", "
                         "\"exergy\" or \"actors\" but is "
                         "set to {0}".format(obj))

    if write_lp:
        model.writeLP(work_path + r'\optim_models\waste_e_recovery.lp')
    # Writing into lp file
    model.solve_and_update(pareto_step=0.2)  # Running optimization and update values

    return model, time, indus, district_heat_load, heat_pump, \
           heat_production, dissipation, thermal_storage, \
           heat_node_indus, heat_node_recovery, \
           heat_node_network


def print_results_waste_heat(model, time, indus, district_heat_load, dissipation,
                             heat_pump, heat_production, thermal_storage,
                             heat_node_indus, heat_node_recovery,
                             heat_node_network):
    """
        *** This function prints the optimisation result:
                - The district consumption during the year
                - The industry consumption during the year
                - The district heat network production during the year
                - The heat exported from the industry
                - The rate of the load covered by the industry

            And plots the power curves :
            On the first figure : the energy out of the industry with the
            recovered and the dissipated parts
            On the second figure: the energy on the district heating network
            with the part produced by the heat pump and
            the part produced by the district heating production unit.

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('Storage capacity = {0} kWh.'.format(
            thermal_storage.capacity.get_value()))
        print('District consumption = {0} kWh.'.format(
            district_heat_load.e_tot))
        print('Industry consumption = {0} kWh.'.format(
            indus.elec_consumption_unit.e_tot))
        print('District heat network production = {0} kWh.'.format(
            heat_production.e_tot))
        print('Industry heat exported = {0} kWh.'.format(
            sum(
                heat_node_indus.energy_export_to_heat_node_recovery
                    .value.values())))
        print('Heat pump electricity consumption = {0} kWh.'.format(
            heat_pump.elec_consumption_unit.e_tot))
        print("{0} % of the load coming from the industry".format(
            round(sum(
                heat_node_indus.energy_export_to_heat_node_recovery
                    .value.values()) /
                  district_heat_load.e_tot.value * 100)))  # value is a dict,
        print('Dissipated heat = {0} kWh.'.format(dissipation.e_tot))

        # with time as a key, and power levels as values.
        print('  ')
        print('--- OVERALL EXERGY DESTRUCTION BALANCE ---')
        exdtot = 0
        if hasattr(indus, 'exd_tot'):
            exdtot = exdtot + indus.exd_tot.value
            print('LNCMI TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(indus.exd_tot.value / 1e3, 2)))
        if hasattr(DISSIPATION, 'exd_tot'):
            exdtot = exdtot + DISSIPATION.exd_tot.value
            print('DISSIPATION TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(DISSIPATION.exd_tot.value / 1e3, 2)))
        if hasattr(THERMAL_STORAGE, 'exd_tot'):
            exdtot = exdtot + THERMAL_STORAGE.exd_tot.value
            print('STORAGE TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(THERMAL_STORAGE.exd_tot.value / 1e3, 5)))
        if hasattr(HEAT_PUMP, 'exd_tot'):
            exdtot = exdtot + HEAT_PUMP.exd_tot.value
            print('HEAT PUMP TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(HEAT_PUMP.exd_tot.value / 1e3, 2)))
        if hasattr(HEAT_PRODUCTION, 'exd_tot'):
            exdtot = exdtot + HEAT_PRODUCTION.exd_tot.value
            print('HEAT PROD TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(HEAT_PRODUCTION.exd_tot.value / 1e3, 2)))
        if hasattr(HEAT_NODE_NET, 'exd_tot'):
            exdtot = exdtot + HEAT_NODE_NET.exd_tot.value
            print('NODE 3 TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(HEAT_NODE_NET.exd_tot.value / 1e3, 2)))
        if hasattr(DISTRICT_HEAT_LOAD, 'exd_tot'):
            exdtot = exdtot + DISTRICT_HEAT_LOAD.exd_tot.value
            print('CONSO TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(DISTRICT_HEAT_LOAD.exd_tot.value / 1e3, 2)))
            print('OVERALL TOTAL EXERGY DESTRUCTION = {0} MWh'.
                  format(round(exdtot / 1e3, 2)))
            # Exergy plot
            fig1 = plt.figure(1)
            ax1 = plt.axes()
            plt.title('OVERALL EXERGY DESTRUCTION')
            plt.xlabel('Time [h]')
            plt.ylabel('Exergy [kW]')
            legend1 = []
            if hasattr(DISSIPATION, 'exergy_dest'):
                plot_quantity(TIME, DISSIPATION.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D dissipation']
            if hasattr(THERMAL_STORAGE, 'exergy_dest'):
                plot_quantity(TIME, THERMAL_STORAGE.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D thermal storage']
            if hasattr(HEAT_PUMP, 'exergy_dest'):
                plot_quantity(TIME, HEAT_PUMP.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D heat pump']
            if hasattr(HEAT_PRODUCTION, 'exergy_dest'):
                plot_quantity(TIME, HEAT_PRODUCTION.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D HEAT PROD']
            if hasattr(HEAT_NODE_NET, 'exergy_dest'):
                plot_quantity(TIME, HEAT_NODE_NET.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D node 3']
            if hasattr(DISTRICT_HEAT_LOAD, 'exergy_dest'):
                plot_quantity(TIME, DISTRICT_HEAT_LOAD.exergy_dest, fig1, ax1)
                legend1 += ['Ex_D district heating']
            plt.legend(legend1)

        # SHOW THE GRAPH
        # Storage SOC
        plot_quantity_bar(time=time, quantity=thermal_storage.e,
                          title="State of charge of the thermal storage")
        # Recovered and dissipated heat
        plot_node_energetic_flows(heat_node_indus)

        # Energy on the recovery system
        plot_node_energetic_flows(heat_node_recovery)

        # Energy on the district heating network
        plot_node_energetic_flows(heat_node_network)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")
        find_definition_and_actor_infeasible_constraints_set(model)

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("If not a pareto evaluation."
              "Sorry, the optimisation problem has not been solved.")
        plot_pareto2D(model,
                      thermal_storage.capacity,
                      heat_production.e_tot,
                      title="Pareto between thermal capacity and heat production",
                      legend_on=False)


if __name__ == "__main__":
    # *** OPTIMIZATION PARAMETERS *** #
    WORK_PATH = os.getcwd()
    SCENARIO = "A_dissipation"  # either A_dissipation or A_CO2 (prosumer) or B_minproduction or B_shift (operator)
    OBJ = "energy"  # either energy, exergy or actors

    # --- Time Schedule for thermal grid operator ---
    # Define the hour when the peak of thermal consumpation are foreseen
    MAX_PEAK_SCHEDULE = [6, 7, 8, 18, 19, 20, 21]

    # --- Electricity-to-Thermal conversion ---
    # 90% of the electrical consumption is converted into heat
    ELEC_TO_HEAT_RATIO = 0.9

    CAPA_STORAGE = 25000 # Storage capacity of 25MWh
    SOC_0_STORAGE = None  # Initial state of charge of 10%

    # --- Thermal storage parameters ---
    # The maximal charging and discharging powers both equal 5 MW
    PC_MAX_STORAGE = PD_MAX_STORAGE = CAPA_STORAGE / 3

    # When charging/discharging, the power should at least be 20% of the
    # maximal charging/discharging powers
    PC_MIN_STORAGE = PD_MIN_STORAGE = 0.05 * PC_MAX_STORAGE

    # --- Heat pump parameters ---
    COP = 3  # The coefficient of performance equals 3
    P_MAX_HP = 1e3  # The heat pump has a electrical power limit of 1 MW
    P_MIN_HP = 0.2 * P_MAX_HP # The heat pump has minimal power of usage

    # *** RUN MAIN ***
    MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD, HEAT_PUMP, HEAT_PRODUCTION, \
    DISSIPATION, THERMAL_STORAGE, HEAT_NODE_INDUS, HEAT_NODE_RECO, \
    HEAT_NODE_NET = waste_heat_rec(
        work_path=WORK_PATH, elec2therm_ratio=ELEC_TO_HEAT_RATIO,
        pc_max=PC_MAX_STORAGE, pd_max=PD_MAX_STORAGE, pc_min=PC_MIN_STORAGE,
        pd_min=PD_MIN_STORAGE, self_disch_t=0.01 / 24, capa=CAPA_STORAGE,
        cop_hp=COP,
        pmax_elec_hp=P_MAX_HP, storage_soc_0=SOC_0_STORAGE,
        temp_wh=35, obj=OBJ, scenario=SCENARIO, write_lp=False, pareto=False)

    # *** SHOW THE RESULTS ***
    print_results_waste_heat(MODEL, TIME, INDUS, DISTRICT_HEAT_LOAD, DISSIPATION, HEAT_PUMP,
                             HEAT_PRODUCTION, THERMAL_STORAGE,
                             HEAT_NODE_INDUS, HEAT_NODE_RECO,
                             HEAT_NODE_NET)
