#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
Study case for the 2020 IBPSA conference
Article ""

..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import os
from pulp import LpStatus, GUROBI_CMD
from omegalpes.actor.operator_actors.consumer_producer_actors import \
    Prosumer, Supplier
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.units.storage_units import *
from omegalpes.energy.units.conversion_units import *
from omegalpes.general.optimisation.model import OptimisationModel, \
    compute_gurobi_IIS
from omegalpes.general.utils.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.utils.input_data import *
from omegalpes.general.time import TimeUnit
from LPFICS.lpfics.prefered_lpfics import *
import time as pytime

# ------------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# ------------------------------------------------------------------------------

def main(work_path):
    # --- OPTIMIZATION MODEL CREATION AND DATA IMPORT ---
    ## Creating the unit dedicated to time management ##
    # Considering a week period with a time step of half an hour
    # The time step is based on the time step of the data collected
    # for the electrical consumptions
    time = TimeUnit(periods=24 * 7 * 2, dt=0.5)

    # Creating an empty model
    model = OptimisationModel(time=time,
                              name=
                              'article_2020_IBPSA_constraints_identification')

    ## Importing time-dependent data ##
    # Importing pv production 11-17 may 2016 (no data in 2019) from PVGIS #
    #   https://re.jrc.ec.europa.eu/pvg_tools/en/tools.html#HR
    #    Hourly radiation data (then divided by two to convert it into
    #    half-an-hour data)
    #    Solar radiation data: PVGIS-SARAH, start and end year: 2019
    #    Mounting type: Fixed; slope 15°; azimuth 0°;
    #    PV power; PV technology : crystalline silicon;
    #    installed peak PV power: 0.250 kWc; system loss 0%
    pv_production = read_enedis_data_csv_file(
        file_path=work_path +
                  "/data/Timeseries_45.173_5.724_SA_0"
                  ".250kWp_crystSi_0_15deg_0deg_2016_2016.csv")

    # Importing electrical load consumption 11-17 may 2019 from enedis #
    #   https://www.enedis.fr/agregats-segmentes-de-consommation-et-production
    #    -electriques-au-pas-12-h
    #    To find the data, the following configuration have been followed:
    #    Consommation <36kVA; courbe moyenne: Wh;
    #    Sélectionner le national ou une région : Auvergne-Rhône-Alpes;
    #    Profil : RES1 (+RES1WE), plage de puissance souscrite: P2:]3-6kVA[
    consumption_day = read_enedis_data_csv_file(
        file_path=work_path + "/data/Dwelling_elec_consumption_3-6kVA_11-17"
                              "-2019.txt")

    # Importing heat consumption data #
    # Only domestic hot water , no heating system as the study is in may
    heat_consumption_day_file = open(
        work_path + "/data/Dwelling_domestic_hot_water_consumption.txt", "r")
    heat_consumption_day = [p for p in map(float, heat_consumption_day_file)]

    # --- ENERGY UNIT CREATION AND CONFIGURATION---
    ## Creating the dwelling loads for electricity and heat ##
    # as FixedConsumptionUnits based on the data above

    # Dwelling 1 #
    elec_consumption_1 = \
        FixedConsumptionUnit(time, name='elec_consumption_1',
                             p=consumption_day,
                             energy_type='Electrical')
    heat_consumption_1 = FixedConsumptionUnit(time, name='heat_consumption_1',
                                              p=heat_consumption_day,
                                              energy_type='Thermal')
    # Dwelling 2 #
    elec_consumption_2 = \
        FixedConsumptionUnit(time, name='elec_consumption_2',
                             p=consumption_day,
                             energy_type='Electrical')
    heat_consumption_2 = FixedConsumptionUnit(time, name='heat_consumption_2',
                                              p=heat_consumption_day,
                                              energy_type='Thermal')

    # Dwelling 3 #
    elec_consumption_3 = \
        FixedConsumptionUnit(time, name='elec_consumption_3',
                             p=consumption_day,
                             energy_type='Electrical')
    heat_consumption_3 = FixedConsumptionUnit(time, name='heat_consumption_3',
                                              p=heat_consumption_day,
                                              energy_type='Thermal')

    # Creating the PV production #
    # As a SeveralProductionUnit of 250 Wc in order to have the possibility to
    # have several of it
    local_pv_production = SeveralProductionUnit(time=time,
                                                name='local_pv_production',
                                                fixed_prod=pv_production,
                                                energy_type='Electrical')

    # Creating a heat-pump #
    heat_pump = HeatPump(time=time, name='heat_pump')
    # Adding a constraint on the minimal time when the HeatPump should
    # fonction once it is started-up
    heat_pump.elec_consumption_unit.add_min_time_on(min_time_on=2)

    # Creating the energy units corresponding to the supply and the recovery
    #  to the local project #
    elec_supply = VariableProductionUnit(time=time, name='elec_supply',
                                         energy_type='Electrical')
    elec_recovery = VariableConsumptionUnit(time=time,
                                            name='elec_recovery',
                                            energy_type='Electrical')

    # --- NODES CREATION AND CONFIGURATION ---
    # ELECTRICAL NODE #
    # Creating the electrical nodes for the energy flows repartition #
    elec_node = EnergyNode(time=time, name='elec_node',
                           energy_type='Electrical')

    # Connecting units to the node #
    elec_node.connect_units(elec_consumption_1,
                            heat_pump.elec_consumption_unit,
                            elec_consumption_2,
                            elec_consumption_3,
                            local_pv_production,
                            elec_supply, elec_recovery)

    # HEAT NODE #
    # Creating heat nodes for each dwelling linked to the heat pump and
    # connected the heat pump and the dwelling to the node

    # Heat Node Dwelling 1
    heat_node_dwelling1 = EnergyNode(time=time, name='dwelling1_heat_node',
                                     energy_type='Thermal')
    heat_node_dwelling1.connect_units(heat_pump.thermal_production_unit,
                                      heat_consumption_1)

    # Heat Node Dwelling 2
    heat_node_dwelling2 = EnergyNode(time=time, name='dwelling2_heat_node',
                                     energy_type='Thermal')
    heat_node_dwelling2.connect_units(heat_pump.thermal_production_unit,
                                      heat_consumption_2)

    # Heat Node Dwelling 3
    heat_node_dwelling3 = EnergyNode(time=time, name='dwelling3_heat_node',
                                     energy_type='Thermal')
    heat_node_dwelling3.connect_units(heat_pump.thermal_production_unit,
                                      heat_consumption_3)

    # --- ACTOR CREATION AND CONFIGURATION ---
    # Creating the "actors" involved in the project #
    prosumers = Prosumer(name='prosumer',
                         operated_consumption_unit_list=[elec_consumption_1,
                                                         elec_consumption_2,
                                                         elec_consumption_3],
                         operated_production_unit_list=[local_pv_production])

    supplier = Supplier(name='supplier',
                        operated_consumption_unit_list=[elec_recovery],
                        operated_production_unit_list=[elec_supply])

    # Adding actors' constraints #
    # Supplier's constraints

    # The supplier does not want any injection on the network
    supplier.add_actor_dynamic_constraint(cst_name='no_injection',
                                          exp_t='elec_recovery_p[t]'
                                                '== 0')

    # Prosumers' constraints
    # Scenario 1 : The prosumers want at maximum 200 modules of 250Wc PV
    # pannels corresponding to around 50kWc
    # ***REMOVE COMMENTS HERE***
    prosumers.add_actor_constraint(cst_name='number_pv',
                                   exp='local_pv_production_nb_unit<=200')

    # Scenario 2 : The supplier wants 200 modules of 250 Wc PV pannels
    # corresponding to around 50kWc
    # ***REMOVE COMMENTS HERE***
    # prosumer.add_actor_constraint(cst_name='number_pv',
    #                               exp='local_pv_production_nb_unit==200')

    # Adding actors' objectives #
    # Prosumers' Objective
    # Scenario 1 : The prosumers want to maximise the local production
    prosumers.maximize_production(obj_operated_unit_list=[local_pv_production])

    # Adding the energy nodes and the actors to the model #
    model.add_nodes_and_actors(elec_node,
                               heat_node_dwelling1,
                               heat_node_dwelling2,
                               heat_node_dwelling3,
                               prosumers, supplier)

    # --- OPTIMISATION PROCESS ---
    # Creating an .lp file in which the model is described for the solver
    model.writeLP('optim_models/article_2020_IBPSA_constraints_identification'
                  '.lp')

    # Choosing a solver #
    # COIN-CBC Solver #
    # Run optimization with COIN-CBC solver and update values
    # ***REMOVE COMMENTS HERE***
    model.solve_and_update()

    # GUROBI Solver #
    # Run optimization with Gurobi solver
    # Adding times to evaluate the time of the resolution

    # ***REMOVE COMMENTS HERE***
    # start_solve = pytime.time()
    # model.solve(GUROBI_CMD())
    # solve_time = pytime.time()
    # print('solve_gurobi duration', solve_time -
    #       start_solve)

    # Choosing the method of constraints identification #
    # LPFICS #
    # run LPFICS algorithm
    # Adding times to evaluate the time of the identification
    LPFICS_start = pytime.time()

    # V1 - basic algorithm
    # ***REMOVE COMMENTS HERE***
    find_infeasible_constraint_set(model)

    # V2 - remove technical constraints and test identify actors' constraints
    #  if the problem is still infeasible
    # ***REMOVE COMMENTS HERE***
    # find_definition_and_actor_infeasible_constraints_set(model)

    # V3 - remove actors' constraints and test identify technical constraints
    #  if the problem is still infeasible
    # ***REMOVE COMMENTS HERE***
    # find_definition_and_technical_infeasible_constraints_set(model)

    LPFICS_time = pytime.time()
    print('LPFICS duration', LPFICS_time - LPFICS_start)

    # GUROBI #
    # run ComputeISS algorithm for Gurobi
    # ***REMOVE COMMENTS HERE***
    # gurobi_start = pytime.time()
    # compute_gurobi_IIS(opt_model=model)
    # Gurobi_time = pytime.time()
    # print('gurobi IIS duration', Gurobi_time - gurobi_start)

    return time, model, \
           elec_consumption_1, heat_consumption_1, \
           elec_consumption_2, heat_consumption_2, \
           elec_consumption_3, heat_consumption_3, \
           local_pv_production, heat_pump, \
           elec_supply, elec_recovery, \
           elec_node, \
           heat_node_dwelling1, heat_node_dwelling2, heat_node_dwelling3, \
           prosumers, supplier


def print_results(time, model, \
                  elec_consumption_1, heat_consumption_1, \
                  elec_consumption_2, heat_consumption_2, \
                  elec_consumption_3, heat_consumption_3, \
                  local_pv_production, heat_pump, \
                  elec_supply, elec_recovery, \
                  elec_node, \
                  heat_node_dwelling1, heat_node_dwelling2,
                  heat_node_dwelling3, \
                  prosumer, supplier):
    print("\n - - - - - ACTORS OBJECTIVES & CONSTRAINTS - - - - - ")
    print(
        "supplier constraints {}".format(supplier.get_constraints_name_list()))
    print("supplier objectives {}".format(supplier.get_objectives_name_list()))
    print(
        "prosumer constraints {}".format(prosumer.get_constraints_name_list()))
    print("prosumer objectives {}".format(prosumer.get_objectives_name_list()))

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print("number of pv panels = {}".format(local_pv_production.nb_unit))
        print('dwelling elec consumption 1= {0} kWh.'.format(
            elec_consumption_1.e_tot))
        print('dwelling elec consumption 2= {0} kWh.'.format(
            elec_consumption_2.e_tot))
        print('dwelling elec consumption 3= {0} kWh.'.format(
            elec_consumption_3.e_tot))
        print('dwelling heat consumption 1= {0} kWh.'.format(
            heat_consumption_1.e_tot))
        print('dwelling heat consumption 2= {0} kWh.'.format(
            heat_consumption_2.e_tot))
        print('dwelling heat consumption 3= {0} kWh.'.format(
            heat_consumption_3.e_tot))
        print('PV production = {0} kWh'.format(local_pv_production.e_tot))
        print('HeatPump consumption = {0} kWh'.format(
            heat_pump.elec_consumption_unit.e_tot))
        print('electricity supply = {0} kWh'.format(elec_supply.e_tot))
        print('electricity recovery = {0} kWh'.format(elec_recovery.e_tot))

        plot_node_energetic_flows(elec_node).show()
        plot_node_energetic_flows(heat_node_dwelling1).show()
        plot_node_energetic_flows(heat_consumption_2).show()
        plot_node_energetic_flows(heat_consumption_3).show()


    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the problem is infeasible !")


if __name__ == "__main__":
    WORK_PATH = os.getcwd()

    time, model, \
    elec_consumption_1, heat_consumption_1, \
    elec_consumption_2, heat_consumption_2, \
    elec_consumption_3, heat_consumption_3, \
    local_pv_production, heat_pump, elec_supply, elec_recovery, \
    elec_node, heat_node_dwelling1, heat_node_dwelling2, heat_node_dwelling3, \
    prosumer, supplier = main(work_path=WORK_PATH)

    print_results(time, model, \
                  elec_consumption_1, heat_consumption_1, \
                  elec_consumption_2, heat_consumption_2, \
                  elec_consumption_3, heat_consumption_3, \
                  local_pv_production, heat_pump, elec_supply, elec_recovery, \
                  elec_node, heat_node_dwelling1, heat_node_dwelling2,
                  heat_node_dwelling3, \
                  prosumer, supplier)
