#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
Study case submitted to Energy Conversion and Management journal.
Reference:

Fitó J., Hodencq S., Ramousse J., Würtz F., Stutz B., Debray F., Vincent B.
'Energy- and exergy-based optimal designs of a low-temperature industrial
 waste heat recovery system in district heating'

"""

from pulp import LpStatus, GUROBI_CMD
import os
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.exergy import *
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plt, plot_node_energetic_flows, \
    plot_quantity

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def main(work_path, elec2heat_ratio=0.85, pc_max=6700, pd_max=6700,
         pc_min=1000, pd_min=1000, e_max=20000, self_disch_t=0.01 / 24,
         cop_hp=3.25, pmax_elec_hp=1140, storage_soc_0=0.2, temp_wh=35,
         pmax_bypass=249.2):
    # OPTIMIZATION MODEL ------------------------------------------------------
    time = TimeUnit(periods=8760, dt=1)
    model = OptimisationModel(time=time, name='waste_e_recovery_model')

    # DATA TO IMPORT ----------------------------------------------------------
    lncmi_cons_file = open(work_path + "/lncmi_cons_year.txt", "r")
    lncmi_cons = [c for c in map(float, lncmi_cons_file)]
    heat_load_file = open(work_path + "/heat_load_year.txt", "r")
    heat_load = [c for c in map(float, heat_load_file)]

    # INDUSTRIAL WASTE HEAT REJECTION UNIT (LNCMI) ----------------------------
    lncmi = ElectricalToThermalConversionUnit(
        time, 'lncmi', elec_to_therm_ratio=elec2heat_ratio,
        p_in_elec=lncmi_cons, pmax_out_therm=2e5)
    lncmi.thermal_production_unit.e_tot.ub = 5e7

    # WASTE HEAT DISSIPATION UNIT (ISERE RIVER) -------------------------------
    dissipation = VariableConsumptionUnit(time, 'dissipation', p_max=2e5,
                                          energy_type='Thermal', e_max=5e7)
    ThermalExergy(energy_unit=dissipation, temp_ref=8, temp_heat=temp_wh)
    ExergyDestruction(energy_unit=dissipation, exergy_eff=0)

    # THERMAL ENERGY STORAGE UNIT ---------------------------------------------
    thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=pc_max,
                                  energy_type='Thermal', pd_max=pd_max,
                                  pc_min=pc_min, pd_min=pd_min, capacity=e_max,
                                  e_0=storage_soc_0 * e_max, ef_is_e0=True,
                                  self_disch_t=self_disch_t)
    ThermalExergy(energy_unit=thermal_storage, temp_heat=temp_wh, temp_ref=8)
    ExergyDestruction(energy_unit=thermal_storage, temp_heat=temp_wh,
                      temp_ref=8)

    # HEAT PUMP ---------------------------------------------------------------
    heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                         pmax_in_elec=pmax_elec_hp)
    heat_pump.thermal_consumption_unit.e_tot.ub = 5e7
    heat_pump.elec_consumption_unit.e_tot.ub = 5e7
    heat_pump.thermal_production_unit.e_tot.ub = 5e7

    ElectricalExergy(energy_unit=heat_pump.elec_consumption_unit)
    ThermalExergy(energy_unit=heat_pump.thermal_consumption_unit,
                  temp_heat=temp_wh, temp_ref=8)
    ThermalExergy(energy_unit=heat_pump.thermal_production_unit,
                  temp_heat=85, temp_ref=8)
    ExergyDestruction(energy_unit=heat_pump)

    # HEAT SUPPLIER -----------------------------------------------------------
    heat_production = VariableProductionUnit(time, name='heat_production',
                                             energy_type='Thermal', e_max=5e7,
                                             p_max=2e4)
    ThermalExergy(energy_unit=heat_production, temp_heat=120, temp_ref=8)
    ExergyDestruction(energy_unit=heat_production, exergy_eff=0.4)

    # DISTRICT HEATING CONSUMPTION (RESIDENTIAL NEEDS) ------------------------
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=heat_load,
                                              energy_type='Thermal')
    ThermalExergy(energy_unit=district_heat_load, temp_heat=85, temp_ref=8)
    ExergyDestruction(energy_unit=district_heat_load, exergy_eff=0.726)

    # THERMAL ENERGY NODES ----------------------------------------------------
    heat_node_bef_valve = EnergyNode(time, 'heat_node_bef_valve',
                                     energy_type='Thermal')
    heat_node_aft_valve = EnergyNode(time, 'heat_node_aft_valve',
                                     energy_type='Thermal')
    heat_node_aft_hp = EnergyNode(time, 'heat_node_aft_hp',
                                  energy_type='Thermal')

    # CONNECTIONS BETWEEN NODES AND UNITS, EXPORTS ----------------------------
    heat_node_bef_valve.connect_units(lncmi.thermal_production_unit,
                                      dissipation)
    heat_node_bef_valve.export_to_node(heat_node_aft_valve, export_max=2e5)
    heat_node_aft_valve.connect_units(heat_pump.thermal_consumption_unit,
                                      thermal_storage)
    heat_node_aft_valve.export_to_node(heat_node_aft_hp,
                                       export_max=pmax_bypass)
    heat_node_aft_hp.connect_units(heat_pump.thermal_production_unit,
                                   heat_production, district_heat_load)

    # ENERGY-BASED OPTIMIZATION OBJECTIVES ------------------------------------
    # dissipation.minimize_consumption()
    # heat_production.minimize_production()

    # EXERGY-BASED OPTIMIZATION OBJECTIVES ------------------------------------
    dissipation.minimize_exergy_destruction()
    thermal_storage.minimize_exergy_destruction()
    heat_pump.minimize_exergy_destruction()
    heat_production.minimize_exergy_destruction()
    district_heat_load.minimize_exergy_destruction()

    # ASSEMBLY OF THE OPTIMIZATION MODEL AND CALL TO THE OPTIMIZER ------------
    model.add_nodes(heat_node_bef_valve, heat_node_aft_valve, heat_node_aft_hp)
    model.writeLP(work_path + '\waste_heat_recovery.lp')
    # model.solve_and_update()
    model.solve_and_update(GUROBI_CMD())

    return model, time, lncmi, district_heat_load, heat_pump, \
           heat_production, \
           dissipation, thermal_storage, heat_node_bef_valve, \
           heat_node_aft_valve, heat_node_aft_hp


def print_results():
    # Print results
    if LpStatus[MODEL.status] == 'Optimal':
        print('  ')
        print('--- OVERALL ENERGY BALANCE ---')
        print('LNCMI TOTAL HEAT REJECTION = {0} GWh'.
              format(
            round(LNCMI.thermal_production_unit.e_tot.value / 1e6, 2)))
        print('DISSIPATION TOTAL HEAT CONSUMPTION = {0} GWh'.
              format(round(DISSIPATION.e_tot.value / 1e6, 2)))
        print('Industry heat exported = {0} GWh.'.format(
            round(sum(HEAT_NODE_BEF_VALVE.energy_export_to_heat_node_aft_valve
                      .value.values()) / 1e6, 2)))
        print('Total heat by-passed = {0} GWh.'.format(
            round(sum(HEAT_NODE_AFT_VALVE.energy_export_to_heat_node_aft_hp
                      .value.values()) / 1e6, 2)))
        print('HEAT PUMP TOTAL HEAT CONSUMPTION = {0} GWh'.
            format(
            round(HEAT_PUMP.thermal_consumption_unit.e_tot.value / 1e6, 2)))
        print('HEAT PUMP TOTAL ELECTRICITY CONSUMPTION = {0} GWh'.
            format(
            round(HEAT_PUMP.elec_consumption_unit.e_tot.value / 1e6, 2)))
        print('HEAT PUMP TOTAL HEAT PRODUCTION = {0} GWh'.
            format(
            round(HEAT_PUMP.thermal_production_unit.e_tot.value / 1e6, 2)))
        print('CCIAG TOTAL HEAT PRODUCTION = {0} GWh'.
              format(round(HEAT_PRODUCTION.e_tot.value / 1e6, 2)))
        print('CONSO TOTAL HEAT CONSUMPTION = {0} GWh'.
              format(round(DISTRICT_HEAT_LOAD.e_tot.value / 1e6, 2)))

        print('  ')
        print('--- OVERALL EXERGY DESTRUCTION BALANCE ---')
        exdtot = 0
        if hasattr(LNCMI, 'exd_tot'):
            exdtot = exdtot + LNCMI.exd_tot.value
            print('LNCMI TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(LNCMI.exd_tot.value / 1e6, 2)))
        if hasattr(DISSIPATION, 'exd_tot'):
            exdtot = exdtot + DISSIPATION.exd_tot.value
            print('DISSIPATION TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(DISSIPATION.exd_tot.value / 1e6, 2)))
        if hasattr(THERMAL_STORAGE, 'exd_tot'):
            exdtot = exdtot + THERMAL_STORAGE.exd_tot.value
            print('STORAGE TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(THERMAL_STORAGE.exd_tot.value / 1e6, 5)))
        if hasattr(HEAT_PUMP, 'exd_tot'):
            exdtot = exdtot + HEAT_PUMP.exd_tot.value
            print('HEAT PUMP TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(HEAT_PUMP.exd_tot.value / 1e6, 2)))
        if hasattr(HEAT_PRODUCTION, 'exd_tot'):
            exdtot = exdtot + HEAT_PRODUCTION.exd_tot.value
            print('CCIAG TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(HEAT_PRODUCTION.exd_tot.value / 1e6, 2)))
        if hasattr(HEAT_NODE_AFT_HP, 'exd_tot'):
            exdtot = exdtot + HEAT_NODE_AFT_HP.exd_tot.value
            print('NODE 3 TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(HEAT_NODE_AFT_HP.exd_tot.value / 1e6, 2)))
        if hasattr(DISTRICT_HEAT_LOAD, 'exd_tot'):
            exdtot = exdtot + DISTRICT_HEAT_LOAD.exd_tot.value
            print('CONSO TOTAL EXERGY DESTRUCTION = {0} GWh'.
                  format(round(DISTRICT_HEAT_LOAD.exd_tot.value / 1e6, 2)))
        print('OVERALL TOTAL EXERGY DESTRUCTION = {0} GWh'.
              format(round(exdtot / 1e6, 2)))

        fig1 = plt.figure(1)
        ax1 = plt.axes()
        plt.title('OVERALL EXERGY DESTRUCTION')
        plt.xlabel('Time [h]')
        plt.ylabel('Exergy [kW]')
        legend1 = []
        if hasattr(DISSIPATION, 'exergy_dest'):
            plot_quantity(TIME, DISSIPATION.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D dissipation']
        if hasattr(THERMAL_STORAGE, 'exergy_dest'):
            plot_quantity(TIME, THERMAL_STORAGE.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D thermal storage']
        if hasattr(HEAT_PUMP, 'exergy_dest'):
            plot_quantity(TIME, HEAT_PUMP.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D heat pump']
        if hasattr(HEAT_PRODUCTION, 'exergy_dest'):
            plot_quantity(TIME, HEAT_PRODUCTION.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D CCIAG']
        if hasattr(HEAT_NODE_AFT_HP, 'exergy_dest'):
            plot_quantity(TIME, HEAT_NODE_AFT_HP.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D node 3']
        if hasattr(DISTRICT_HEAT_LOAD, 'exergy_dest'):
            plot_quantity(TIME, DISTRICT_HEAT_LOAD.exergy_dest, fig1, ax1)
            legend1 += ['Ex_D district heating']
        plt.legend(legend1)

        fig2 = plt.figure(2)
        ax2 = plt.axes()
        plt.title('STORAGE')
        plt.xlabel('Time [h]')
        plt.ylabel('Energy [kWh]')
        legend2 = []
        plot_quantity(TIME, THERMAL_STORAGE.e, fig2, ax2)
        legend2 += ['Stored energy']
        if hasattr(THERMAL_STORAGE, 'exergy_dest_f'):
            plot_quantity(TIME, THERMAL_STORAGE.exergy, fig2, ax2)
            legend2 += ['Stored exergy']
            plot_quantity(TIME, THERMAL_STORAGE.exergy_dest, fig2, ax2)
            legend2 += ['Exergy destruction']
        plt.legend(legend2)

        plot_node_energetic_flows(HEAT_NODE_BEF_VALVE)
        plot_node_energetic_flows(HEAT_NODE_AFT_VALVE)
        plot_node_energetic_flows(HEAT_NODE_AFT_HP)
        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")
    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")
    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")
    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    # *** OPTIMIZATION PARAMETERS *** #
    WORK_PATH = os.getcwd()

    """ These parameters are common to all scenarios considered """
    ELEC_TO_HEAT_RATIO = 0.85
    CAPA_STORAGE = 40000  # Storage capacity (kWh) -- Enter 0 if no storage
    PC_MAX_STORAGE = CAPA_STORAGE / 3
    PD_MAX_STORAGE = CAPA_STORAGE / 3
    PC_MIN_STORAGE = PD_MIN_STORAGE = 200
    SOC_0_STORAGE = 0  # Initial state of charge of 0%

    """ SCENARIOS CONSIDERED:
     1 ) The LNMCI rejects heat at 35 °C and this heat is recovered with a
         thermal storage unit and a heat pump that increases the temperature
         of the waste heat from 35 °C to 85 °C.
     2 ) The LNMCI rejects heat at 50 °C and this heat is recovered with a
         thermal storage unit and a heat pump that increases the temperature
         of the waste heat from 50 °C to 85 °C.
     3 ) The LNCMI rejects heat at 85 °C. In this case, the waste heat is at
         the same temperature as the District Heating Network. Therefore, the
         waste heat is directly injected into the DHN and no heat pump is
         necessary. A thermal storage is still used, nevertheless.    
     """

    "Choose the design scenario by uncommenting it and commenting the others"
    # -- SCENARIO 1: Heat pump from 35 °C TO 85 °C --
    # P_MAX_HP = 1260
    # COP = 3
    # WASTE_HEAT_TEMP = 35
    # STORAGE_LOSSES = 0.01/24  # %/day based on the energy stored currently
    # P_MAX_BYPASS = 0  # No heat is by-passed directly from LNCMI to the DHN
    # # P_MAX_BYPASS = 249.2 # Low-T by-pass to pre-heat DHW from 10 °C to
    # 45 °C

    # -- SCENARIO 2: Heat pump from 50 °C TO 85 °C --
    # P_MAX_HP = 766  # FOR Q_IN (50 °C) = Q_IN (35 °C)
    # P_MAX_HP = 1260  # FOR P_MAX_HP (T_in = 50 °C) = P_MAX_HP (T_in = 35 °C)
    P_MAX_HP = 881  # FOR Q_OUT (T_in = 50 °C) = Q_OUT (T_in = 35 °C)
    COP = 4.29
    WASTE_HEAT_TEMP = 50
    STORAGE_LOSSES = 0.02 / 24  # %/day based on the energy stored currently
    P_MAX_BYPASS = 0  # No Low-T by-pass
    # P_MAX_BYPASS = 436.1  # Low-T by-pass to pre-heat DHW from 10 °C to 45 °C

    # -- SCENARIO 3: No heat pump (Waste heat at 85 °C and by-passed to DHN) --
    # P_MAX_HP = 0
    # COP = 1.5
    # WASTE_HEAT_TEMP = 85
    # STORAGE_LOSSES = 0.0433 / 24  # %/day based on the energy stored
    # currently
    # P_MAX_BYPASS = 50000  # Unlimited by-pass
    # # P_MAX_BYPASS = 1260 * 4.29 # By-pass limited to Q_out_HP

    # *** RUN MAIN ***
    MODEL, TIME, LNCMI, DISTRICT_HEAT_LOAD, HEAT_PUMP, HEAT_PRODUCTION, \
    DISSIPATION, THERMAL_STORAGE, HEAT_NODE_BEF_VALVE, HEAT_NODE_AFT_VALVE, \
    HEAT_NODE_AFT_HP = main(
        work_path=WORK_PATH, elec2heat_ratio=ELEC_TO_HEAT_RATIO,
        pc_max=PC_MAX_STORAGE, self_disch_t=STORAGE_LOSSES,
        pd_max=PD_MAX_STORAGE, pc_min=PC_MIN_STORAGE,
        pd_min=PD_MIN_STORAGE, e_max=CAPA_STORAGE, cop_hp=COP,
        pmax_elec_hp=P_MAX_HP, storage_soc_0=SOC_0_STORAGE,
        temp_wh=WASTE_HEAT_TEMP, pmax_bypass=P_MAX_BYPASS)

    # *** SHOW THE RESULTS ***
    print_results()
