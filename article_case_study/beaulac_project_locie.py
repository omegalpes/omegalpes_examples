#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.units.conversion_units import *
from omegalpes.energy.units.storage_units import *
from omegalpes.energy.energy_nodes import *
from omegalpes.energy.exergy import *
from omegalpes.general.time import *
from omegalpes.general.optimisation.model import *
from omegalpes.general.utils.plots import *
from omegalpes.general.utils.output_data import save_energy_flows
from pulp import LpStatus, GUROBI_CMD
import time as timer


def main(work_path, exergy_analysis=False, time_lapse=8760, temp_dead_state=20,
         pv_area=1, pv_efficiency=0, st_area=1, st_efficiency=0,
         st_temp_out=100, elec_batt_pc_max=1, elec_batt_pd_max=1,
         elec_batt_capa=1, elec_batt_soc_min=1, elec_batt_soc_max=1,
         elec_batt_self_disch=1, elec_batt_soc_ini=1, therm_stor_pc_max=1,
         therm_stor_pd_max=1, therm_stor_capa=1, therm_stor_soc_min=1,
         therm_stor_soc_max=1, therm_stor_self_disch=1, therm_stor_soc_ini=1,
         therm_stor_temp=65, p_max_lake=100, conduit_max_cooling=100,
         hp_heating_cop=2, hp_heating_pmax_elec=1, hp_heating_temp_in=25,
         hp_heating_temp_out=50, hp_dhw_cop=2, hp_dhw_pmax_elec=1,
         hp_dhw_temp_in=25, hp_dhw_temp_out=50, hp_cooling_cop=2,
         hp_cooling_pmax_elec=1, hp_cooling_temp_in=25, hp_cooling_temp_out=50,
         fluid_pumps_exergy_eff=1, high_heat_diss_temp=70,
         medium_heat_diss_temp=35, low_heat_diss_temp=15,
         heat_diss_exergy_eff=0):

    start_time = timer.time()
    time = TimeUnit(periods=time_lapse, dt=1)

    # RETRIEVING FIXED ENERGY PROFILES FOR 1 DAY OF OPERATION #

    if time_lapse == 24:
        conduit_hydraulics_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/conduit_hydraulics_elec_cons_1_day.txt", "r")
        conduit_hydraulics_cons = [c for c in map(float, conduit_hydraulics_file)]
        hameau_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_dhw_cons_1_day.txt", "r")
        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]
        hameau_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_elec_cons_1_day.txt", "r")
        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]
        hameau_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_heating_cons_1_day.txt", "r")
        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]
        ines_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/ines_cooling_cons_1_day.txt", "r")
        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]
        solar_irradiation_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/solar_irradiation_1_day.txt", "r")
        solar_pv_input = [c * pv_area for c in map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/solar_irradiation_1_day.txt", "r")
        solar_thermal_input = [c * st_area for c in map(float, solar_irradiation_file2)]
        zac_2_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_cooling_cons_1_day.txt", "r")
        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]
        zac_2_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_dhw_cons_1_day.txt", "r")
        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]
        zac_2_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_elec_cons_1_day.txt", "r")
        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]
        zac_2_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_heating_cons_1_day.txt", "r")
        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]
        zac_3_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_cooling_cons_1_day.txt", "r")
        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]
        zac_3_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_dhw_cons_1_day.txt", "r")
        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]
        zac_3_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_elec_cons_1_day.txt", "r")
        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]
        zac_3_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_heating_cons_1_day.txt", "r")
        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]
    elif time_lapse == 8760:
        conduit_hydraulics_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/conduit_hydraulics_elec_cons_1_year.txt",
            "r")
        conduit_hydraulics_cons = [c for c in
                                   map(float, conduit_hydraulics_file)]
        hameau_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_dhw_cons_1_year.txt", "r")
        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]
        hameau_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_elec_cons_1_year.txt", "r")
        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]
        hameau_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/hameau_heating_cons_1_year.txt",
            "r")
        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]
        ines_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/ines_cooling_cons_1_year.txt", "r")
        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]
        solar_irradiation_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/solar_irradiation_1_year.txt", "r")
        solar_pv_input = [c * pv_area for c in
                          map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/solar_irradiation_1_year.txt", "r")
        solar_thermal_input = [c * st_area for c in
                               map(float, solar_irradiation_file2)]
        zac_2_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_cooling_cons_1_year.txt", "r")
        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]
        zac_2_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_dhw_cons_1_year.txt", "r")
        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]
        zac_2_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_elec_cons_1_year.txt", "r")
        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]
        zac_2_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_2_heating_cons_1_year.txt", "r")
        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]
        zac_3_cooling_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_cooling_cons_1_year.txt", "r")
        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]
        zac_3_dhw_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_dhw_cons_1_year.txt", "r")
        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]
        zac_3_elec_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_elec_cons_1_year.txt", "r")
        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]
        zac_3_heating_file = open(
            work_path + "/Stage LOCIE Ali CHOUMAN/Profiles conso BEEAULAC/zac_3_heating_cons_1_year.txt", "r")
        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]
    else:
        raise(ValueError, 'Only time lapses of either 24 h or 8760 h are '
                         'allowed.')

    # *** CREATING THE UNITS ***

    lake = VariableProductionUnit(
        time, name='lake', energy_type='Thermal', p_max=p_max_lake)

    buy_elec_from_grid = VariableProductionUnit(
        time, name='buy_elec_from_grid', energy_type='Electrical')
    sell_elec_to_grid = VariableConsumptionUnit(
        time, name='sell_elec_to_grid', energy_type='Electrical')
    zac_2_elec = FixedConsumptionUnit(
        time, name='zac_2_elec', p=zac_2_elec_cons, energy_type='Electrical')
    zac_3_elec = FixedConsumptionUnit(
        time, name='zac_3_elec', p=zac_3_elec_cons, energy_type='Electrical')
    hameau_elec = FixedConsumptionUnit(
        time, name='hameau_elec', p=hameau_elec_cons, energy_type='Electrical')
    conduit_hydraulics = FixedConsumptionUnit(
        time, name='conduit_hydraulics', p=[0]*time_lapse, energy_type='Electrical')

    zac_2_dhw = FixedConsumptionUnit(
        time, name='zac_2_dhw', p=zac_2_dhw_cons, energy_type='Thermal')
    zac_3_dhw = FixedConsumptionUnit(
        time, name='zac_3_dhw', p=zac_3_dhw_cons, energy_type='Thermal')
    hameau_dhw = FixedConsumptionUnit(
        time, name='hameau_dhw', p=hameau_dhw_cons, energy_type='Thermal')

    zac_2_heating = FixedConsumptionUnit(
        time, name='zac_2_heating', p=zac_2_heating_cons, energy_type='Thermal')
    zac_3_heating = FixedConsumptionUnit(
        time, name='zac_3_heating', p=zac_3_heating_cons, energy_type='Thermal')
    hameau_heating = FixedConsumptionUnit(
        time, name='hameau_heating', p=hameau_heating_cons, energy_type='Thermal')

    solar_irradiation_pv = FixedProductionUnit(
        time, name='solar_irradiation_pv', p=solar_pv_input, energy_type='Electrical')
    solar_irradiation_st = FixedProductionUnit(
        time, name='solar_irradiation_st', p=solar_thermal_input, energy_type='Electrical')

    zac_2_cooling = FixedProductionUnit(
        time, name='zac_2_cooling', p=zac_2_cooling_prod, energy_type='Thermal')
    zac_3_cooling = FixedProductionUnit(
        time, name='zac_3_cooling', p=zac_3_cooling_prod, energy_type='Thermal')
    ines_cooling = FixedProductionUnit(
        time, name='ines_cooling', p=ines_cooling_prod, energy_type='Thermal')

    elec_batteries = StorageUnit(time, name='elec_batteries',
                                 pc_max=elec_batt_pc_max,
                                 pd_max=elec_batt_pd_max,
                                 capacity=elec_batt_capa,
                                 soc_min=elec_batt_soc_min,
                                 soc_max=elec_batt_soc_max,
                                 self_disch_t=elec_batt_self_disch,
                                 e_0=elec_batt_soc_ini,
                                 # ef_is_e0=True,
                                 energy_type='Electrical')

    thermal_storage = StorageUnit(time, name='thermal_storage',
                                  pc_max=therm_stor_pc_max,
                                  pd_max = therm_stor_pd_max,
                                  capacity = therm_stor_capa,
                                  soc_min = therm_stor_soc_min,
                                  soc_max = therm_stor_soc_max,
                                  self_disch_t = therm_stor_self_disch,
                                  e_0 = therm_stor_soc_ini,
                                  # ef_is_e0=True,
                                  energy_type='Thermal')

    high_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='high_temp_heat_dissipation', energy_type='Thermal')

    medium_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='medium_temp_heat_dissipation', energy_type='Thermal')

    low_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='low_temp_heat_dissipation', energy_type='Thermal')

    pv_panels = ElectricalConversionUnit(
        time, name='pv_panels', elec_to_elec_ratio=pv_efficiency)

    solar_thermal_collectors = ElectricalToThermalConversionUnit(
        time, name='solar_thermal_collectors', elec_to_therm_ratio=st_efficiency)

    heat_pump_heating = HeatPump(time, name='heat_pump_heating',
                                 cop=hp_heating_cop,
                                 pmax_in_elec=hp_heating_pmax_elec)

    heat_pump_dhw = HeatPump(time, name='heat_pump_dhw',
                             cop=hp_dhw_cop,  pmax_in_elec=hp_dhw_pmax_elec)

    heat_pump_cooling = HeatPump(time, name='heat_pump_cooling',
                                 cop=hp_cooling_cop,
                                 pmax_in_elec=hp_cooling_pmax_elec)

    imaginary_elec_prod_unit = VariableProductionUnit(time, name='imaginary_elec_prod_unit', energy_type='Electrical')
    from_high_temp_to_medium_temp = HeatPump(time, name='from_high_temp_to_medium_temp', cop=1e10, pmax_out_therm=1e4)
    # from_cooling_to_conduit = HeatPump(time, name='from_cooling_to_conduit', cop=1e5, pmax_out_therm=1e4)

    if exergy_analysis is True:
        # ElectricalExergy(energy_unit=buy_elec_from_grid)
        # ExergyDestruction(energy_unit=buy_elec_from_grid,
        #                   exergy_eff=buy_elec_from_grid_exergy_eff)
        ElectricalExergy(energy_unit=pv_panels.elec_consumption_unit)
        ElectricalExergy(energy_unit=pv_panels.elec_production_unit)
        ExergyDestruction(energy_unit=pv_panels)
        ElectricalExergy(energy_unit=solar_thermal_collectors.elec_consumption_unit)
        ThermalExergy(energy_unit=solar_thermal_collectors.thermal_production_unit,
                      temp_heat=st_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=solar_thermal_collectors)
        ElectricalExergy(energy_unit=conduit_hydraulics)
        ExergyDestruction(energy_unit=conduit_hydraulics,
                          exergy_eff=fluid_pumps_exergy_eff)
        ElectricalExergy(energy_unit=elec_batteries)
        ExergyDestruction(energy_unit=elec_batteries, temp_heat=50000,
                          temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=thermal_storage, temp_heat=therm_stor_temp,
                      temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=thermal_storage,
                          temp_heat=therm_stor_temp,
                          temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=high_temp_heat_dissipation,
                      temp_heat=high_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=high_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)
        ThermalExergy(energy_unit=medium_temp_heat_dissipation,
                      temp_heat=medium_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=medium_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)
        ThermalExergy(energy_unit=low_temp_heat_dissipation,
                      temp_heat=low_heat_diss_temp, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=low_temp_heat_dissipation,
                          exergy_eff=heat_diss_exergy_eff)

        ElectricalExergy(energy_unit=heat_pump_dhw.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_dhw.thermal_consumption_unit,
                      temp_heat=hp_dhw_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_dhw.thermal_production_unit,
                      temp_heat=hp_dhw_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_dhw)

        ElectricalExergy(energy_unit=heat_pump_heating.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_heating.thermal_consumption_unit,
                      temp_heat=hp_heating_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_heating.thermal_production_unit,
                      temp_heat=hp_heating_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_heating)

        # - Cooling heat pump -
        ElectricalExergy(energy_unit=heat_pump_cooling.elec_consumption_unit)
        ThermalExergy(energy_unit=heat_pump_cooling.thermal_consumption_unit,
                      temp_heat=hp_cooling_temp_in, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=heat_pump_cooling.thermal_production_unit,
                      temp_heat=hp_cooling_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=heat_pump_cooling)

        " Imaginary heat pump representing use of high temp heat for medium temp needs "
        ElectricalExergy(energy_unit=from_high_temp_to_medium_temp.elec_consumption_unit)
        ThermalExergy(energy_unit=from_high_temp_to_medium_temp.thermal_consumption_unit,
                      temp_heat=hp_dhw_temp_out, temp_ref=temp_dead_state)
        ThermalExergy(energy_unit=from_high_temp_to_medium_temp.thermal_production_unit,
                      temp_heat=hp_heating_temp_out, temp_ref=temp_dead_state)
        ExergyDestruction(energy_unit=from_high_temp_to_medium_temp)

        " Imaginary heat pump representing the recycling of cooling heat for the conduit "
        # ElectricalExergy(energy_unit=from_cooling_to_conduit.elec_consumption_unit)
        # ThermalExergy(energy_unit=from_cooling_to_conduit.thermal_consumption_unit,
        #               temp_heat=low_heat_diss_temp, temp_ref=temp_dead_state)
        # ThermalExergy(energy_unit=from_cooling_to_conduit.thermal_production_unit,
        #               temp_heat=hp_heating_temp_in, temp_ref=temp_dead_state)
        # ExergyDestruction(energy_unit=from_cooling_to_conduit)

    elec_node = EnergyNode(time, name='elec_node', energy_type='Electrical')
    pv_outlet_node = EnergyNode(time, name='pv_outlet_node', energy_type='Electrical')
    hp_dhw_elec_inlet_node = EnergyNode(time, name='hp_dhw_elec_inlet_node', energy_type='Electrical')
    hp_heating_elec_inlet_node = EnergyNode(time, name="hp_heating_elec_inlet_node", energy_type='Electrical')
    hp_cooling_elec_inlet_node = EnergyNode(time, name="hp_cooling_elec_inlet_node", energy_type='Electrical')
    # elec_storage_node = EnergyNode(time, name="elec_storage_node", energy_type='Electrical')
    solar_pv_node = EnergyNode(time, name='solar_pv_node', energy_type='Electrical')
    solar_st_node = EnergyNode(time, name='solar_st_node', energy_type='Electrical')
    thermal_collectors_node = EnergyNode(
        time, name='thermal_collectors_node', energy_type='Thermal')
    lake_node = EnergyNode(time, name='lake_node', energy_type='Thermal')
    cooling_node = EnergyNode(time, name='cooling_node', energy_type='Thermal')
    medium_temp_dissipation_node = EnergyNode(
        time, name='medium_temp_dissipation_node', energy_type='Thermal')
    heating_node = EnergyNode(
        time, name='heating_node', energy_type='Thermal')
    dhw_node = EnergyNode(time, name='dhw_node', energy_type='Thermal')
    imaginary_elec_node = EnergyNode(time, name='imaginary_elec_node', energy_type='Electrical')

    elec_node.connect_units(buy_elec_from_grid, zac_2_elec, zac_3_elec, hameau_elec,
                            conduit_hydraulics, # elec_batteries,
                            heat_pump_dhw.elec_consumption_unit,
                            heat_pump_heating.elec_consumption_unit,
                            heat_pump_cooling.elec_consumption_unit)
    # elec_node.export_to_node(hp_dhw_elec_inlet_node)
    # elec_node.export_to_node(hp_heating_elec_inlet_node)
    # elec_node.export_to_node(hp_cooling_elec_inlet_node)
    # elec_node.export_to_node(elec_storage_node)
    pv_outlet_node.connect_units(pv_panels.elec_production_unit, sell_elec_to_grid, elec_batteries.production_unit, elec_batteries.consumption_unit)
    pv_outlet_node.export_to_node(elec_node)
    # pv_outlet_node.export_to_node(elec_storage_node)
    # pv_outlet_node.export_to_node(hp_dhw_elec_inlet_node)
    # pv_outlet_node.export_to_node(hp_heating_elec_inlet_node)
    # pv_outlet_node.export_to_node(hp_cooling_elec_inlet_node)
    # hp_dhw_elec_inlet_node.connect_units(heat_pump_dhw.elec_consumption_unit)
    # hp_heating_elec_inlet_node.connect_units(heat_pump_heating.elec_consumption_unit)
    # hp_cooling_elec_inlet_node.connect_units(heat_pump_cooling.elec_consumption_unit)
    # elec_storage_node.connect_units(elec_batteries)
    # elec_storage_node.export_to_node(elec_node)

    solar_pv_node.connect_units(solar_irradiation_pv,
                                pv_panels.elec_consumption_unit)
    solar_st_node.connect_units(solar_irradiation_st,
                                solar_thermal_collectors.elec_consumption_unit)
    thermal_collectors_node.connect_units(solar_thermal_collectors.thermal_production_unit,
                                          high_temp_heat_dissipation)
    thermal_collectors_node.export_to_node(dhw_node)
    # thermal_collectors_node.export_to_node(heating_node)

    lake_node.connect_units(lake, heat_pump_dhw.thermal_consumption_unit,
                                heat_pump_heating.thermal_consumption_unit
                            # , from_cooling_to_conduit.thermal_production_unit
                            )

    cooling_node.connect_units(zac_2_cooling, zac_3_cooling, ines_cooling,
                               heat_pump_cooling.thermal_consumption_unit,
                               low_temp_heat_dissipation,
                               # from_cooling_to_conduit.thermal_consumption_unit
                               )
    # cooling_node.export_to_node(lake_node  # , export_max=conduit_max_cooling
    #                             )
    medium_temp_dissipation_node.connect_units(medium_temp_heat_dissipation,
        heat_pump_cooling.thermal_production_unit)
    medium_temp_dissipation_node.export_to_node(heating_node)

    heating_node.connect_units(heat_pump_heating.thermal_production_unit,
                               zac_2_heating, zac_3_heating, hameau_heating,
                               from_high_temp_to_medium_temp.thermal_production_unit)

    dhw_node.connect_units(thermal_storage.production_unit, thermal_storage.consumption_unit, zac_2_dhw, zac_3_dhw,
                           hameau_dhw, heat_pump_dhw.thermal_production_unit,
                           from_high_temp_to_medium_temp.thermal_consumption_unit)

    # dhw_node.export_to_node(heating_node)
    # dhw_node.export_to_node(dissipation_node) # In theory, not necessary
    # heating_node.export_to_node(dissipation_node) # In theory, not necessary

    imaginary_elec_node.connect_units(imaginary_elec_prod_unit, from_high_temp_to_medium_temp.elec_consumption_unit,
                                      # from_cooling_to_conduit.elec_consumption_unit
                                      )

    """ ENERGY-FOCUSED OPTIMIZATION OBJECTIVES """
    buy_elec_from_grid.minimize_production()
    # buy_elec_from_grid.maximize_production()
    sell_elec_to_grid.minimize_consumption()
    # lake.maximize_production()
    # lake.minimize_production()
    high_temp_heat_dissipation.minimize_consumption()
    medium_temp_heat_dissipation.minimize_consumption()
    low_temp_heat_dissipation.minimize_consumption()
    # heat_pump_dhw.thermal_production_unit.minimize_production()
    # heat_pump_heating.thermal_production_unit.minimize_production()
    # heat_pump_cooling.thermal_production_unit.maximize_production()

    """ EXERGY-FOCUSED OPTIMIZATION OBJECTIVES """
    # heat_pump_dhw.minimize_exergy_destruction()
    # heat_pump_heating.minimize_exergy_destruction()
    # heat_pump_cooling.minimize_exergy_destruction()
    # elec_batteries.minimize_exergy_destruction()
    # thermal_storage.minimize_exergy_destruction()
    # high_temp_heat_dissipation.minimize_exergy_destruction()
    # medium_temp_heat_dissipation.minimize_exergy_destruction()
    # low_temp_heat_dissipation.minimize_exergy_destruction()
    # from_high_temp_to_medium_temp.minimize_exergy_destruction()

    # CREATING THE OPTIMIZATION MODEL
    model = OptimisationModel(time, name='optimization_model')
    model.add_nodes(elec_node, pv_outlet_node, # hp_dhw_elec_inlet_node,
                    # hp_heating_elec_inlet_node, hp_cooling_elec_inlet_node,
                    solar_pv_node, solar_st_node, lake_node, cooling_node,
                    medium_temp_dissipation_node, heating_node, dhw_node,
                    thermal_collectors_node, imaginary_elec_node)
    # model.writeLP(work_path + r'\optim_models\beeaulac.lp')
    model.solve_and_update()
    #model.solve_and_update()

    print('  ')
    print('Execution time: {} minutes.'.
          format(round((timer.time() - start_time)/60, 1)))
    print('  ')

    return model, time, lake, buy_elec_from_grid, sell_elec_to_grid, zac_2_elec, zac_3_elec, hameau_elec, conduit_hydraulics, zac_2_dhw, zac_3_dhw, hameau_dhw, zac_2_heating, zac_3_heating, hameau_heating, solar_irradiation_pv, solar_irradiation_st, zac_2_cooling, zac_3_cooling, ines_cooling, elec_batteries, high_temp_heat_dissipation, medium_temp_heat_dissipation, low_temp_heat_dissipation, thermal_storage, pv_panels, solar_thermal_collectors, heat_pump_dhw, heat_pump_heating, heat_pump_cooling, from_high_temp_to_medium_temp, elec_node, pv_outlet_node, solar_pv_node, solar_st_node, lake_node, cooling_node, medium_temp_dissipation_node, thermal_collectors_node, heating_node, dhw_node, imaginary_elec_node  # hp_dhw_elec_inlet_node, hp_heating_elec_inlet_node, hp_cooling_elec_inlet_node, from_cooling_to_conduit,


def print_results(exergy_analysis=False, figures=False):
    if LpStatus[MODEL.status] == 'Optimal':

        grid = [a for a in BUY_ELEC_FROM_GRID.p.value.values()]
        pv = [b for b in PV_OUTLET_NODE.energy_export_to_elec_node.get_value()]
        bat = [c for c in ELEC_BATTERIES.pd.value.values()]
        zac2 = [d for d in ZAC_2_ELEC.p.value]
        zac3 = [e for e in ZAC_3_ELEC.p.value]
        ham = [f for f in HAMEAU_ELEC.p.value]
        hpco = [g for g in HEAT_PUMP_COOLING.elec_consumption_unit.p.value.values()]
        hphe = [h for h in HEAT_PUMP_HEATING.elec_consumption_unit.p.value.values()]
        hpdw = [i for i in HEAT_PUMP_DHW.elec_consumption_unit.p.value.values()]

        total_resid_elec_demands = [d+e+f for d, e, f in zip(zac2, zac3, ham)]
        total_renewable_elec_prod = [b for b in pv]
        resid_elec_demands_covered_by_pv = [min(b, d+e+f) for b, d, e, f in zip(pv, zac2, zac3, ham)]
        resid_elec_demands_covered_by_grid = [min(a, b-c) for a, b, c in zip(grid,
                                                                             total_resid_elec_demands,
                                                                             resid_elec_demands_covered_by_pv)]
        hp_cooling_elec_cons_covered_by_pv = [min(a, max(0, b-c)) for a, b, c in zip(hpco,
                                                                                     total_renewable_elec_prod,
                                                                                     resid_elec_demands_covered_by_pv)]
        hp_cooling_elec_cons_covered_by_grid = [a-b for a, b in zip(hpco, hp_cooling_elec_cons_covered_by_pv)]
        hp_heating_elec_cons_covered_by_pv = [min(a, max(0, b-c-d)) for a, b, c, d in zip(hphe,
                                                                                     total_renewable_elec_prod,
                                                                                     resid_elec_demands_covered_by_pv,
                                                                                     hp_cooling_elec_cons_covered_by_pv)]
        hp_heating_elec_cons_covered_by_grid = [a - b for a, b in zip(hphe, hp_heating_elec_cons_covered_by_pv)]
        hp_dhw_elec_cons_covered_by_pv = [min(a, max(0, b -c -d -e)) for a, b, c, d, e in zip(hpdw,
                                                                                  total_renewable_elec_prod,
                                                                                  resid_elec_demands_covered_by_pv,
                                                                                  hp_cooling_elec_cons_covered_by_pv,
                                                                                  hp_heating_elec_cons_covered_by_pv)]
        hp_dhw_elec_cons_covered_by_grid = [a - b for a, b in zip(hpdw, hp_dhw_elec_cons_covered_by_pv)]

        total_heat_pumps_electricity_consumption = [g + h + i for g, h, i in zip(hpco, hphe, hpdw)]
        heat_pumps_elec_cons_covered_by_pv = [a+b+c for a, b, c in zip(hp_cooling_elec_cons_covered_by_pv,
                                                                       hp_heating_elec_cons_covered_by_pv,
                                                                       hp_dhw_elec_cons_covered_by_pv)]
        heat_pumps_elec_cons_covered_by_grid = [a + b + c for a, b, c in zip(hp_cooling_elec_cons_covered_by_grid,
                                                                           hp_heating_elec_cons_covered_by_grid,
                                                                           hp_dhw_elec_cons_covered_by_grid)]

        print('  ')
        print('ENERGY ANALYSIS:')
        print('  ')
        print('Heat supplied by the anergy conduit = {0} kWh.'.
              format(round(LAKE.e_tot.value, 2)))
        print('Electricity bought from the Grid = {0} kWh.'.
              format(round(BUY_ELEC_FROM_GRID.e_tot.value, 2)))
        print('Electricity sold to the Grid = {0} kWh.'.
              format(round(SELL_ELEC_TO_GRID.e_tot.value, 2)))
        print('zac_2_elec = {0} kWh.'.
              format(round(ZAC_2_ELEC.e_tot.value, 2)))
        print('zac_3_elec = {0} kWh.'.format(round(ZAC_3_ELEC.e_tot.value, 2)))
        print('hameau_elec = {0} kWh.'.format(round(HAMEAU_ELEC.e_tot.value, 2)))
        print('conduit_hydraulics = {0} kWh.'.
              format(round(CONDUIT_HYDRAULICS.e_tot.value, 2)))
        print('zac_2_dhw = {0} kWh.'.format(round(ZAC_2_DHW.e_tot.value, 2)))
        print('zac_3_dhw = {0} kWh.'.format(round(ZAC_3_DHW.e_tot.value, 2)))
        print('hameau_dhw = {0} kWh.'.format(round(HAMEAU_DHW.e_tot.value, 2)))
        print('zac_2_heating = {0} kWh.'.format(round(ZAC_2_HEATING.e_tot.value, 2)))
        print('zac_3_heating = {0} kWh.'.format(round(ZAC_3_HEATING.e_tot.value, 2)))
        print('hameau_heating = {0} kWh.'.format(round(HAMEAU_HEATING.e_tot.value, 2)))
        print('zac_2_cooling = {0} kWh.'.
              format(round(ZAC_2_COOLING.e_tot.value, 2)))
        print('zac_3_cooling = {0} kWh.'.
              format(round(ZAC_3_COOLING.e_tot.value, 2)))
        print('ines_cooling = {0} kWh.'.
              format(round(INES_COOLING.e_tot.value, 2)))
        print('elec_batteries = {0} kWh.'.
              format(round(ELEC_BATTERIES.e_tot.value, 2)))
        print('high_temperature_heat_dissipation = {0} kWh.'.
              format(round(HIGH_TEMP_HEAT_DISSIPATION.e_tot.value, 2)))
        print('medium_temperature_heat_dissipation = {0} kWh.'.
              format(round(MEDIUM_TEMP_HEAT_DISSIPATION.e_tot.value, 2)))
        print('low_temperature_heat_dissipation = {0} kWh.'.
              format(round(LOW_TEMP_HEAT_DISSIPATION.e_tot.value, 2)))
        print('thermal_storage = {0} kWh.'.
              format(round(THERMAL_STORAGE.e_tot.value, 2)))

        print('pv_panels_solar_cons = {0} kWh.'.format(
            round(PV_PANELS.elec_consumption_unit.e_tot.value, 2)))
        print('pv_panels_elec_prod = {0} kWh.'.format(
            round(PV_PANELS.elec_production_unit.e_tot.value, 2)))
        print('st_coll_solar_cons = {0} kWh.'.format(
            round(SOLAR_THERMAL_COLLECTORS.elec_consumption_unit.e_tot.value, 2)))
        print('st_coll_thermal_prod = {0} kWh.'.format(
            round(SOLAR_THERMAL_COLLECTORS.thermal_production_unit.e_tot.value, 2)))
        print('hp_dhw_elec_cons = {0} kWh.'.format(
            round(HEAT_PUMP_DHW.elec_consumption_unit.e_tot.value, 2)))
        print('hp_dhw_thermal_cons = {0} kWh.'.format(
            round(HEAT_PUMP_DHW.thermal_consumption_unit.e_tot.value, 2)))
        print('hp_dhw_thermal_prod = {0} kWh.'.format(
            round(HEAT_PUMP_DHW.thermal_production_unit.e_tot.value, 2)))
        print('hp_heating_elec_cons = {0} kWh.'.format(
            round(HEAT_PUMP_HEATING.elec_consumption_unit.e_tot.value, 2)))
        print('hp_heating_thermal_cons = {0} kWh.'.format(
            round(HEAT_PUMP_HEATING.thermal_consumption_unit.e_tot.value, 2)))
        print('hp_heating_thermal_prod = {0} kWh.'.format(
            round(HEAT_PUMP_HEATING.thermal_production_unit.e_tot.value, 2)))
        print('hp_cooling_elec_cons = {0} kWh.'.format(
            round(HEAT_PUMP_COOLING.elec_consumption_unit.e_tot.value, 2)))
        print('hp_cooling_thermal_cons = {0} kWh.'.format(
            round(HEAT_PUMP_COOLING.thermal_consumption_unit.e_tot.value, 2)))
        print('hp_cooling_thermal_prod = {0} kWh.'.format(
            round(HEAT_PUMP_COOLING.thermal_production_unit.e_tot.value, 2)))
        # print('hp_dhw_elec_cons_from_grid = {0} kWh.'.format(round(sum(
        #       ELEC_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value()), 2)))
        # print('hp_dhw_elec_cons_from_pv = {0} kWh.'.format(round(sum(
        #       PV_OUTLET_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value()), 2)))
        # print('hp_heating_elec_cons_from_grid = {0} kWh.'.
        #       format(round(sum(ELEC_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value()), 2)))
        # print('hp_heating_elec_cons_from_pv = {0} kWh.'.format(round(sum(
        #       PV_OUTLET_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value()), 2)))
        # print('hp_cooling_elec_cons_from_grid = {0} kWh.'.format(round(sum(
        #       ELEC_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value()), 2)))
        # print('hp_cooling_elec_cons_from_pv = {0} kWh.'.format(round(sum(
        #       PV_OUTLET_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value()), 2)))
        # print('export_from_cooling_node_to_lake_node = {0} kWh.'.format(round(
        #     FROM_COOLING_TO_CONDUIT.thermal_consumption_unit.e_tot.value, 2)))
        print('export_from_pv_node_to_elec_node = {0} kWh.'.format(
            round(sum(PV_OUTLET_NODE.energy_export_to_elec_node.get_value()), 2)))
        print('export_from_st_node_to_dhw_node = {0} kWh.'.format(
            round(sum(THERMAL_COLLECTORS_NODE.energy_export_to_dhw_node.get_value()),
                  2)))
        # print('export_from_st_node_to_heating_node = {0} kWh.'.format(
        #     round(sum(THERMAL_COLLECTORS_NODE.energy_export_to_heating_node.get_value()), 2)))
        print('export_from_dhw_node_to_heating_node = {0} kWh.'.format(
            round(FROM_HIGH_TEMP_TO_MEDIUM_TEMP.thermal_consumption_unit.e_tot.value, 2)))
        print('export_from_medium_temp_diss_node_to_heating_node = {0} kWh.'.format(
            round(sum(MEDIUM_TEMP_DISSIPATION_NODE.energy_export_to_heating_node.get_value()), 2)))
        print('-> hp_dhw_elec_cons_covered_by_pv = {0} kWh.'.format(round(sum(
            hp_dhw_elec_cons_covered_by_pv), 2)))
        print('-> hp_dhw_elec_cons_covered_by_grid = {0} kWh.'.format(round(sum(
            hp_dhw_elec_cons_covered_by_grid), 2)))
        print('-> hp_heating_elec_cons_covered_by_pv = {0} kWh.'.format(round(sum(
            hp_heating_elec_cons_covered_by_pv), 2)))
        print('-> hp_heating_elec_cons_covered_by_grid = {0} kWh.'.format(round(sum(
            hp_heating_elec_cons_covered_by_grid), 2)))
        print('-> hp_cooling_elec_cons_covered_by_pv = {0} kWh.'.format(round(sum(
            hp_cooling_elec_cons_covered_by_pv), 2)))
        print('-> hp_cooling_elec_cons_covered_by_grid = {0} kWh.'.format(round(sum(
            hp_cooling_elec_cons_covered_by_grid), 2)))
        print('total residential electricity demands = {0} kWh.'.format(round(
            ZAC_2_ELEC.e_tot.value + ZAC_3_ELEC.e_tot.value + HAMEAU_ELEC.e_tot.value, 2)))
        print('-> residential electricity demands covered by PV = {0} kWh.'.format(round(sum(
            resid_elec_demands_covered_by_pv), 2)))
        print('-> residential electricity demands covered by the Grid = {0} kWh.'.format(round(sum(
            resid_elec_demands_covered_by_grid), 2)))
        print('total heat pumps electricity consumption = {0} kWh.'.format(round(sum(
            total_heat_pumps_electricity_consumption), 2)))
        print('-> heat pumps electric consumption covered by PV = {0} kWh.'.format(round(sum(
            heat_pumps_elec_cons_covered_by_pv), 2)))
        print('-> heat pumps electric consumption covered by the Grid = {0} kWh.'.format(round(sum(
            heat_pumps_elec_cons_covered_by_grid), 2)))
        print('(imaginary_elec_cons_from_dhw_node_to_heating_node = {0} kWh.)'.format(
            round(FROM_HIGH_TEMP_TO_MEDIUM_TEMP.elec_consumption_unit.e_tot.value, 2)))
        # print('imaginary_elec_cons_from_cooling_node_to_lake_node = {0} kWh.'.format(round(
        #     FROM_COOLING_TO_CONDUIT.elec_consumption_unit.e_tot.value, 2)))

        if exergy_analysis is True:
            print('  ')
            print('EXERGY ANALYSIS:')
            print('  ')
            print('Exergy destruction (PV PANELS) = {0} kWh.'.format(
                round(PV_PANELS.exd_tot.value, 2)))
            print('Exergy destruction (ELECTRIC BATTERIES) = {0} kWh.'.format(
                round(ELEC_BATTERIES.exd_tot.value, 2)))
            print('Exergy destruction (ST COLLECTORS) = {0} kWh.'.format(
                round(SOLAR_THERMAL_COLLECTORS.exd_tot.value, 2)))
            print('Exergy destruction (THERMAL STORAGE) = {0} kWh.'.format(
                round(THERMAL_STORAGE.exd_tot.value, 2)))
            print('Exergy destruction (HP_DHW) = {0} kWh.'.format(
                round(HEAT_PUMP_DHW.exd_tot.value, 2)))
            print('Exergy destruction (HP_HEATING) = {0} kWh.'.format(
                round(HEAT_PUMP_HEATING.exd_tot.value, 2)))
            print('Exergy destruction (HP_COOLING) = {0} kWh.'.format(
                round(HEAT_PUMP_COOLING.exd_tot.value, 2)))
            print('Exergy destruction (HYDRAULIC PUMPS) = {0} kWh.'.format(
                round(CONDUIT_HYDRAULICS.exd_tot.value, 2)))
            print('Exergy destruction (HIGH TEMP HEAT DISSIPATION) = {0} kWh.'.
                format(round(HIGH_TEMP_HEAT_DISSIPATION.exd_tot.value, 2)))
            print('Exergy destruction (MEDIUM TEMP HEAT DISSIPATION) = {0} kWh.'.
                  format(round(MEDIUM_TEMP_HEAT_DISSIPATION.exd_tot.value, 2)))
            print('Exergy destruction (LOW TEMP HEAT DISSIPATION) = {0} kWh.'.
                  format(round(LOW_TEMP_HEAT_DISSIPATION.exd_tot.value, 2)))
            print('Exergy destruction (FROM HIGH TEMP TO MEDIUM TEMP) = {0} kWh.'.
                  format(round(FROM_HIGH_TEMP_TO_MEDIUM_TEMP.exd_tot.value, 2)))
            # print('Exergy destruction (FROM COOLING TO CONDUIT) = {0} kWh.'.
            #       format(round(FROM_COOLING_TO_CONDUIT.exd_tot.value, 2)))

        with open('beeaulac_energy_analysis.txt', 'w') as f:
            f.write('%s\n' % round(LAKE.e_tot.value, 8))
            f.write('%s\n' % round(BUY_ELEC_FROM_GRID.e_tot.value, 8))
            f.write('%s\n' % round(SELL_ELEC_TO_GRID.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_2_ELEC.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_3_ELEC.e_tot.value, 8))
            f.write('%s\n' % round(HAMEAU_ELEC.e_tot.value, 8))
            f.write('%s\n' % round(CONDUIT_HYDRAULICS.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_2_DHW.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_3_DHW.e_tot.value, 8))
            f.write('%s\n' % round(HAMEAU_DHW.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_2_HEATING.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_3_HEATING.e_tot.value, 8))
            f.write('%s\n' % round(HAMEAU_HEATING.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_2_COOLING.e_tot.value, 8))
            f.write('%s\n' % round(ZAC_3_COOLING.e_tot.value, 8))
            f.write('%s\n' % round(INES_COOLING.e_tot.value, 8))
            f.write('%s\n' % round(ELEC_BATTERIES.e_tot.value, 8))
            f.write('%s\n' % round(HIGH_TEMP_HEAT_DISSIPATION.e_tot.value, 8))
            f.write('%s\n' % round(MEDIUM_TEMP_HEAT_DISSIPATION.e_tot.value, 8))
            f.write('%s\n' % round(LOW_TEMP_HEAT_DISSIPATION.e_tot.value, 8))
            f.write('%s\n' % round(THERMAL_STORAGE.e_tot.value, 8))
            f.write('%s\n' % round(PV_PANELS.elec_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(PV_PANELS.elec_production_unit.e_tot.value, 8))
            f.write('%s\n' % round(SOLAR_THERMAL_COLLECTORS.elec_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(SOLAR_THERMAL_COLLECTORS.thermal_production_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_DHW.elec_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_DHW.thermal_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_DHW.thermal_production_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_HEATING.elec_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_HEATING.thermal_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_HEATING.thermal_production_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_COOLING.elec_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_COOLING.thermal_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(HEAT_PUMP_COOLING.thermal_production_unit.e_tot.value, 8))
            f.write('%s\n' % round(sum(PV_OUTLET_NODE.energy_export_to_elec_node.get_value()), 8))
            f.write('%s\n' % round(sum(THERMAL_COLLECTORS_NODE.energy_export_to_dhw_node.get_value()), 8))
            f.write('%s\n' % round(FROM_HIGH_TEMP_TO_MEDIUM_TEMP.thermal_consumption_unit.e_tot.value, 8))
            f.write('%s\n' % round(sum(MEDIUM_TEMP_DISSIPATION_NODE.energy_export_to_heating_node.get_value()), 8))
            f.write('%s\n' % round(sum(hp_dhw_elec_cons_covered_by_pv), 8))
            f.write('%s\n' % round(sum(hp_dhw_elec_cons_covered_by_grid), 8))
            f.write('%s\n' % round(sum(hp_heating_elec_cons_covered_by_pv), 8))
            f.write('%s\n' % round(sum(hp_heating_elec_cons_covered_by_grid), 8))
            f.write('%s\n' % round(sum(hp_cooling_elec_cons_covered_by_pv), 8))
            f.write('%s\n' % round(sum(hp_cooling_elec_cons_covered_by_grid), 8))
            f.write('%s\n' % round(sum(total_resid_elec_demands), 8))
            f.write('%s\n' % round(sum(resid_elec_demands_covered_by_pv), 8))
            f.write('%s\n' % round(sum(resid_elec_demands_covered_by_grid), 8))
            f.write('%s\n' % round(sum(total_heat_pumps_electricity_consumption), 8))
            f.write('%s\n' % round(sum(heat_pumps_elec_cons_covered_by_pv), 8))
            f.write('%s\n' % round(sum(heat_pumps_elec_cons_covered_by_grid), 8))

            # f.write('%s\n' % round(sum(ELEC_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(sum(PV_OUTLET_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(sum(ELEC_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(sum(PV_OUTLET_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(sum(ELEC_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(sum(PV_OUTLET_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value()), 8))
            # f.write('%s\n' % round(FROM_COOLING_TO_CONDUIT.thermal_consumption_unit.e_tot.value, 2))

        with open('profile_lake.txt', 'w') as f:
            for p in LAKE.p.value.values():
                f.write('%s\n' % p)

        with open('profile_buy_elec_from_grid.txt', 'w') as f:
            for p in BUY_ELEC_FROM_GRID.p.value.values():
                f.write('%s\n' % p)

        with open('profile_sell_elec_to_grid.txt', 'w') as f:
            for p in SELL_ELEC_TO_GRID.p.value.values():
                f.write('%s\n' % p)

        with open('profile_high_temp_heat_diss.txt', 'w') as f:
            for p in HIGH_TEMP_HEAT_DISSIPATION.p.value.values():
                f.write('%s\n' % p)

        with open('profile_medium_temp_heat_diss.txt', 'w') as f:
            for p in MEDIUM_TEMP_HEAT_DISSIPATION.p.value.values():
                f.write('%s\n' % p)

        with open('profile_low_temp_heat_diss.txt', 'w') as f:
            for p in LOW_TEMP_HEAT_DISSIPATION.p.value.values():
                f.write('%s\n' % p)

        with open('profile_pv_solar_inlet.txt', 'w') as f:
            for p in PV_PANELS.elec_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_pv_elec_outlet.txt', 'w') as f:
            for p in PV_PANELS.elec_production_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_st_solar_inlet.txt', 'w') as f:
            for p in SOLAR_THERMAL_COLLECTORS.elec_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_st_thermal_outlet.txt', 'w') as f:
            for p in SOLAR_THERMAL_COLLECTORS.thermal_production_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_dhw_elec-total_cons.txt', 'w') as f:
            for p in HEAT_PUMP_DHW.elec_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_dhw_thermal_cons.txt', 'w') as f:
            for p in HEAT_PUMP_DHW.thermal_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_dhw_thermal_prod.txt', 'w') as f:
            for p in HEAT_PUMP_DHW.thermal_production_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_heating_elec-total_cons.txt', 'w') as f:
            for p in HEAT_PUMP_HEATING.elec_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_heating_thermal_cons.txt', 'w') as f:
            for p in HEAT_PUMP_HEATING.thermal_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_heating_thermal_prod.txt', 'w') as f:
            for p in HEAT_PUMP_HEATING.thermal_production_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_cooling_elec-total_cons.txt', 'w') as f:
            for p in HEAT_PUMP_COOLING.elec_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_cooling_thermal_cons.txt', 'w') as f:
            for p in HEAT_PUMP_COOLING.thermal_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_heat_pump_cooling_thermal_prod.txt', 'w') as f:
            for p in HEAT_PUMP_COOLING.thermal_production_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_elec_batteries_charge.txt', 'w') as f:
            for p in ELEC_BATTERIES.pc.value.values():
                f.write('%s\n' % p)

        with open('profile_elec_batteries_discharge.txt', 'w') as f:
            for p in ELEC_BATTERIES.pd.value.values():
                f.write('%s\n' % p)

        with open('profile_elec_batteries_net-flow.txt', 'w') as f:
            for p in ELEC_BATTERIES.p.value.values():
                f.write('%s\n' % p)

        with open('profile_elec_batteries_state-of-charge.txt', 'w') as f:
            for p in ELEC_BATTERIES.e.value.values():
                f.write('%s\n' % p)

        with open('profile_thermal_storage_charge.txt', 'w') as f:
            for p in THERMAL_STORAGE.pc.value.values():
                f.write('%s\n' % p)

        with open('profile_thermal_storage_discharge.txt', 'w') as f:
            for p in THERMAL_STORAGE.pd.value.values():
                f.write('%s\n' % p)

        with open('profile_thermal_storage_net-flow.txt', 'w') as f:
            for p in THERMAL_STORAGE.p.value.values():
                f.write('%s\n' % p)

        with open('profile_thermal_storage_state-of-charge.txt', 'w') as f:
            for p in THERMAL_STORAGE.e.value.values():
                f.write('%s\n' % p)

        with open('profile_export_from_pv-node_to_elec-node.txt', 'w') as f:
            for p in PV_OUTLET_NODE.energy_export_to_elec_node.get_value():
                f.write('%s\n' % p)

        with open('profile_export_from_st-node_to_dhw-node.txt', 'w') as f:
            for p in THERMAL_COLLECTORS_NODE.energy_export_to_dhw_node.get_value():
                f.write('%s\n' % p)

        with open('profile_export_from_dhw-node_to_heating-node.txt', 'w') as f:
            for p in FROM_HIGH_TEMP_TO_MEDIUM_TEMP.thermal_consumption_unit.p.value.values():
                f.write('%s\n' % p)

        with open('profile_export_from_medium_temp_diss_node_to_heating_node.txt', 'w') as f:
            for p in MEDIUM_TEMP_DISSIPATION_NODE.energy_export_to_heating_node.get_value():
                f.write('%s\n' % p)

        with open('profile_heat_pump_dhw_elec-pv_cons.txt', 'w') as f:
            for p in hp_dhw_elec_cons_covered_by_pv:
                f.write('%s\n' % p)

        with open('profile_heat_pump_dhw_elec-grid_cons.txt', 'w') as f:
            for p in hp_dhw_elec_cons_covered_by_grid:
                f.write('%s\n' % p)

        with open('profile_heat_pump_heating_elec-pv_cons.txt', 'w') as f:
            for p in hp_heating_elec_cons_covered_by_pv:
                f.write('%s\n' % p)

        with open('profile_heat_pump_heating_elec-grid_cons.txt', 'w') as f:
            for p in hp_heating_elec_cons_covered_by_grid:
                f.write('%s\n' % p)

        with open('profile_heat_pump_cooling_elec-pv_cons.txt', 'w') as f:
            for p in hp_cooling_elec_cons_covered_by_pv:
                f.write('%s\n' % p)

        with open('profile_heat_pump_cooling_elec-grid_cons.txt', 'w') as f:
            for p in hp_cooling_elec_cons_covered_by_grid:
                f.write('%s\n' % p)

        with open('profile_residential_total_elec_demands.txt', 'w') as f:
            for p in total_resid_elec_demands:
                f.write('%s\n' % p)

        with open('profile_residential_elec_demands_covered_by_pv.txt', 'w') as f:
            for p in resid_elec_demands_covered_by_pv:
                f.write('%s\n' % p)

        with open('profile_residential_elec_demands_covered_by_grid.txt', 'w') as f:
            for p in resid_elec_demands_covered_by_grid:
                f.write('%s\n' % p)

        with open('profile_heat_pumps_total_elec_cons.txt', 'w') as f:
            for p in total_heat_pumps_electricity_consumption:
                f.write('%s\n' % p)

        with open('profile_heat_pumps_total_elec-pv_cons.txt', 'w') as f:
            for p in heat_pumps_elec_cons_covered_by_pv:
                f.write('%s\n' % p)

        with open('profile_heat_pumps_total_elec-grid_cons.txt', 'w') as f:
            for p in heat_pumps_elec_cons_covered_by_grid:
                f.write('%s\n' % p)

        # with open('profile_heat_pump_dhw_elec-grid_cons.txt', 'w') as f:
        #     for p in ELEC_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_heat_pump_dhw_elec-pv_cons.txt', 'w') as f:
        #     for p in PV_OUTLET_NODE.energy_export_to_hp_dhw_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_heat_pump_heating_elec-grid_cons.txt', 'w') as f:
        #     for p in ELEC_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_heat_pump_heating_elec-pv_cons.txt', 'w') as f:
        #     for p in PV_OUTLET_NODE.energy_export_to_hp_heating_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_heat_pump_cooling_elec-grid_cons.txt', 'w') as f:
        #     for p in ELEC_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_heat_pump_cooling_elec-pv_cons.txt', 'w') as f:
        #     for p in PV_OUTLET_NODE.energy_export_to_hp_cooling_elec_inlet_node.get_value():
        #         f.write('%s\n' % p)

        # with open('profile_export_from_cooling-node_to_lake-node.txt', 'w') as f:
        #     for p in FROM_COOLING_TO_CONDUIT.thermal_consumption_unit.p.value.values():
        #         f.write('%s\n' % p)

        # with open('profile_export_from_st-node_to_heating-node.txt', 'w') as f:
        #     for p in THERMAL_COLLECTORS_NODE.energy_export_to_heating_node.get_value():
        #         f.write('%s\n' % p)

        if exergy_analysis is True:
            with open('beeaulac_exergy_analysis.txt', 'w') as f:
                f.write('%s\n' % round(PV_PANELS.exd_tot.value, 8))
                f.write('%s\n' % round(ELEC_BATTERIES.exd_tot.value, 8))
                f.write('%s\n' % round(SOLAR_THERMAL_COLLECTORS.exd_tot.value, 8))
                f.write('%s\n' % round(THERMAL_STORAGE.exd_tot.value, 8))
                f.write('%s\n' % round(HEAT_PUMP_DHW.exd_tot.value, 8))
                f.write('%s\n' % round(HEAT_PUMP_HEATING.exd_tot.value, 8))
                f.write('%s\n' % round(HEAT_PUMP_COOLING.exd_tot.value, 8))
                f.write('%s\n' % round(CONDUIT_HYDRAULICS.exd_tot.value, 8))
                f.write('%s\n' % round(HIGH_TEMP_HEAT_DISSIPATION.exd_tot.value, 8))
                f.write('%s\n' % round(MEDIUM_TEMP_HEAT_DISSIPATION.exd_tot.value, 8))
                f.write('%s\n' % round(LOW_TEMP_HEAT_DISSIPATION.exd_tot.value, 8))
                f.write('%s\n' % round(FROM_HIGH_TEMP_TO_MEDIUM_TEMP.exd_tot.value, 8))
                # f.write('%s\n' % round(FROM_COOLING_TO_CONDUIT.exd_tot.value, 8))

            with open('profile_exergy_dest_pv_panels.txt', 'w') as f:
                for p in PV_PANELS.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_elec_batteries.txt', 'w') as f:
                for p in ELEC_BATTERIES.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_st_collectors.txt', 'w') as f:
                for p in SOLAR_THERMAL_COLLECTORS.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_thermal_storage.txt', 'w') as f:
                for p in THERMAL_STORAGE.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_heat_pump_dhw.txt', 'w') as f:
                for p in HEAT_PUMP_DHW.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_heat_pump_heating.txt', 'w') as f:
                for p in HEAT_PUMP_HEATING.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_heat_pump_cooling.txt', 'w') as f:
                for p in HEAT_PUMP_COOLING.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_high_temp_heat_diss.txt', 'w') as f:
                for p in HIGH_TEMP_HEAT_DISSIPATION.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_medium_temp_heat_diss.txt', 'w') as f:
                for p in MEDIUM_TEMP_HEAT_DISSIPATION.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_low_temp_heat_diss.txt', 'w') as f:
                for p in LOW_TEMP_HEAT_DISSIPATION.exergy_dest.value.values():
                    f.write('%s\n' % p)

            with open('profile_exergy_dest_from_high_temp_to_medium_temp.txt', 'w') as f:
                for p in FROM_HIGH_TEMP_TO_MEDIUM_TEMP.exergy_dest.value.values():
                    f.write('%s\n' % p)

            # with open('profile_exergy_dest_from_cooling_to_conduit.txt', 'w') as f:
            #     for p in FROM_COOLING_TO_CONDUIT.exergy_dest.value.values():
            #         f.write('%s\n' % p)

        if figures is True:
            # plot_node_energetic_flows(SOLAR_PV_NODE)
            # plot_node_energetic_flows(SOLAR_ST_NODE)
            plot_node_energetic_flows(ELEC_NODE)
            # plot_node_energetic_flows(ELEC_STORAGE_NODE)
            plot_node_energetic_flows(PV_OUTLET_NODE)
            # plot_node_energetic_flows(HP_DHW_ELEC_INLET_NODE)
            # plot_node_energetic_flows(HP_HEATING_ELEC_INLET_NODE)
            # plot_node_energetic_flows(HP_COOLING_ELEC_INLET_NODE)
            plot_node_energetic_flows(THERMAL_COLLECTORS_NODE)
            plot_node_energetic_flows(DHW_NODE)
            plot_node_energetic_flows(HEATING_NODE)
            plot_node_energetic_flows(COOLING_NODE)
            plot_node_energetic_flows(LAKE_NODE)
            plot_node_energetic_flows(MEDIUM_TEMP_DISSIPATION_NODE)
            # plot_node_energetic_flows(IMAGINARY_ELEC_NODE)
            plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print('Sorry, the optimisation problem has no feasible solution !')
    elif LpStatus[MODEL.status] == 'Unbounded':
        print('The cost function of the optimisation problem is unbounded !')
    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exists). PuLP does not manage to interpret the solver's output,the infeasibility of the MILP problem may have beendetected during presolve")


if __name__ == '__main__':
    # *** INPUT PARAMETERS *** #
    WORK_PATH = os.getcwd()

    # TODO: Au-dela de 50% de batteries électriques l'outil se plante. C'est
    #  a cause du SOC_FIN = SOC_INI ? Non.
    EXERGY_ANALYSIS = True  # "True" to apply, "False" to NOT apply
    FIGURES = True  # "True" to plot (time-consuming!!), "False" to NOT plot
    TIME_LAPSE = 24  # studied period (hours) ; 24 for a day, 8760 for a year
    DEAD_STATE_TEMPERATURE = -11  # °C (for exergy analysis purposes)
    ST_COLLECTORS_AREA = 507  # m²
    ST_COLLECTORS_EFF = 0.80  # kW of heat / kW of solar irradiation
    ST_COLLECTORS_OUTLET_TEMPERATURE = 65  # °C
    PV_PANELS_AREA = 42307  # m²
    PV_PANELS_EFF = 0.15  # kW of electricity / kW of solar irradiation
    LAKE_MAX_THERMAL_POWER = 75510  # kW
    CONDUIT_MAX_COOLING_POWER = 52857  # kW
    ELEC_BATT_CAPA = 15000  # kWh (Capacity of electric batteries)
    ELEC_BATT_MAX_CHARGE_POWER = 0.50 * ELEC_BATT_CAPA  # kW
    ELEC_BATT_MAX_DISCHARGE_POWER = 0.50 * ELEC_BATT_CAPA  # kW
    ELEC_BATT_SOC_MIN = 0.30  # % of maximal capacity
    ELEC_BATT_SOC_MAX = 1.00  # % of maximal capacity
    ELEC_BATT_SOC_INI = (0.5 * ELEC_BATT_SOC_MIN + 0.5 * ELEC_BATT_SOC_MAX) * ELEC_BATT_CAPA  # kWh
    ELEC_BATT_SELF_DISCHARGE = 0.0002  # %/h with respect to stored energy
    THERMAL_STORAGE_CAPACITY = 12374  # kWh
    THERMAL_STORAGE_MAX_POWER_CHARGE = 0.20 * THERMAL_STORAGE_CAPACITY  # kW
    THERMAL_STORAGE_MAX_POWER_DISCHARGE = 0.20 * THERMAL_STORAGE_CAPACITY   # kW
    THERMAL_STORAGE_SOC_MIN = 0   # % with respect to capacity
    THERMAL_STORAGE_SOC_MAX = 1.00   # % with respect to capacity
    THERMAL_STORAGE_SELF_DISCHARGE = 0.0015  # pu/h with respect to stored energy
    THERMAL_STORAGE_SOC_INI = 0   # % with respect to capacity
    THERMAL_STORAGE_TEMPERATURE = 65  # °C (assumed constant)
    HEAT_PUMP_HEATING_COP = 5   # kW_heat_output/kW_elec_input
    HEAT_PUMP_HEATING_PMAX_ELEC = 1500  # kW
    HEAT_PUMP_HEATING_INLET_TEMPERATURE = 7  # °C (assumed constant)
    HEAT_PUMP_HEATING_OUTLET_TEMPERATURE = 35  # °C (assumed constant)
    HEAT_PUMP_DHW_COP = 3   # kW_heat_output/kW_elec_input
    HEAT_PUMP_DHW_PMAX_ELEC = 1500  # kW
    HEAT_PUMP_DHW_INLET_TEMPERATURE = 7  # °C (assumed constant)
    HEAT_PUMP_DHW_OUTLET_TEMPERATURE = 65  # °C (assumed constant)
    HEAT_PUMP_COOLING_COP = 4   # kW_heat_output/kW_elec_input
    HEAT_PUMP_COOLING_PMAX_ELEC = 1500  # kW
    HEAT_PUMP_COOLING_INLET_TEMPERATURE = 15  # °C (assumed constant)
    HEAT_PUMP_COOLING_OUTLET_TEMPERATURE = 35  # °C (assumed constant)
    FLUID_PUMPS_EFFICIENCY = 0.9  # Isentropic efficiency
    HEAT_DISSIPATION_HIGH_TEMPERATURE = 65  # °C (assumed constant)
    HEAT_DISSIPATION_MEDIUM_TEMPERATURE = 35  # °C (assumed constant)
    HEAT_DISSIPATION_LOW_TEMPERATURE = 15  # °C (assumed constant)
    HEAT_DISSIPATION_EXERGY_EFF = 0

    # *** RUN MAIN ***
    MODEL, TIME, LAKE, BUY_ELEC_FROM_GRID, SELL_ELEC_TO_GRID, ZAC_2_ELEC, \
    ZAC_3_ELEC, HAMEAU_ELEC, CONDUIT_HYDRAULICS, ZAC_2_DHW, ZAC_3_DHW, \
    HAMEAU_DHW, ZAC_2_HEATING, ZAC_3_HEATING, HAMEAU_HEATING, \
    SOLAR_IRRADIATION_PV, SOLAR_IRRADIATION_ST, ZAC_2_COOLING, ZAC_3_COOLING, \
    INES_COOLING, ELEC_BATTERIES, HIGH_TEMP_HEAT_DISSIPATION, \
    MEDIUM_TEMP_HEAT_DISSIPATION, LOW_TEMP_HEAT_DISSIPATION, THERMAL_STORAGE, \
    PV_PANELS, SOLAR_THERMAL_COLLECTORS, HEAT_PUMP_DHW, HEAT_PUMP_HEATING, \
    HEAT_PUMP_COOLING, FROM_HIGH_TEMP_TO_MEDIUM_TEMP, \
    ELEC_NODE, PV_OUTLET_NODE, SOLAR_PV_NODE, \
    SOLAR_ST_NODE, LAKE_NODE, COOLING_NODE, MEDIUM_TEMP_DISSIPATION_NODE, \
    THERMAL_COLLECTORS_NODE, HEATING_NODE, DHW_NODE, IMAGINARY_ELEC_NODE = \
                                   main(work_path=WORK_PATH,
                                   exergy_analysis=EXERGY_ANALYSIS,
                                   time_lapse=TIME_LAPSE,
                                   temp_dead_state=DEAD_STATE_TEMPERATURE,
                                   st_area=ST_COLLECTORS_AREA,
                                   st_efficiency=ST_COLLECTORS_EFF,
                                   st_temp_out=ST_COLLECTORS_OUTLET_TEMPERATURE,
                                   pv_area=PV_PANELS_AREA,
                                   pv_efficiency=PV_PANELS_EFF,
                                   p_max_lake=LAKE_MAX_THERMAL_POWER,
                                   conduit_max_cooling=CONDUIT_MAX_COOLING_POWER,
                                   elec_batt_pc_max=ELEC_BATT_MAX_CHARGE_POWER,
                                   elec_batt_pd_max=ELEC_BATT_MAX_DISCHARGE_POWER,
                                   elec_batt_capa=ELEC_BATT_CAPA,
                                   elec_batt_soc_min=ELEC_BATT_SOC_MIN,
                                   elec_batt_soc_max=ELEC_BATT_SOC_MAX,
                                   elec_batt_self_disch=ELEC_BATT_SELF_DISCHARGE,
                                   elec_batt_soc_ini=ELEC_BATT_SOC_INI,
                                   therm_stor_pc_max=THERMAL_STORAGE_MAX_POWER_CHARGE,
                                   therm_stor_pd_max = THERMAL_STORAGE_MAX_POWER_DISCHARGE,
                                   therm_stor_capa=THERMAL_STORAGE_CAPACITY,
                                   therm_stor_soc_min=THERMAL_STORAGE_SOC_MIN,
                                   therm_stor_soc_max=THERMAL_STORAGE_SOC_MAX,
                                   therm_stor_self_disch=THERMAL_STORAGE_SELF_DISCHARGE,
                                   therm_stor_soc_ini=THERMAL_STORAGE_SOC_INI,
                                   therm_stor_temp=THERMAL_STORAGE_TEMPERATURE,
                                   hp_heating_cop=HEAT_PUMP_HEATING_COP,
                                   hp_heating_pmax_elec=HEAT_PUMP_HEATING_PMAX_ELEC,
                                   hp_heating_temp_in=HEAT_PUMP_HEATING_INLET_TEMPERATURE,
                                   hp_heating_temp_out=HEAT_PUMP_HEATING_OUTLET_TEMPERATURE,
                                   hp_dhw_cop=HEAT_PUMP_DHW_COP,
                                   hp_dhw_pmax_elec=HEAT_PUMP_DHW_PMAX_ELEC,
                                   hp_dhw_temp_in=HEAT_PUMP_DHW_INLET_TEMPERATURE,
                                   hp_dhw_temp_out=HEAT_PUMP_DHW_OUTLET_TEMPERATURE,
                                   hp_cooling_cop=HEAT_PUMP_COOLING_COP,
                                   hp_cooling_pmax_elec=HEAT_PUMP_COOLING_PMAX_ELEC,
                                   hp_cooling_temp_in=HEAT_PUMP_COOLING_INLET_TEMPERATURE,
                                   hp_cooling_temp_out=HEAT_PUMP_COOLING_OUTLET_TEMPERATURE,
                                   fluid_pumps_exergy_eff=FLUID_PUMPS_EFFICIENCY,
                                   high_heat_diss_temp=HEAT_DISSIPATION_HIGH_TEMPERATURE,
                                   medium_heat_diss_temp=HEAT_DISSIPATION_MEDIUM_TEMPERATURE,
                                   low_heat_diss_temp=HEAT_DISSIPATION_LOW_TEMPERATURE,
                                   heat_diss_exergy_eff=HEAT_DISSIPATION_EXERGY_EFF
                                   )

    # print_results(exergy_analysis=EXERGY_ANALYSIS, figures=FIGURES)

    # print('solar_irradiation_pv = {0} kWh.'.
    #       format(round(SOLAR_IRRADIATION_PV.e_tot.value, 2)))
    # print('solar_irradiation_st = {0} kWh.'.format(
    #     round(SOLAR_IRRADIATION_ST.e_tot.value, 2)))
    # print('pv_panels_energy_in = {0} kWh.'.format(
    #     PV_PANELS.elec_consumption_unit.p))
    # print('pv_panels_exergy_in = {0} kWh.'.format(
    #     PV_PANELS.elec_consumption_unit.exergy))
    # print('pv_panels_energy_out = {0} kWh.'.format(
    #     PV_PANELS.elec_production_unit.p))
    # print('pv_panels_exergy_out = {0} kWh.'.format(
    #     PV_PANELS.elec_production_unit.exergy))
    # print('pv_panels_exergy_dest = {0} kWh.'.format(
    #     PV_PANELS.exergy_dest))
    # print('st_coll_exergy_cons = {0} kWh.'.format(
    #     SOLAR_THERMAL_COLLECTORS.elec_consumption_unit.p))
    # print('st_coll_exergy_cons = {0} kWh.'.format(
    #     SOLAR_THERMAL_COLLECTORS.elec_consumption_unit.exergy))
    # print('st_coll_exergy_prod = {0} kWh.'.format(
    #     SOLAR_THERMAL_COLLECTORS.thermal_production_unit.p))
    # print('st_coll_exergy_prod = {0} kWh.'.format(
    #     SOLAR_THERMAL_COLLECTORS.thermal_production_unit.exergy))
    # print('st_coll_exergy_dest = {0} kWh.'.format(
    #     SOLAR_THERMAL_COLLECTORS.exergy_dest.value))
    # print('hp_dhw_elec_in = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.elec_consumption_unit.p))
    # print('hp_dhw_elec_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.elec_consumption_unit.exergy))
    # print('hp_dhw_heat_in = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.thermal_consumption_unit.p))
    # print('hp_dhw_thermal_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.thermal_consumption_unit.exergy))
    # print('hp_dhw_heat_out = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.thermal_production_unit.p))
    # print('hp_dhw_thermal_exergy_out = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.thermal_production_unit.exergy))
    # print('hp_dhw_exergy_dest = {0} kWh.'.format(
    #     HEAT_PUMP_DHW.exergy_dest))
    # print('hp_heating_elec_in = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.elec_consumption_unit.p))
    # print('hp_heating_elec_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.elec_consumption_unit.exergy))
    # print('hp_heating_heat_in = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.thermal_consumption_unit.p))
    # print('hp_heating_thermal_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.thermal_consumption_unit.exergy))
    # print('hp_heating_heat_out = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.thermal_production_unit.p))
    # print('hp_heating_thermal_exergy_out = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.thermal_production_unit.exergy))
    # print('hp_heating_exergy_dest = {0} kWh.'.format(
    #     HEAT_PUMP_HEATING.exergy_dest))
    # print('hp_cooling_elec_in = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.elec_consumption_unit.p))
    # print('hp_cooling_elec_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.elec_consumption_unit.exergy))
    # print('hp_cooling_heat_in = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.thermal_consumption_unit.p))
    # print('hp_cooling_thermal_exergy_in = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.thermal_consumption_unit.exergy))
    # print('hp_cooling_heat_out = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.thermal_production_unit.p))
    # print('hp_cooling_thermal_exergy_out = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.thermal_production_unit.exergy))
    # print('hp_cooling_exergy_dest = {0} kWh.'.format(
    #     HEAT_PUMP_COOLING.exergy_dest))

    # print('Energy (ELECTRIC BATTERIES) = {0} kWh.'.format(
    #     ELEC_BATTERIES.e))
    # print('Exergy (ELECTRIC BATTERIES) = {0} kWh.'.format(
    #     ELEC_BATTERIES.exergy))
    # print('Exergy dest (ELECTRIC BATTERIES) = {0} kWh.'.format(
    #     ELEC_BATTERIES.exergy_dest))
    # print('Energy (THERMAL STORAGE) = {0} kWh.'.format(
    #     THERMAL_STORAGE.e))
    # print('Exergy (THERMAL STORAGE) = {0} kWh.'.format(
    #     THERMAL_STORAGE.exergy))
    # print('Exergy dest (THERMAL STORAGE) = {0} kWh.'.format(
    #     THERMAL_STORAGE.exergy_dest))
    # print('Energy (CONDUIT_HYDRAULICS) = {0} kWh.'.format(
    #     CONDUIT_HYDRAULICS.p))
    # print('Exergy (CONDUIT_HYDRAULICS) = {0} kWh.'.format(
    #     CONDUIT_HYDRAULICS.exergy))
    # print('Exergy dest (CONDUIT_HYDRAULICS) = {0} kWh.'.format(
    #     CONDUIT_HYDRAULICS.exergy_dest))
    # print('Energy (HEAT DISSIPATION) = {0} kWh.'.format(
    #     HEAT_DISSIPATION.p))
    # print('Exergy (HEAT DISSIPATION) = {0} kWh.'.format(
    #     HEAT_DISSIPATION.exergy))
    # print('Exergy dest (HEAT DISSIPATION) = {0} kWh.'.format(
    #     HEAT_DISSIPATION.exergy_dest))
