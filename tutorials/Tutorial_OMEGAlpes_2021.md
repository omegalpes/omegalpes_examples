---
layout: "post"
title: "Tutorial_OMEGAlpes_2021"
description: "Tutorial initially developped for the openmod community in Berlin"
date: "2021-10-26"
---
# Learning how to use OMEGAlpes with notebooks
Working on the “To_Modify_PV_self_consumption_eng” notebook

## Available documentation
- [Source code documentation on readthedocs](https://omegalpes.readthedocs.io/en/latest/OMEGAlpes_description.html)
- [Source code on GITlab](https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes/tree/master/omegalpes)
- [Gitlab for examples](https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples)
- [Documentation for the GITlab examples on readthedocs](https://omegalpes-examples.readthedocs.io/en/latest/)
- [Tutorials and OMEGAlpes presentation in LibreOffice format](https://cloud.univ-grenoble-alpes.fr/index.php/s/ZyFecn636tmswy6)

## How to open the Notebook ?
Go to the notebook ["To_Modify__PV_self_consumption_eng.ipynb" on Gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/blob/master/notebooks/To_Modify__PV_self_consumption_eng.ipynb) and click the mybinder icon that looks like this: ![Binder](https://mybinder.org/badge_logo.svg).

It will launch a notebook instance on the public service mybinder, and you will then be able to modify and run it.

## Understanding the study case & the notebook
Once the notebook “To_Modify_PV_self_consumption_eng” is open, try to understand the whole code, and launch *Cell* --> *Run All* or *Cell* --> *Run* to run cell by cell.  
The results are available in notebooks/results thanks to the function `save_energy_flows`  
For more information on this study case or on OMEGAlpes, please refer to the document: « OMEGAlpes Presentation.pptx » available in the folder "omegalpes_notebooks" or to the documentation.

### Mission 1
**Adding a dryer to the model.**
- The starting hour can be variable in order to be optimised.
- A fixed load curve is already defined.
- You can use the function `import_clothes_dryer_load_profiles()` in order to get the load curve  

The model can be a bit long to execute once the dryer is added (~30 to 60 seconds), a hourglass symbol appears before the website name instead of a notebook.

### Mission 2
**Adding a constraint in order for the dryer to be able to start only between 8 am and noon.**  
For this mission, you can have a look at the [Source code documentation on readthedocs](https://omegalpes.readthedocs.io/en/latest/OMEGAlpes_description.html), especially the part:  
> OMEGAlpes structure /
> Energy Package /
> Energy_unit module /  

If you wish to provide some feedbacks, do not hesitate to reach omegalpes-users@groupes.renater.fr.
