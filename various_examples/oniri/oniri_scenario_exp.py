#! usr/bin/env python3
#  coding=utf-8 #

"""
    Ce module décrit des scénarios énergétiques pour le projet Oniri de
    la compagnie Organic Orchestra.
    Commande à appliquer pour l'exécuter : py -m oniri_scenario_new : il
    utilise à présent des unités des Reversibleunit et
    ReversibleConversionUnit

    ..

    Copyright 2018 G2ELab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

"""
import os
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalConversionUnit, ReversibleConversionUnit
from omegalpes.energy.units.reversible_units import ReversibleUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_quantity_bar, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.utils.input_data import resample_data
from omegalpes.general.time import TimeUnit

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def oniri(work_path, p_alex, p_ez_ju_sound, h_balance, h_show,
          mob, h_dep, h_arr, p_mob, pv_profile, pc_pv, storage_pmax,
          storage_capa, show, balance, l_placing, l_balance, l_show,
          pmax_bicy_prod_no_show, pmax_bicy_prod_show, nb_pdt, dt, h_pv_beg,
          h_pv_end, e0_eq_ef):
    global time, model, battery1, battery2, node_pv1, node_bicycle1, \
        node_video, node_sound_light

    # Adapting values depending on the dt
    nb_pdt = int(nb_pdt / dt)
    h_dep = int(h_dep / dt)
    h_balance = int(h_balance / dt)
    h_arr = int(h_arr / dt)
    l_placing = int(l_placing / dt)
    l_balance = int(l_balance / dt)
    l_show = int(l_show / dt)
    h_pv_beg = int(h_pv_beg / dt)
    h_pv_end = int(h_pv_end / dt)

    # Other possible methods

    # for value in ['nb_pdt', 'h_dep', 'h_balance', 'h_arr', 'l_placing',
    #               'l_balance', 'l_show', 'h_pv_beg', 'h_pv_end']:
    #     cmd = '{0} = int({0}/dt)'.format(value)
    #     exec(cmd)

    # [nb_pdt_dt, h_dep_dt, h_balance_dt, h_arr_dt, l_placing_dt, l_balance_dt,
    #  l_show_dt, h_pv_beg_dt, h_pv_end_dt] = [0]*9
    # for (value_dt, value) in zip([nb_pdt_dt, h_dep_dt, h_balance_dt, h_arr_dt,
    #                               l_placing_dt, l_balance_dt, l_show_dt,
    #                               h_pv_beg_dt, h_pv_end_dt],
    #                              [nb_pdt, h_dep, h_balance, h_arr, l_placing,
    #                               l_balance, l_show, h_pv_beg, h_pv_end]):
    #     value_dt = int(value/dt)

    if isinstance(h_show, list):
        h_show = [int(hs / dt) for hs in h_show]
    else:
        h_show = int(h_show / dt)

    # Creating an empty model
    time = TimeUnit(periods=nb_pdt, dt=dt)
    # = 1h
    model = OptimisationModel(name='scenario_oniri_new', time=time)

    # Data management
    pv1_profile = [p * pc_pv / 2 for p in resample_data(input_list=pv_profile,
                                                        dt_final=dt)]
    pv2_profile = [p * pc_pv / 2 for p in resample_data(input_list=pv_profile,
                                                        dt_final=dt)]
    # PV use
    for h in [*range(0, h_pv_beg), *range(h_pv_end, nb_pdt)]:
        pv1_profile[h] = 0
        pv2_profile[h] = 0

    # Maximal power for the production of energy with the bicycles out of OR
    # during the the show:
    pmax_bicy_prod = [pmax_bicy_prod_no_show] * nb_pdt

    # limitation of bicycle production during the show and placing
    if isinstance(h_show, list):
        for hs in h_show:
            for h in range(hs - l_placing, hs + l_show + 1):
                pmax_bicy_prod[h] = pmax_bicy_prod_show
    else:
        pmax_bicy_prod[h_show] = pmax_bicy_prod_show

    #
    cons_mob_profile = [0] * nb_pdt
    # if mob:
    #     for h in range(h_dep, h_arr):
    #         cons_mob_profile[h] = p_mob
    #     # No PV production before being onsite.
    #     for h in range(0, h_arr):
    #         pv1_profile[h] = 0
    #         pv2_profile[h] = 0
    #         pv3_profile[h] = 0
    #         pv4_profile[h] = 0

    # PRODUCTION UNITS
    # pv panels
    pv1 = FixedProductionUnit(time, name='pv1', p=pv1_profile)
    pv2 = FixedProductionUnit(time, name='pv2', p=pv2_profile)

    # bicycle generators / mobility
    bicycle1 = ReversibleUnit(time=time, name='bicycle1',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle2 = ReversibleUnit(time=time, name='bicycle2',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle3 = ReversibleUnit(time=time, name='bicycle3',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')
    bicycle4 = ReversibleUnit(time=time, name='bicycle4',
                              p_cons=cons_mob_profile,
                              pmax_prod=pmax_bicy_prod,
                              energy_type_cons='Electrical',
                              energy_type_prod='Electrical')

    # Bicycle can only produce during the day
    h_bicy_prod = ["08:00", "20:00"]
    bicycle1.production_unit.set_operating_time_range([h_bicy_prod])
    bicycle2.production_unit.set_operating_time_range([h_bicy_prod])
    bicycle3.production_unit.set_operating_time_range([h_bicy_prod])
    bicycle4.production_unit.set_operating_time_range([h_bicy_prod])

    # Minimizing the production coming from the bicycles
    bicycle1.production_unit.minimize_production()
    bicycle2.production_unit.minimize_production()
    bicycle3.production_unit.minimize_production()
    bicycle4.production_unit.minimize_production()

    # Power supply from the power grid after the show
    power_supply1 = VariableProductionUnit(time=time, name='power_supply1',
                                           p_max=0.15,
                                           energy_type='Electrical')
    power_supply2 = VariableProductionUnit(time=time, name='power_supply2',
                                           p_max=0.15,
                                           energy_type='Electrical')
    power_supply1.set_operating_time_range([["00:00", "08:00"],
                                            ["20:00", "00:00"]])
    power_supply2.set_operating_time_range([["00:00", "08:00"],
                                            ["20:00", "00:00"]])
    # Minimizing the production coming from power grid
    power_supply1.minimize_production()
    power_supply2.minimize_production()

    # CONSUMPTION
    # Consumption profiles creation
    cons_profile_alex = [0] * nb_pdt
    cons_profile_ez_ju_sound = [0] * nb_pdt
    if balance:
        for lb in range(0, l_balance):
            # length of the balance
            cons_profile_alex[h_balance + lb] = p_alex
            cons_profile_ez_ju_sound[h_balance + lb] = p_ez_ju_sound
    if show:
        for hs in h_show:
            # for each placing
            for lp in range(0, int(l_placing)):
                # length of the placing
                cons_profile_alex[hs - l_placing + lp] = p_alex
            # for each show
            for ls in range(0, int(l_show)):
                # length of the show
                cons_profile_alex[hs + ls] = p_alex
                cons_profile_ez_ju_sound[hs + ls] = p_ez_ju_sound

    # Consumption units
    cons_alex = FixedConsumptionUnit(time, 'cons_alex', p=cons_profile_alex,
                                     energy_type='Electrical')

    cons_ez_ju_sound = FixedConsumptionUnit(time, 'cons_ez_ju_sound',
                                            p=cons_profile_ez_ju_sound,
                                            energy_type='Electrical')
    # In order for the problem to remain feasible when to much consumption.
    cons_excess1 = VariableConsumptionUnit(time, 'cons_exc1')
    cons_excess2 = VariableConsumptionUnit(time, 'cons_exc2')

    # STORAGE
    hourly_self_disch = 0.01 / 24  # 1% per day

    battery1 = StorageUnit(time, name='battery1',
                           pc_max=storage_pmax, pd_max=storage_pmax,
                           eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                           self_disch_t=hourly_self_disch, e_0=1,
                           ef_is_e0=e0_eq_ef, capacity=storage_capa)

    battery2 = StorageUnit(time, name='battery2',
                           pc_max=storage_pmax, pd_max=storage_pmax,
                           eff_c=0.88, eff_d=0.88, soc_min=0.2, soc_max=1,
                           self_disch_t=hourly_self_disch, e_0=1,
                           ef_is_e0=e0_eq_ef, capacity=storage_capa)

    # CONVERSION UNITS
    # mppt
    # Losses are taken into account in the PV profile calculation
    mppt1 = ElectricalConversionUnit(time=time, name='mppt1',
                                     elec_to_elec_ratio=1)
    mppt2 = ElectricalConversionUnit(time=time, name='mppt2',
                                     elec_to_elec_ratio=1)

    # rectifiers
    rec_eff = 0.88
    rectifier1 = ReversibleConversionUnit(time=time, name='rectifier1',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier2 = ReversibleConversionUnit(time=time, name='rectifier2',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier3 = ReversibleConversionUnit(time=time, name='rectifier3',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')
    rectifier4 = ReversibleConversionUnit(time=time, name='rectifier4',
                                          up2down_eff=rec_eff,
                                          down2up_eff=rec_eff,
                                          energy_type_up='Electrical',
                                          energy_type_down='Electrical')

    # inverters
    inverter_alex = ElectricalConversionUnit(time=time,
                                             name='inverter_alex',
                                             elec_to_elec_ratio=0.91)
    inverter_ez_ju_sound = ElectricalConversionUnit(time=time,
                                                    name=
                                                    'inverter_ez_ju_sound',
                                                    elec_to_elec_ratio=0.91)

    # ENERGY NODES
    node_pv1 = EnergyNode(time, 'node_pv1', 'Electrical')
    node_pv2 = EnergyNode(time, 'node_pv2', 'Electrical')

    node_bicycle1 = EnergyNode(time, 'node_bicycle1', 'Electrical')
    node_bicycle2 = EnergyNode(time, 'node_bicycle2', 'Electrical')
    node_bicycle3 = EnergyNode(time, 'node_bicycle3', 'Electrical')
    node_bicycle4 = EnergyNode(time, 'node_bicycle4', 'Electrical')

    node_video = EnergyNode(time, 'node_video', 'Electrical')
    node_sound_light = EnergyNode(time, 'node_sound_light', 'Electrical')

    node_alex = EnergyNode(time, 'node_alex', 'Electrical')
    node_ez_ju_sound = EnergyNode(time, 'node_ez_ju_sound', 'Electrical')

    node_pv1.connect_units(pv1, mppt1.elec_consumption_unit, cons_excess1)
    node_pv2.connect_units(pv2, mppt2.elec_consumption_unit, cons_excess2)
    node_bicycle1.connect_units(bicycle1.production_unit,
                                bicycle1.consumption_unit,
                                rectifier1.rev_unit_upstream.consumption_unit,
                                rectifier1.rev_unit_upstream.production_unit)
    node_bicycle2.connect_units(bicycle2.production_unit,
                                bicycle2.consumption_unit,
                                rectifier2.rev_unit_upstream.consumption_unit,
                                rectifier2.rev_unit_upstream.production_unit)
    node_bicycle3.connect_units(bicycle3.production_unit,
                                bicycle3.consumption_unit,
                                rectifier3.rev_unit_upstream.consumption_unit,
                                rectifier3.rev_unit_upstream.production_unit)
    node_bicycle4.connect_units(bicycle4.production_unit,
                                bicycle4.consumption_unit,
                                rectifier4.rev_unit_upstream.consumption_unit,
                                rectifier4.rev_unit_upstream.production_unit)

    node_video.connect_units(
        rectifier1.rev_unit_downstream.consumption_unit,
        rectifier1.rev_unit_downstream.production_unit,
        rectifier2.rev_unit_downstream.consumption_unit,
        rectifier2.rev_unit_downstream.production_unit,
        mppt1.elec_production_unit,
        inverter_alex.elec_consumption_unit,
        battery1, power_supply1)

    node_sound_light.connect_units(mppt2.elec_production_unit,
                                   rectifier3.rev_unit_downstream.
                                   consumption_unit,
                                   rectifier3.rev_unit_downstream.
                                   production_unit,
                                   rectifier4.rev_unit_downstream.
                                   consumption_unit,
                                   rectifier4.rev_unit_downstream.
                                   production_unit,
                                   inverter_ez_ju_sound.elec_consumption_unit,
                                   battery2, power_supply2)

    node_alex.connect_units(inverter_alex.elec_production_unit, cons_alex)
    node_ez_ju_sound.connect_units(inverter_ez_ju_sound.elec_production_unit,
                                   cons_ez_ju_sound)

    model.add_nodes(node_pv1, node_pv2, node_bicycle1,
                    node_bicycle2, node_bicycle3, node_bicycle4,
                    node_video, node_sound_light, node_alex, node_ez_ju_sound)

    # model.writeLP(work_path + r'\optim_models\oniri_new.lp')
    model.solve_and_update()

    return model, time, pv1, pv2, power_supply1, power_supply2, mppt1, mppt2, \
           bicycle1, bicycle2, bicycle3, bicycle4, battery1, battery2, \
           rectifier1, rectifier2, rectifier3, rectifier4, inverter_alex, \
           inverter_ez_ju_sound, node_pv1, node_pv2, node_bicycle1, \
           node_bicycle2, node_bicycle3, node_bicycle4, node_video, \
           node_sound_light, node_alex, node_ez_ju_sound


def print_results_oniri(model, time, battery1, battery2, node_pv1,
                        node_bicycle1, node_video, node_sound_light):
    """
        *** This function print the optimisation result
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        plot_quantity_bar(time=time, quantity=battery1.e,
                          title='État de charge de la batterie 1 sur la '
                                'période d\'étude')
        plot_quantity_bar(time=time, quantity=battery2.e,
                          title='État de charge de la batterie 2 sur la '
                                'période d\'étude')
        plot_node_energetic_flows(node_pv1)
        plot_node_energetic_flows(node_bicycle1)
        plot_node_energetic_flows(node_video)
        plot_node_energetic_flows(node_sound_light)
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")
    else:
        print("Sorry, the optimisation problem has not been solved.")


def save_results_oniri(file_name, node_video, node_sound_light):
    save_energy_flows(node_video, node_sound_light, file_name=file_name,
                      sep=';', decimal_sep=',')


if __name__ == '__main__':
    # OPTIMIZATION PARAMETERS #
    WORK_PATH = os.getcwd()
    # # ---------------------------
    # # --> DATA from Python file |
    # # ------------------
    # # CONSUMPTION DATA
    # # -----------------
    DT = 1 / 12  # 1/4 <=> 15min
    NB_H = 24
    SHOW = True
    BALANCE = False
    P_ALEX = 0.31
    p_ez = 0.084 + 0.050 + 0.010
    p_ju = 0.075 + 0.050 + 0.010
    p_kev = 0.0095
    P_EZ_JU_SOUND = p_ez + p_ju + p_kev
    H_BALANCE = 19  # balance hour
    L_BALANCE = 1 / 4  # balance length
    L_PLACING = 1 / 4  # placing length
    H_SHOW = [9.5, 14.25, 16]  # show hour
    L_SHOW = 1 / 3  # show length
    MOB = False  # True if using bicycles electrical assistance
    H_DEP = 8  # Departure hour
    H_ARR = 10  # Arrival hour
    P_MOB = 0.187  # /2 because 187Wh is the energy consumed during two
    # hours, so the power value will be 0.187/2
    # # ------------------
    # # PRODUCTION DATA
    # # ------------------
    # PV PROFILES
    # -----------
    # See http://re.jrc.ec.europa.eu/pvg_tools/en/tools.html#PVP
    # Monocristalline, losses 14%, 1kWc in Grenoble, on the 15th of
    # Februrary 2016, 1kW
    PV_PROFILE = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00435,
                  0.01865, 0.12174, 0.17084, 0.09912, 0.06432, 0.098,
                  0.01636, 0.00835, 0.00483, 0.0, 0.0, 0.0, 0.0,
                  0.0, 0.0, 0.0]
    PC_PV = 0.440
    H_PV_BEG = 10.5
    H_PV_END = 13.5
    # Bicycle generators :
    PMAX_BICY_PROD_NO_SHOW = 0.1  # 100W/bicycle
    PMAX_BICY_PROD_SHOW = PMAX_BICY_PROD_NO_SHOW / 4
    # # ----------------
    # # STORAGE DATA
    # # ----------------
    STORAGE_PMAX = 1
    STORAGE_CAPA = 21 * 48 * 1e-3
    E0_EQ_EF = True
    # STORAGE_CAPA_BICYCLE = 0.840  # 840Wh per bike

    # Run main
    MODEL, TIME, PV1, PV2, POWER_SUPPLY1, POWER_SUPPLY2, MPPT1, MPPT2, \
    BICYCLE1, BICYCLE2, BICYCLE3, BICYCLE4, BATTERY1, BATTERY2, RECTIFIER1, \
    RECTIFIER2, RECTIFIER3, RECTIFIER4, INVERTER_ALEX, INVERTER_EZ_JU_SOUND, \
    NODE_PV1, NODE_PV2, NODE_BICYCLE1, NODE_BICYCLE2, NODE_BICYCLE3, \
    NODE_BICYCLE4, NODE_VIDEO, NODE_SOUND_LIGHT, NODE_ALEX, NODE_EZ_JU_SOUND \
        = oniri(WORK_PATH, P_ALEX, P_EZ_JU_SOUND, H_BALANCE,
                H_SHOW, MOB, H_DEP, H_ARR, P_MOB, PV_PROFILE, PC_PV,
                STORAGE_PMAX,
                STORAGE_CAPA, SHOW, BALANCE, L_PLACING, L_BALANCE, L_SHOW,
                PMAX_BICY_PROD_NO_SHOW, PMAX_BICY_PROD_SHOW, NB_H, DT,
                H_PV_BEG, H_PV_END, E0_EQ_EF)

    print_results_oniri(MODEL, TIME, BATTERY1, BATTERY2, NODE_PV1,
                        NODE_BICYCLE1, NODE_VIDEO, NODE_SOUND_LIGHT)

    save_energy_flows(NODE_VIDEO, NODE_SOUND_LIGHT, file_name=
    WORK_PATH + r'\results\oniri_new', sep=';', decimal_sep=',')
