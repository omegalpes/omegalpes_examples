#! usr/bin/env python3
#  coding=utf-8 #

"""
    Ce module décrit des scénarios énergétiques pour le projet Oniri de
    la compagnie Organic Orchestra
    Commande à appliquer pour l'exécuter : py -m oniri_scenario
"""
import os
import csv
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.energy.units.conversion_units import ElectricalConversionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.plots import plt, plot_quantity, \
    plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows
from omegalpes.general.time import TimeUnit
from omegalpes.general.optimisation.elements import DynamicConstraint, \
    HourlyDynamicConstraint

__docformat__ = "restructuredtext en"


def main(work_path, p_var, t_var, p_spec, t_bal, h_balance, h_spectacle, mob,
         h_dep, h_arr, p_mob, profil_pv, pc_pv, pmax_spec,
         pmax_no_spec, stockage_pmax, ateliers, spectacles, h_ateliers,
         p_ateliers, nb_pdt):
    """
    :param production_pmax: Maximal power delivered by the production unit [kW]
    :type production_pmax: float or int
    :param stockage_pcharge_max: Maximal charging power for the stockage [kW]
    :type stockage_pcharge_max: float or int
    :param stockage_pdischarge_max: Maximal discharging power for the stockage
    [kW]
    :type stockage_pdischarge_max: float or int
    :param profil_conso: hourly conso profile during a day
    :type profil_conso: list of 24 float or int

    """

    # Creating an empty model
    time = TimeUnit(periods=nb_pdt, dt=1)  # Study on a day (24h)+1h, delta_t
    # = 1h
    model = OptimisationModel(name='scenario_oniri', time=time)

    # Creating the consumptions
    profil_conso = [0] * nb_pdt
    profil_conso[h_ateliers] = p_ateliers * ateliers[0]
    profil_conso[h_ateliers + 24] = p_ateliers * ateliers[1]
    profil_conso[h_balance] = p_spec * t_bal * spectacles[0]
    profil_conso[h_spectacle] = p_spec * spectacles[0]
    profil_conso[h_balance + 24] = p_spec * t_bal * spectacles[1]
    profil_conso[h_spectacle + 24] = p_spec * spectacles[1]
    conso_fixe = FixedConsumptionUnit(time, 'conso_f', p=profil_conso)

    list_pvar = nb_pdt * [p_var]
    conso_var = VariableConsumptionUnit(time, 'conso_v', pmax=list_pvar)
    e_var = 2 * p_var * t_var  # 2 jours
    conso_var.set_energy_limits_on_time_period(e_min=e_var, e_max=e_var)
    # Consommation excédentaire : permet de faire passer l'excédent de
    # consommation, autrement augmente la capacité de la batterie pour avoir
    #  plus de self_disch. Concrètement, il suffira de brancher des
    # appareils supplémentaires, ou bien de ranger les panneaux OPV.
    conso_excedent = VariableConsumptionUnit(time, 'conso_exc', pmax=pc_pv)
    profil_conso_mobilite = [0] * nb_pdt
    if mob == 'oui':
        for h in range(h_dep, h_arr):
            profil_conso_mobilite[h] = p_mob
    conso_mob = FixedConsumptionUnit(time, 'conso_mob',
                                     p=profil_conso_mobilite)

    # Creating the production unit - The production profile is unknown
    production_pmax = [0] * nb_pdt
    # h_ev = heures éveillés
    for h_ev in list(range(h_dep, h_spectacle + 1)) + list(
            range(h_dep + 24, h_spectacle + 25)):
        production_pmax[h_ev] = pmax_no_spec
    # h_spec : heure de spectacles ou d'atliers :
    h_spec = []
    if ateliers[0]:
        h_spec.append(h_ateliers)
    if ateliers[1]:
        h_spec.append(h_ateliers + 24)
    if spectacles[0]:
        h_spec.append(h_balance)
        h_spec.append(h_spectacle)
    if spectacles[1]:
        h_spec.append(h_balance + 24)
        h_spec.append(h_spectacle + 24)

    for h_s in h_spec:
        production_pmax[h_s] = pmax_spec

    if mob == 'oui':
        for h_mobi in range(h_dep, h_arr):
            production_pmax[h_mobi] = 0  # Pas de générations de puissance
            # pendant le voyage

    production_velo = VariableProductionUnit(time, 'prod_velo',
                                             pmax=production_pmax)
    production_velo.minimize_production()

    # Pas de production PV pendant le voyage
    # profil_prod_pv = [p * s_pv * (1-pertes_sys) * rendement *0.001 for p in
    #                   profil_pv]
    profil_prod_pv = [p * pc_pv * 0.001 for p in profil_pv]

    if mob == 'oui':
        for h in range(h_dep, h_arr):
            profil_prod_pv[h] = 0
    production_pv = FixedProductionUnit(time, name='prod_pv', p=profil_prod_pv)

    # Création stockage
    hourly_self_disch = 0.01 / 24  # 1% par jour
    stockage = StorageUnit(time, name='stockage',
                           pc_max=stockage_pmax,
                           pd_max=stockage_pmax, eff_c=0.88, eff_d=0.88,
                           soc_min=0.2, soc_max=1,
                           self_disch_t=hourly_self_disch,
                           ef_is_e0=True, capacity=2)
    # stockage = StorageUnit(time, name='stockage', eff_c=0.88, eff_d=0.88,
    #                        soc_min=0.2, soc_max=1,
    #                        self_disch_t=hourly_self_disch, ef_is_e0=True)

    # stockage.minimize_capacity(pc_max_ratio=0.1, pd_max_ratio=0.1,
    #                            weight=nb_pdt+1)
    # stockage.minimize_capacity(weight=nb_pdt+1)
    # Minimize the storage capacity, weight = nombre de pas de temps car
    # minimize prod velo prend chaque p de l'horizon temporel étudié

    # Creating the converters

    mppt = ElectricalConversionUnit(time, name='mppt', elec_to_elec_ratio=1)
    # losses are taken into account in the PV profile calculation

    conv_220 = ElectricalConversionUnit(time, name='conv_220V',
                                            elec_to_elec_ratio=0.91)

    conv_19 = ElectricalConversionUnit(time, name='conv_19V',
                                           elec_to_elec_ratio=0.9)

    conv_5 = ElectricalConversionUnit(time, name='conv_5V',
                                          elec_to_elec_ratio=0.9)

    conv_velo = ElectricalConversionUnit(time, name='conv_velo',
                                                  elec_to_elec_ratio=0.9)

    # Creating the energy node and connecting units

    node = EnergyNode(time, 'energy_node')
    node_velo = EnergyNode(time, 'node_velo')
    node_pv = EnergyNode(time, 'node_pv')
    node_220 = EnergyNode(time, 'node_220')
    node_19 = EnergyNode(time, 'node_19')
    # node_5 = EnergyNode(time, 'node_5')

    # Connect all units on the same energy node

    node_velo.connect_units(production_velo,
                            conv_velo.elec_consumption_unit)
    node_pv.connect_units(production_pv, mppt.elec_consumption_unit,
                          conso_excedent)
    node_220.connect_units(conv_220.elec_production_unit, conso_fixe,
                           conso_mob)
    node_19.connect_units(conv_19.elec_production_unit, conso_var)
    # node_5.connect_units(onduleur_5.elec_production_unit, )
    # Todo : distingeur conso 5, 19, 220V, ajouter onduleur 5V au modèle
    node.connect_units(conv_220.elec_consumption_unit,
                       conv_19.elec_consumption_unit,
                       mppt.elec_production_unit,
                       conv_velo.elec_production_unit, stockage)

    # Add the energy node to the model
    model.add_nodes(node, node_19, node_220, node_pv, node_velo)
    # print(""
    #       "TEST"
    #       "")
    # print(profil_pv)
    # print(profil_conso)
    # print(production_pmax)
    # Optimisation process
    model.writeLP(work_path + r'\optim_models\oniri.lp')
    model.solve_and_update()

    return model, time, conso_mob, conso_fixe, conso_excedent, conso_var, \
           production_velo, production_pv, stockage, node


def print_results():
    """
        *** This function print the optimisation result:
                - The optimal capacity

            And plot the power curves :
            Figure 1 :
                - Power consumed by the stockage, labelled 'stockage'
                - Power consumed by the conso, labelled 'conso'
                - Power delivered by the production unit, labelled 'Production'
            Figure 2 :
                - The state of charge of the stockage unit
    """

    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        # print("La capacité de stockage optimale est de {0} kWh".format(
        #     round(STOCKAGE.capacity.value, 3)))
        print("L'état de charge initial et final est de {1}"
              "kWh".format(round(STOCKAGE.e.value[0], 3),
                           round(STOCKAGE.e_f.value, 3)))
        print("L'énergie produite par les panneaux solaires est de {0} "
              "kWh".format(round(PROD_PV.e_tot.value, 3)))
        print("L'énergie produite par les pédaliers est de {0} "
              "kWh".format(round(PROD_VEL.e_tot.value, 3)))
        print("L'énergie consommée lors des ateliers, balances et spectacle "
              "est de {0} kWh".format(round(CONSO_FIXE.e_tot.value, 3)))
        print("L'énergie consommée hors spectacle (tablettes) est de {0} "
              "kWh".format(round(CONSO_VAR.e_tot.value, 3)))
        print("L'énergie consommée par les vélos électriques est de {0} "
              "kWh".format(round(CONSO_MOB.e_tot.value, 3)))

        # Show the graph
        # Power curves
        plot_node_energetic_flows(NODE)
        # SOC curve
        plot_quantity(TIME, STOCKAGE.e)
        plt.xlabel('Time (h)')
        plt.ylabel('Energy (kWh)')
        plt.title('State of charge of the storage system')
        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == '__main__':
    # OPTIMIZATION PARAMETERS #
    WORK_PATH = os.getcwd()
    # -------------------------------
    # --> DONNÉES depuis fichier csv|
    # -------------------------------
    with open(WORK_PATH + "\data\Parametrage_modele.csv") as csvfile:
        file_reader = csv.reader(csvfile, delimiter=';')
        # Colonne 0 : noms des paramètres, colonne 1 : valuer (sauf pour
        # listes)
        data_lists = [list(rows) for rows in file_reader]
        # On passe des virgules aux point en séparateur :
        value_lists = [[data.replace(',', '.') for data in d_list] for
                       d_list in data_lists]
        ATELIERS = [int(a) for a in value_lists[16][1].split('.')]
        SPECTACLES = [int(s) for s in value_lists[17][1].split('.')]
        value_lists[11].pop(0)
        PROFIL_PROD_PV_GRE = [float(v) for v in value_lists[11]]
        P_VAR = float(value_lists[1][1])
        T_VAR = float(value_lists[2][1])
        P_SPEC = float(value_lists[3][1])
        TAUX_CONSO_BALANCE = float(value_lists[4][1])
        H_BALANCE = int(value_lists[5][1])
        H_SPECTACLE = int(value_lists[6][1])
        MOBILITE = value_lists[7][1]
        H_DEPART = int(value_lists[8][1])
        H_ARRIVEE = int(value_lists[9][1])
        P_MOBILITE = float(value_lists[10][1]) / (H_ARRIVEE - H_DEPART)  #
        PC_PV = float(value_lists[12][1])
        PMAX_NO_SPEC = float(value_lists[13][1])
        PMAX_SPEC = float(value_lists[14][1])
        STOCKAGE_P_MAX = float(value_lists[15][1])
        H_ATELIERS = int(value_lists[18][1])
        P_ATELIERS = float(value_lists[19][1])
        NB_PDT = 48

    #
    # # ----------------------------------
    # # --> DONNÉES depuis fichier Python|
    # # ----------------------------------
    # # ---------------------
    # # DONNÉES CONSOMMATIONS
    # # ---------------------
    # # Puissance variable (tablettes Ju & Alex) : considérée pour
    # #  1h30, donc Evar=P_VAR*T_VAR
    # P_VAR = 0.0195
    # T_VAR = 1.5
    # # Puissance fixe : (somme des consommations Ezra Juliette et Alex moins
    # # le variable)
    # P_Fix = 0.144 + 0.352 + 0.136 - P_VAR
    # Taux_conso_balance = 0.75
    # # Conso pour le spectacle
    # H_BALANCE = 20  # Heure balance
    # H_SPECTACLE = 21  # Heure spectacle
    # PROFIL_CONSO_FIX = [0] * 25
    # PROFIL_CONSO_FIX[H_BALANCE] = P_Fix * Taux_conso_balance  # balances de
    # #  1h * Taux_conso_balance
    # PROFIL_CONSO_FIX[H_SPECTACLE] = P_Fix
    # MOBILITE = True  # True si utilisation d'assistance mobilité, False sinon
    # H_DEPART = 8  # Heure de départ
    # H_ARRIVEE = 10  # Heure d'arrivée
    # P_MOBILITE = 0.2 * 4 / (H_ARRIVEE-H_DEPART)  # Puissance d'assistance
    # # mobilité totale (200Wh pour 30/40km) : *4 pour 4 personnes, /2 car
    # # réparties sur 2h
    #
    # # -----------
    # # PROFILS PV
    # # -----------
    # # Voir http://re.jrc.ec.europa.eu/pvg_tools/en/tools.html#PVP
    # # Silicone monocristallin, rendement 20%, pertes 14%, 200Wc (environ
    # # 1m² du coup), 1er janvier 2016 à Corbel (Charteuse)
    # PROFIL_PROD_PV = [0, 0, 0, 0, 0, 0, 0.00274, 0.06335, 0.08304, 0.10211,
    #                   0.06441, 0.09136, 0.06068, 0.02485, 0, 0, 0, 0, 0, 0, 0,
    #                   0, 0, 0, 0]
    # # Organic PV approximé,rendement 4%, pertes 10%, 1m² (40Wc), 1er janvier
    # # 2016 à Grenoble
    # PROFIL_PROD_OPV_GRE = [0, 0, 0, 0, 0, 0, 0, 0.00103, 0.00231, 0.00411,
    #                        0.01719, 0.02237, 0.01969, 0.01317, 0.00190, 0,
    #                        0, 0, 0, 0, 0, 0, 0, 0, 0]
    # SURFACE_PV = 12
    #
    # # ------------------
    # # DONNÉES PRODUCTION
    # # ------------------
    # # Puissance max systèmes pédaliers :
    # PRODUCTION_P_MAX = [0] * 25
    # for h_ev in range(8, H_BALANCE):
    #     PRODUCTION_P_MAX[h_ev] = 0.1 * 4  # 4 pédaliers, 100W/pédalier si
    #     # éveillés
    # for h_spec in range(H_BALANCE, H_SPECTACLE + 1):
    #     PRODUCTION_P_MAX[h_spec] = 0.025 * 4  # 25W/pédalier pendant le
    #     # spectacle
    # for h_mobi in range(H_DEPART, H_ARRIVEE):
    #     PRODUCTION_P_MAX[h_mobi] = 0  # Pas de générations de puissance
    #     # pendant le voyage
    #
    # # ----------------
    # # DONNÉES STOCKAGE
    # # ----------------
    # # stockage maximal charging and discharging powers
    # STOCKAGE_P_MAX = 1

    # Run main
    MODEL, TIME, CONSO_MOB, CONSO_FIXE, CONSO_EX, CONSO_VAR, PROD_VEL, \
    PROD_PV, STOCKAGE, NODE = \
             main(work_path=WORK_PATH, p_var=P_VAR, t_var=T_VAR,
             p_spec=P_SPEC, t_bal=TAUX_CONSO_BALANCE, h_balance=H_BALANCE,
             h_spectacle=H_SPECTACLE, mob=MOBILITE,
             h_dep=H_DEPART, h_arr=H_ARRIVEE, p_mob=P_MOBILITE, profil_pv=
             PROFIL_PROD_PV_GRE, pc_pv=PC_PV, pmax_spec=PMAX_SPEC,
             pmax_no_spec=PMAX_NO_SPEC, stockage_pmax=STOCKAGE_P_MAX,
             ateliers=ATELIERS, spectacles=SPECTACLES, h_ateliers=H_ATELIERS,
             p_ateliers=P_ATELIERS, nb_pdt=NB_PDT)

    # Save energy flows into a CSV file
    save_energy_flows(NODE, file_name=WORK_PATH +
                                      '\\results\Oniri_scenario', sep=';',
                      decimal_sep=',')
    with open(WORK_PATH + r"\results\Oniri_resultats.csv", 'w', newline='') \
            as csvfile:
        file_writer = csv.writer(csvfile, delimiter=';')
        # Creating the output list
        capa_out = ['Capacité de stockage (kWh)',
                    str(round(STOCKAGE.capacity.value, 3)).replace('.', ',')]
        e_init_out = ["État de charge initial et final (kWh)",
                      str(round(STOCKAGE.e.value[0], 3)).replace('.', ',')]
        e_pv_out = ["Énergie prod PV (kWh)",
                    str(round(PROD_PV.e_tot.value, 3)).replace('.', ',')]
        e_pedal_out = ["Énergie prod pédaliers (kWh)",
                       str(round(PROD_VEL.e_tot.value, 3)).replace('.', ',')]
        e_spec_out = ["Énergie balances & spectacle (kWh)",
                      str(round(CONSO_FIXE.e_tot.value, 3)).replace('.', ',')]
        e_velo_out = ["Énergie conso mobilité (kWh)",
                      str(round(CONSO_MOB.e_tot.value, 3)).replace('.', ',')]
        e_var_out = ["Autre énergie conso (kWh)",
                     str(round(CONSO_VAR.e_tot.value, 3)).replace('.', ',')]
        output_list = [capa_out, e_init_out, e_pv_out, e_pedal_out,
                       e_spec_out, e_velo_out, e_var_out]
        for row in output_list:
            file_writer.writerow(row)

    # Show results
    # print(str(round(STOCKAGE.e.value[0], 3)).replace('.', ','))
    # print(type(str(round(STOCKAGE.e.value[0], 3)).replace('.', ',')))
    # print(type(str(round(STOCKAGE.capacity.value, 3))))
    print_results()
