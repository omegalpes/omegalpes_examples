#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    ** Module créé pour la preuve de concept d'interopérabilité entre PEGASE et
     OMEGAlpes. Il reprend un exemple de formation présent dans PEGASE**

"""

import os
from pulp import LpStatus, GUROBI_CMD, GLPK_CMD
import time as temps
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.energy.units.storage_units import StorageUnit, StorageUnitTm1
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.plots import plt, plot_quantity, \
    plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows


def comm_optim(work_path, p_demande, p_prod_max, cout_prod, e0, n_periods):
    # Creating an empty model
    model = OptimisationModel(name='com_optim')

    # Creating the unit dedicated to time management, and importing
    # time-dependent data
    time = TimeUnit(periods=n_periods,
                    dt=1 / 4)  # horizon de 2 jours, dt=15min

    # Creating the load profile
    heat_cons = FixedConsumptionUnit(time, 'heat_consumption',
                                     energy_type='Heat', p=p_demande)

    # Creating the production unit - the production profile is unknown
    heat_prod = VariableProductionUnit(time=time, name='heat_production',
                                       pmax=p_prod_max, pmin=0,
                                       energy_type='Heat',
                                       operating_cost=cout_prod)

    # Creating the storage unit
    thermal_storage = StorageUnit(time, 'thermal_storage', pc_max=1,
                                  pd_max=1, pd_min=0, pc_min=0, e_0=e0,
                                  capacity=8.54e+9 / (3600 * 1e6),
                                  eff_c=0.999, eff_d=0.999,
                                  self_disch_t=8.33e-4,
                                  energy_type='Heat')

    # Objective : Minimizing the part of the electrical load covered by the
    # electrical production plants
    heat_prod.minimize_operating_cost()

    # Creating the energy nodes and connecting units
    heat_node = EnergyNode(time, 'heat_node', energy_type='Heat')
    heat_node.connect_units(heat_cons, heat_prod, thermal_storage)

    # Adding the energy node to the model
    model.add_nodes(heat_node)

    print("time before solving: " + str(temps.clock()))
    # Optimisation process
    # model.writeLP(work_path + r'\optim_models\commande_optim_peg.lp')
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update values

    return model, time, heat_cons, heat_prod, thermal_storage, heat_node

# if __name__ == "__main__":
#     WORK_PATH = os.getcwd()
#     # *** OPTIMIZATION PARAMETERS ***
#     # File with hourly data for the consumption profile during the day
#     CONSUMPTION_PROFILE = "data/cons_file.txt"
#     COST_PROFILE = "data/cout_file.txt"
#
#     # *** RUN MAIN ***
#     MODEL, TIME, HEAT_CONS, HEAT_PROD, THERMAL_STO, HEAT_NODE = comm_optim(
#         work_path=WORK_PATH, cons_file=CONSUMPTION_PROFILE,
#         cout_file=COST_PROFILE)
#
#     # *** SHOW RESULTS ***
#     print_results()
