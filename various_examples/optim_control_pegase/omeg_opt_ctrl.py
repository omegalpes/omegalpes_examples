#! usr/bin/env python3
#  -*- coding: utf-8 -*-


"""
    ** Module créé pour la preuve de concept d'interopérabilité entre PEGASE et
     OMEGAlpes. Il contrôle le modèle OMEGAlpes commande_optim_pegase,
     et permet de lire et écrire des fichiers .csv**

"""

import os
import sys
import time as temps
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from pulp import LpStatus
from omegalpes_examples.various_examples import comm_optim
from omegalpes.general.plots import plt, plot_quantity, \
     plot_node_energetic_flows
import csv

def main(work_path=os.getcwd(),
         file_path=r'C:\Users\sh255625\Documents\Pegase\Projets\LNCMI'
                   r'\StoControlOptimInteropOmeg'):
    # --------------------
    # READING SOURCE FILES
    # --------------------
    cons_file_name = work_path + "/data/cons_file.txt"
    cout_file_name = work_path + "/data/cout_file.txt"
    p_demande_file = open(cons_file_name, "r")
    cout_prod_file = open(cout_file_name, "r")
    p_dem = [p for p in map(float, p_demande_file)]
    cout_prod = [c for c in map(float, cout_prod_file)]
    pprod_max = [8]*192
    esto_init = 2.3

    # # ------------------------
    # # READING FILE FROM PEGASE
    # # ------------------------
    # with open(file_path + "\PEGout.csv") as csvfile:
    #     file_reader = csv.reader(csvfile, delimiter=';', quotechar='|')
    #     data_lists = [list(rows) for rows in file_reader]
    # float_lists = [[float(data.replace('"', '')) for data in d_list] for
    #                d_list in data_lists]
    # p_dem = [p for p in float_lists[0]]
    # pprod_max = [p for p in float_lists[1]]
    # cout_prod = [c for c in float_lists[2]]
    # esto_init = [e for e in float_lists[3]]

    # ---> Ajout pas de temps fictif vide correspondant à l'état passé,
    # et permettant de placer e0 à la bonne temporalité
    # p_dem.insert(0, 0)
    # pprod_max.insert(0, 0)
    # cout_prod.insert(0, 0)


    n_pdt=len(p_dem)

    # *** RUN comm_optim ***
    model, time, heat_cons, heat_prod, thermal_sto, heat_node = comm_optim(
        work_path=work_path, p_demande=p_dem,
        p_prod_max=pprod_max, cout_prod=cout_prod,
        n_periods=n_pdt, e0=esto_init)

    # ------------------------
    # WRITING FILE FOR PEGASE
    # ------------------------
    time_list = []
    pch_opt_list = []
    pdisch_opt_list = []
    pprod_opt_list = []
    e_opt_list = []

    # ----> CALCUL [t+1]-[t]
    # for i in range(1, n_pdt):  # Le premier pas de temps correspond à
    #     # l'initialisation
    #     time_list.append(900 * i)
    #     pch_opt_list.append(list(thermal_sto.pc.value.values())[i])
    #     pdisch_opt_list.append(list(thermal_sto.pd.value.values())[i])
    #     pprod_opt_list.append(list(heat_prod.p.value.values())[i])
    #     e_opt_list.append(list(thermal_sto.e.value.values())[i])
    for i in range(0, n_pdt):
        time_list.append(900 * i)
        pch_opt_list.append(list(thermal_sto.pc.value.values())[i])
        pdisch_opt_list.append(list(thermal_sto.pd.value.values())[i])
        pprod_opt_list.append(list(heat_prod.p.value.values())[i])
    for i in range(1, n_pdt):
        e_opt_list.append(list(thermal_sto.e.value.values())[i])
    e_opt_list.append(thermal_sto.e_f.value)
    
    is_opt = [0] * (n_pdt-1)
    is_opt[0] = (LpStatus[model.status])

    # --> ECRITURE EN COLONNES AVEC HEADERS
    # # Headers
    # time_list.insert(0, "time")
    # pch_opt_list.insert(0, "PChargeConsOpt")
    # pdisch_opt_list.insert(0, "PDischargeConsOpt")
    # pprod_opt_list.insert(0, "PprodOpt")
    # e_opt_list.insert(0, "EstockOpt")
    # is_opt.insert(0, "OptimStatus")
    #
    # # Creating the output list (it needs to be ziped to be written in columns
    # output_list = [time_list, pch_opt_list, pdisch_opt_list, pprod_opt_list,
    #                e_opt_list, is_opt]
    #
    # with open(r'C:\Users\sh255625\Documents\Pegase\Projets'
    #           r'\PreuveConceptInterop'
    #           '\StockControlOptimOMEGAlpes\OMEGout.csv', 'w', newline='') \
    #         as csvfile:
    #     writer = csv.writer(csvfile, delimiter=';')
    #     for row in zip(*output_list):
    #         writer.writerow(row)
    #     print('OMEGout.csv created and written')

    # --> ECRITURE EN LIGNES SANS HEADERS

    with open(file_path + "\OMEGout.csv", 'w', newline='') \
            as csvfile:
        file_writer = csv.writer(csvfile, delimiter=';')
        # Creating the output list
        output_list = [time_list, pch_opt_list, pdisch_opt_list,
                       pprod_opt_list,
                       e_opt_list, is_opt]
        for row in output_list:
            file_writer.writerow(row)
        print('OMEGout.csv created and written')

    return model, time, heat_cons, heat_prod, thermal_sto, heat_node


def print_results():
    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print(
            'Heat Consumption = {0} MWh.'.format(HEAT_CONS.e_tot.value / 4))
        print('Heat production = {0} MWh'.format(HEAT_PROD.e_tot.value / 4))
        # division par 4 car pas de temps 15min
        print('Coût total: {0} €'.format(sum(HEAT_PROD.operating_cost.value)))

        # Power curves
        plot_node_energetic_flows(HEAT_NODE)
        # SOC curve
        plot_quantity(TIME, THERMAL_STO.e)
        plt.xlabel('Time (h)')
        plt.ylabel('Energy (MWh)')
        plt.title('State of charge of the storage system')
        # Show the graph
        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    print("t init: " + str(temps.clock()))
    WORK_PATH = os.getcwd()
    # FILE_PATH = R'D:\00 - Eco-SESA\02 - ' \
    #             R'OmegALPES\dev_omegalpes\examples\results'
    # FILE_PATH = R'C:\Users\sh255625\Documents\Pegase\Projets\LNCMI\StoControlOptimInteropOmeg'
    FILE_PATH = WORK_PATH
    MODEL, TIME, HEAT_CONS, HEAT_PROD, THERMAL_STO, HEAT_NODE = main(
        WORK_PATH, FILE_PATH)
    print("t fin: " + str(temps.clock()))

    print_results()
