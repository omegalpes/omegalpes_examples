#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    LNCMI study case : recovering waste heat from the electrical consumption of
     the LNCMI, with a system of storage & a heat pump to increase the
     temperature and inject on the district heating network
    """

import pandas as pd
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToHeatConversionUnit, HeatPump
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import ThermoclineStorage
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import ExtDynConstraint
from omegalpes.general.time import TimeUnit
from omegalpes.general.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows, \
    select_float_in_text_file_between_dates


def main(dt=1, start_date='01/01/2018 00:00', end_date='31/12/2018 23:50',
         winter_only=False,
         lncmi_cons_config='fixed', heat_pump_config=2,
         ill_on=True, e_lncmi=35430000, cciag_pmax=2e4, p_min_for_export=8500,
         lncmi_power=30, hp_elec_cost=0.0817):
    """
    :param dt: time step 1 = 1 hour, 1/6 = 10 min
    :type dt: float
    :param start_date : start date of the study horizon
    :type start_date: string date 'DD/MM/YYY HH:MM'
    :param end_date : end date of the study horizon
    :type end_date: string date 'DD/MM/YYY HH:MM'
    :param winter_only: if True, the heat injection on the network is only
    possible during the winter heating period
    :type winter_only: bool
    :param lncmi_cons_config: LNCMI consumption, can be: 'free' or 'fixed' (
    that is to say power profiles of the previous year)
    :type lncmi_cons_config: str
    :param heat_pump_config: Should be set to 1, 2 or 3 for Carrier CCIAG,
    Carrier CEA or Johnson CEA
    :type heat_pump_config: int
    :param ill_on: Indicates if the ill is on: True or not: False
    :type ill_on: bool
    :param e_lncmi: Energy which has to be consumed by the LNCMI during the
    time range
    :type e_lncmi: int or float
    :param cciag_pmax: Maximal heating power delivered by the CCIAG to the
    district
    :type cciag_pmax: int or float
    :param p_min_for_export: Minimal heating power required for the LNCMI to
    export
    :type p_min_for_export: int or float
    :param lncmi_power: max power of the LNCMI magnets, either 24, 30 or 36
    (MW)
    :type lncmi_power: int
    :param hp_elec_cost: price of the electricity consumed by the heat_pump
    :type hp_elec_cost: float

    """

    global time, lncmi, district_heat_load, heat_pump, heat_production, \
        heat_node_aft_valve, heat_node_aft_hp, heat_node_bef_valve, \
        model, dissipation, elec_node, elec_prod

    # --- OPTIMIZATION MODEL CREATION ---
    # Creating the unit dedicated to time management
    time = TimeUnit(start=start_date, end=end_date, dt=dt)

    # Creating an empty model
    model = OptimisationModel(name='lncmi_model', time=time)

    # --- LOADING FILES ---
    # lncmi_cons_file : 10min data points --> dt=1/6
    # drac_temp_file : 1 hour data points --> dt=1/6
    # district_consumption_file : 1 hour data points --> dt=1
    # heating_network_temp_file : 1 hour data points --> dt=1
    lncmi_cons_origin = select_float_in_text_file_between_dates(
        "data/lncmi_consumption_v6_10_min.txt", start_date, end_date, 1 / 6)
    drac_temp = select_float_in_text_file_between_dates(
        "data/Tdrac10min.txt", start_date, end_date, 1 / 6)
    district_consumption = select_float_in_text_file_between_dates(
        'data/district_heat_load_predicted.txt', start_date, end_date, 1)
    heating_network_temp = select_float_in_text_file_between_dates(
        "data/loi_eau_approx.txt", start_date, end_date, 1)

    # 10min data points
    lncmi_power_ratio = lncmi_power / 24  # in order to translate the
    # consumption from 24 MW to 30 or 36 MW for instance
    lncmi_cons = [lncmi_power_ratio * c for c in lncmi_cons_origin]
    p_lncmi = []
    t_drac = []

    # --- ADAPTING DATA SETS TO THE TIME STEP ---
    if dt < 1:
        for c in lncmi_cons:
            p_lncmi += [c] * int(1 / (6 * dt))
        for t in drac_temp:
            t_drac += [t] * int(1 / (6 * dt))
    elif dt == 1:
        k = 0
        while k < len(lncmi_cons):
            # calculating the mean value of the 10min points
            p_lncmi.append(round(sum(lncmi_cons[k:k + 6]) / 6, 2))
            t_drac.append(round(sum(drac_temp[k:k + 6]) / 6, 2))
            k += 6
    else:
        raise ValueError("dt should be set between 1/6 and 1 but is set "
                         "to {0}".format(dt))
    # 1 hour data points
    p_heat_load = []
    for d in district_consumption:
        p_heat_load += [d] * int(1 / dt)

    t_heating_network = []
    for t in heating_network_temp:
        t_heating_network += [t] * int(1 / dt)

    # --- LNCMI CREATION ---
    if lncmi_cons_config == 'fixed':
        lncmi = ElectricalToHeatConversionUnit(time, 'lncmi',
                                               elec_to_heat_ratio=0.85,
                                               p_in_elec=p_lncmi, e_max=5e7)
    elif lncmi_cons_config == 'free':
        # Only the annual consumed energy is fixed
        lncmi = ElectricalToHeatConversionUnit(time, 'lncmi',
                                               pmax_in_elec=lncmi_power * 1e3
                                                            + 2e3,
                                               elec_to_heat_ratio=0.85)
        lncmi.elec_consumption_unit.set_energy_limits_on_time_period(
            e_min=e_lncmi, e_max=e_lncmi)

    else:
        raise ValueError(
            "The lncmi consumption configuration should be free or fixed and "
            "is set to {0}".format(lncmi_cons_config))

    # --- HEAT DISSIPATION CREATION ---
    isere_max_temp = 29  # environmental constraint
    RHO = 998.30  # Water density at 20 °C [kg·m-3]
    cp = 4185.5  # Water specific heat at 15°C [J·kg-1·K-1]
    cp /= 1000  # Water specific heat at 15°C [kW.s·kg-1·K-1]
    max_flow_ill_on = 0.333  # Maximum water flow [m3.s-1] when the ILL is in
    #  operation (about 4 months a year)
    max_flow_ill_off = 0.5  # Maximum water flow [m3.s-1] when the ILL is not
    #  in operation.
    max_flow = max_flow_ill_on if ill_on else max_flow_ill_off

    max_dissip_power = [RHO * cp * max_flow * (isere_max_temp - temp) for
                        temp in t_drac]

    new_max_dissip_power = []
    for c in max_dissip_power:
        new_max_dissip_power += [c] * int(1 / dt)
    max_dissip_power = [round(elem, 2) for elem in new_max_dissip_power]

    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          pmax=max_dissip_power, e_max=5e7,
                                          energy_type='Heat')

    # --- DISTRICT HEAT LOAD CREATION ---
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=p_heat_load,
                                              energy_type='Heat')

    # --- HEAT PUMP CREATION ---
    if heat_pump_config == 1:  # Carrier CCIAG
        cop_hp = 3.25
        t_pac = 85  # District heating injection temperature
        # losses_hp = 0.01
        pmax_elec = 1140
        pmin_elec = 0.17 * pmax_elec
        # storage_costs = [0, 0, 0, 0]  # No storage costs known for this
        # configuration
        # hp_cost = 0  # No heat pump cost known for this configuration
    elif heat_pump_config == 2:  # Carrier LST
        cop_hp = 3
        t_pac = 85  # District heating injection temperature
        # losses_hp  = 0.016
        pmax_elec = 1260
        pmin_elec = 0.17 * pmax_elec
        # storage_costs = [900, 1800, 2700, 3600]
        # hp_cost = 810362
    elif heat_pump_config == 3:  # Johnson LST
        cop_hp = 3.43
        t_pac = 90  # District heating injection temperature
        # losses_hp = 0.005
        pmax_elec = 1021
        pmin_elec = 0.2 * pmax_elec
        # storage_costs = [1350, 2700, 4050, 5400]
        # hp_cost = 780000
    elif heat_pump_config == 4:  # Carrier LST 50°C
        cop_hp = 4.29
        t_pac = 85  # District heating injection temperature
        # losses_hp  = 0.016
        # pmax_elec = 784.8
        pmax_elec = 881
        # pmin_elec = 0.17 * pmax_elec
        pmin_elec = 0.01 * pmax_elec

        # storage_costs = [900, 1800, 2700, 3600]
        # hp_cost = 810362
    else:
        raise ValueError("The heat pump configuration should be 1, 2 or 3 and "
                         "is set to {0}".format(heat_pump_config))

    # --- NETWORK TEMPERATURE INFLUENCE ---
    t_return = 70  # supposed return temperature on the heating network
    temp_influence_ratio = []
    for t_net in t_heating_network:
        temp_influence_ratio += [(t_pac - t_return) / (t_net - t_return)]

    pmax_injection = [round(h_l * t_i_r, 2) for h_l, t_i_r
                      in zip(p_heat_load, temp_influence_ratio)]

    if winter_only:
        for summer_time in range(2161*int(1/dt), 7296*int(1/dt)+1):
            # April  -->  October
            pmax_injection[summer_time] = 0

    # Creating the heat pump
    heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp, pmin_in_elec=pmin_elec,
                         pmax_in_elec=pmax_elec, pmax_out_heat=pmax_injection,
                         e_max=5e7)  # , losses=losses_hp)

    # Creating the source of electricity for heat pump
    elec_prod = VariableProductionUnit(time, 'elec_prod',
                                       operating_cost=hp_elec_cost,  e_max=5e7,
                                       energy_type='Electrical')

    # --- CCIAG PRODUCTION CREATION ---
    heat_production = VariableProductionUnit(time, name='heat_production',
                                             energy_type='Heat', e_max=5e7,
                                             pmax=cciag_pmax)  # Creating the
    #  heat production plants

    # --- HEAT NODES CREATION ---
    # Creating the heat node for the energy flows
    heat_node_bef_valve = EnergyNode(time, 'heat_node_bef_valve',
                                     energy_type='Heat')
    heat_node_aft_valve = EnergyNode(time, 'heat_node_aft_valve',
                                     energy_type='Heat')
    heat_node_aft_hp = EnergyNode(time, 'heat_node_aft_hp', energy_type='Heat')
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')

    # Connecting units to the nodes
    heat_node_bef_valve.connect_units(lncmi.heat_production_unit, dissipation)
    heat_node_bef_valve.export_to_node(
        heat_node_aft_valve)  # Export after the valve 1
    heat_node_aft_valve.connect_units(heat_pump.heat_consumption_unit)
    elec_node.connect_units(elec_prod, heat_pump.elec_consumption_unit)
    heat_node_aft_hp.connect_units(heat_pump.heat_production_unit,
                                   heat_production, district_heat_load)
    # --- PMIN RECOVERY CONSTRAINT ---
    pmin_rec_cst_exp = '{0}_heat_prod_p[t] >= {1} * heat_node_bef_valve_is' \
                       '_exporting_to_heat_node_aft_valve[t]' \
        .format(lncmi.name, p_min_for_export)
    heat_node_bef_valve.export_min = \
        ExtDynConstraint(name='pmin_recovery', exp_t=pmin_rec_cst_exp,
                         t_range='for t in time.I',
                         parent=heat_node_bef_valve)

    # --- OBJECTIVE CREATION ---
    # Objective : Minimizing the part of the heat load covered by the heat
    # production plants
    heat_production.minimize_production()

    # --- ADDING ALL UNITS TO THE OPTIMIZATION MODEL ---
    model.add_nodes(heat_node_bef_valve, heat_node_aft_valve,
                    heat_node_aft_hp, elec_node)

    # Writing into lp file
    model.writeLP('optim_models\lncmi_study_case.lp')

    # --- SOLVING AND UPDATING ---
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update values


def print_results():
    """
        *** This function print the optimisation results:
                - The district heat consumption during the year
                - The LNCMI electrical consumption during the year
                - The CCIAG production during the year
                - The heat from the LNCMI injected on the district heating
                network
                - The heat pump electricity & heat consumptions
                - The percentage of the heat load covererd by the heat pump
                - The percentage of the LNCMI heat that is recovered

            And plot the power curves :
                - Figure 1 : The energy flows between the LNCMI heat
                production, the heat dissipation and the export to the waste
                heat recovery system
                - Figure 2 : The energy flows between the heat imported from
                the LNCMI, the thermocline storage and the heat pump heat
                consumption
                - Figure 3 : The energy flows between the heat pump heat
                production, the cciag heat production, and the district heat
                load

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} GWh.'.format(
            round(district_heat_load.e_tot.value / 1e6, 2)))
        print('LNMCI electrical consumption (GWh)= ',
              round(lncmi.elec_consumption_unit.e_tot.value / 1e6, 2))
        print('CCIAG production (GWh)= ', round(heat_production.e_tot.value
                                                / 1e6, 2))
        print('LNMCI heat injected on the district heating network by the '
              'Heat Pump (GWh)= ', round(
            heat_pump.heat_production_unit.e_tot.value / 1e6, 2))
        print('Heat_pump heat consumption (GWh)= ',
              round(heat_pump.heat_consumption_unit.e_tot.value / 1e6, 2))
        print('Heat pump electricity consumption (GWh)= ',
              round(heat_pump.elec_consumption_unit.e_tot.value / 1e6, 2))
        print("{0} % of the load coming from the heat pump".format(
            round(heat_pump.heat_production_unit.e_tot.value /
                  district_heat_load.e_tot.value * 100)))
        # value is a dict, with time as a key, and power levels as values.

        print("{0} % of the LNCMI heat is recovered".format(
            round(sum(
                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                    .value.values()) * DT /
                  lncmi.heat_production_unit.e_tot.value * 100)))
        # value is a results_dict, with time as a key, and power levels as
        # values.

        plot_node_energetic_flows(heat_node_bef_valve)
        plot_node_energetic_flows(heat_node_aft_valve)
        plot_node_energetic_flows(heat_node_aft_hp)
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    # --- PARAMETERS ---
    DT = 1  # Delta t (=1 for one hour ; 1/6 for 10min)
    START_DATE = "01/01/2018 00:00"  # starting date of the study, format
    # DD/MM/YYYY HH:MM
    END_DATE = "31/12/2018 23:50"  # ending date of the study
    WINTER_ONLY = False  # The waste can only be recovered in winter i.e. from
    # November to March
    LNCMI_CONS_CONFIG = 'fixed'  # Choose between 'free' and 'fixed'
    ILL_ON = False  # ILL in operation or not
    E_SET_LNCMI = 35430000  # Electrical power consumed by the LNCMI during a
    #  year [kWh]
    CCIAG_PMAX = 2e4
    HP_ELEC_COSTS = 0.0817  # 81,7 €/MWh
    HEAT_PUMP_NAME = ['Carrier CCIAG', 'Carrier LST', 'Johnson LST',
                      'Carrier 50deg']
    PMIN_EXPORT = [8500]
    LNCMI_POWER = [30]
    NAME = 'test_no_storage_Carrier'

    # --- PARAMETRIC RUN ---
    for PM in PMIN_EXPORT:
        for P in LNCMI_POWER:
            results_dict = {}
            columns_index = ['Config']
            for HEAT_PUMP_CONFIG in [2]:
                columns_index.append(HEAT_PUMP_NAME[HEAT_PUMP_CONFIG - 1])
                results_list = []
                lines_index = []
                main(DT, START_DATE, END_DATE, WINTER_ONLY,
                     LNCMI_CONS_CONFIG,
                     HEAT_PUMP_CONFIG, ILL_ON, cciag_pmax=CCIAG_PMAX,
                     e_lncmi=E_SET_LNCMI, p_min_for_export=PM,
                     lncmi_power=P, hp_elec_cost=HP_ELEC_COSTS)

                # Short results
                try:
                    print(
                        "{0} % of the LNCMI heat is recovered".format(
                            round(sum(
                                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                                    .value.values()) /
                                  lncmi.heat_production_unit.e_tot.value *
                                  100)))

                    print(
                        "{0} GWh of the LNCMI heat is recovered".format(
                            round(sum(
                                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                                    .value.values()) / 1000) / 1000))
                except:
                    pass

                # --- RESULTS FILES ---
                # Energy flows for each storage and heat pump configuration
                file_name = \
                    'results\study_case\lncmi_flows_storage_capa_hp_' \
                    'conf_{0}_lncmi_conf_{1}_{2}_MW_PminValo_{3}MW_' \
                    'ILL_OFF_{4}' \
                        .format(HEAT_PUMP_CONFIG,
                                LNCMI_CONS_CONFIG, P, PM / 1e3, NAME)

                save_energy_flows(heat_node_bef_valve,
                                  heat_node_aft_valve, heat_node_aft_hp,
                                  elec_node, file_name=file_name, sep=';')
            # Parametric result file
                # Recovered heat (before heat pump)
                results_list.append(round(sum(
                    heat_node_bef_valve._exports[0].value.values()) / 1e6,
                                          2))
                results_dict['Config'] = lines_index
                results_dict[
                    columns_index[1]] = results_list
                # results_dict[
                #     columns_index[HEAT_PUMP_CONFIG - 1]] = results_list

            df = pd.DataFrame(results_dict, columns=columns_index)
            df.to_csv(
                'results\study_case\study_case_{0}MW_PminValo_{1}MW_'
                'ILL_OFF_{2}'.format(P, PM / 1000, NAME),
                header=True,
                index=False, sep=';')

            # print_results()
