#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
   LNCMI return recovery : recovering waste heat from the electrical
   consumption of the LNCMI, with a system of storage &  heat pump(s) to
   increase the temperature and inject the heat on the return of the
   primary district heating network
"""

from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import VariableConsumptionUnit
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.storage_units import ThermoclineStorage, \
    StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import ExtDynConstraint
from omegalpes.general.time import TimeUnit
from omegalpes.general.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.utils import save_energy_flows, \
    select_float_in_text_file_between_dates
import csv
import pandas as pd


def main(dt=1, start_date='01/01/2018 00:00', end_date='31/12/2018 23:50',
         winter_only=False, lncmi_cons_config='free', storage_capa=20,
         heat_pump_config=2, ill_on=True,
         e_lncmi=35430000, p_min_for_export=2000,
         lncmi_power=30, e_0_sto=None, hp_elec_cost=0.0817, nb_hp=1):
    """
     :param dt: time step 1 = 1 hour, 1/6 = 10 min
     :type dt: float
     :param start_date : start date of the study horizon
     :type start_date: string date 'DD/MM/YYY HH:MM'
     :param end_date : end date of the study horizon
     :type end_date: string date 'DD/MM/YYY HH:MM'
     :param winter_only: if True, the heat injection on the network is only
     possible during the winter heating period
     :type winter_only: bool
     :param lncmi_cons_config: LNCMI consumption, can be: 'free' or 'fixed' (
     that is to say power profiles of the previous year)
     :type lncmi_cons_config: str
     :param storage_capa: storage capacity (MWh)
     :type storage_capa: int
     :param heat_pump_config: Should be set to 1, 2 or 3 for Carrier CCIAG,
     Carrier CEA or Johnson CEA
     :type heat_pump_config: int
     :param ill_on: Indicates if the ill is on: True or not: False
     :type ill_on: bool
     :param e_lncmi: Energy which has to be consumed by the LNCMI during the
     time range
     :type e_lncmi: int or float
     :param cciag_pmax: Maximal heating power delivered by the CCIAG to the
     district
     :type cciag_pmax: int or float
     :param p_min_for_export: Minimal heating power required for the LNCMI to
     export
     :type p_min_for_export: int or float
     :param lncmi_power: max power of the LNCMI magnets, either 24, 30 or 36
     (MW)
     :type lncmi_power: int
     :param e_0_sto: initial storage state of charge (kWh)
     :type e_0_sto: int or float
     :param hp_elec_cost: price of the electricity consumed by the heat_pump
     :type hp_elec_cost: float
     :param nb_hp: number of heat pumps
     :type nb_hp: int
     """

    global time, lncmi, heating_network_return, heat_pump, \
        heat_node_aft_valve, heat_node_aft_hp, heat_node_bef_valve, \
        thermocline_storage, model, dissipation, elec_node, elec_prod

    # --- OPTIMIZATION MODEL CREATION ---
    # Creating an empty model
    # Creating the unit dedicated to time management
    time = TimeUnit(start=start_date, end=end_date, dt=dt)
    model = OptimisationModel(name='lncmi_model_return', time=time)

    # --- LOADING FILES ---
    # lncmi_cons_file : 10min data points --> dt=1/6
    # drac_temp_file : 1 hour data points --> dt=1/6
    lncmi_cons_origin = select_float_in_text_file_between_dates(
        "data/lncmi_consumption_v6_10_min.txt", start_date, end_date, 1 / 6)
    drac_temp = select_float_in_text_file_between_dates(
        "data/Tdrac10min.txt", start_date, end_date, 1 / 6)

    # 10min data points
    lncmi_power_ratio = lncmi_power / 24  # in order to translate the
    # consumption from 24 MW to 30 or 36 MW for instance
    lncmi_cons = [lncmi_power_ratio * c for c in lncmi_cons_origin]
    p_lncmi = []
    t_drac = []

    # --- ADAPTING DATA SETS TO THE TIME STEP ---
    if dt < 1:
        for c in lncmi_cons:
            p_lncmi += [c] * int(1 / (6 * dt))
        for t in drac_temp:
            t_drac += [t] * int(1 / (6 * dt))
    elif dt == 1:
        k = 0
        while k < len(lncmi_cons):
            # calculating the mean value of the 10min points
            p_lncmi.append(round(sum(lncmi_cons[k:k + 6]) / 6, 2))
            t_drac.append(round(sum(drac_temp[k:k + 6]) / 6, 2))
            k += 6
    else:
        raise ValueError("dt should be set between 1/6 and 1 but is set "
                         "to {0}".format(dt))

    # --- LNCMI CREATION ---
    if lncmi_cons_config == 'fixed':
        lncmi = ElectricalToHeatConversionUnit(time, 'lncmi',
                                               elec_to_heat_ratio=0.85,
                                               p_in_elec=p_lncmi, e_max=5e7)
    elif lncmi_cons_config == 'free':
        # Only the annual consumed energy is fixed
        lncmi = ElectricalToHeatConversionUnit(time, 'lncmi',
                                               pmax_in_elec=lncmi_power * 1e3
                                                            + 2e3,
                                               elec_to_heat_ratio=0.85)
        lncmi.elec_consumption_unit.set_energy_limits_on_time_period(
            e_min=e_lncmi, e_max=e_lncmi)

    else:
        raise ValueError(
            "The lncmi consumption configuration should be free or fixed and "
            "is set to {0}".format(lncmi_cons_config))

    # --- HEAT DISSIPATION CREATION ---
    isere_max_temp = 29  # environmental constraint
    RHO = 998.30  # Water density at 20 °C [kg·m-3]
    cp = 4185.5  # Water specific heat at 15°C [J·kg-1·K-1]
    cp /= 1000  # Water specific heat at 15°C [kW.s·kg-1·K-1]
    max_flow_ill_on = 0.333  # Maximum water flow [m3.s-1] when the ILL is in
    #  operation (about 4 months a year)
    max_flow_ill_off = 0.5  # Maximum water flow [m3.s-1] when the ILL is not
    #  in operation.
    max_flow = max_flow_ill_on if ill_on else max_flow_ill_off

    max_dissip_power = [RHO * cp * max_flow * (isere_max_temp - temp) for
                        temp in t_drac]

    new_max_dissip_power = []
    for c in max_dissip_power:
        new_max_dissip_power += [c] * int(1 / dt)
    max_dissip_power = [round(elem, 2) for elem in new_max_dissip_power]

    dissipation = VariableConsumptionUnit(time, 'dissipation',
                                          pmax=max_dissip_power, e_max=5e7,
                                          energy_type='Heat')

    # --- STORAGE CREATION ---
    # Storage configurations
    capacity = storage_capa * 1e3
    # pc_max = 4e4
    pc_max = round(capacity / 3, 3)
    pd_max = pc_max

    # Creating the thermocline storage
    thermocline_storage = ThermoclineStorage(time, 'thermocline_storage',
                                             pc_max=pc_max, pd_max=pd_max,
                                             pc_min=0.001 * pc_max,
                                             pd_min=0.001 * pd_max,
                                             capacity=capacity, e_0=e_0_sto,
                                             e_max=5e7, Tcycl=120)

    # --- HEAT PUMP CREATION ---
    if heat_pump_config == 1:  # Carrier CCIAG
        cop_hp = 3.25
        losses_hp = 0.01
        pmax_elec = 1140
        pmin_elec = 0.17 * pmax_elec
    elif heat_pump_config == 2:  # Carrier LST dt=20°C
        cop_hp = 2.6
        losses_hp = 0.016
        pmax_elec = 1384.62
        pmin_elec = 0.17 * pmax_elec
    elif heat_pump_config == 3:  # Carrier LST dt=15°C
        cop_hp = 2.9
        losses_hp = 0.016
        pmax_elec = 1241.38
        pmin_elec = 0.17 * pmax_elec

    else:
        raise ValueError("The heat pump configuration should be 1, 2 or 3 and "
                         "is set to {0}".format(heat_pump_config))

    pmax_injection = [cop_hp * pmax_elec] * time.LEN
    if winter_only:
        for summer_time in range(2161 * int(1 / dt), 7296 * int(1 / dt) + 1):
            # April  -->  October
            pmax_injection[summer_time] = 0

    heat_pump_list = []
    heat_pump_cons_list = []
    heat_pump_prod_list = []
    heat_pump_elec_cons_list = []

    for k in range(nb_hp):
        name = 'nb_{0}_heat_pump'.format(k + 1)
        cmd = name + ' = HeatPump(time, "' + name + '", cop=cop_hp, ' \
                                                    'pmax_in_elec=pmax_elec,' \
                                                    'pmin_in_elec=pmin_elec,' \
                                                    'pmax_out_heat=pmax_injection, ' \
                                                    'e_max=5e7)'
        exec(cmd)

        heat_pump_list.append(eval(name))
        heat_pump_cons_list.append(eval(name + '.heat_consumption_unit'))
        heat_pump_prod_list.append(eval(name + '.heat_production_unit'))
        heat_pump_elec_cons_list.append(eval(name + '.elec_consumption_unit'))
    # --- DISTRICT HEAT LOAD CREATION ---
    # Creating the district heat load
    heating_network_return = VariableConsumptionUnit(time,
                                                     'heating_network_return',
                                                     energy_type='Heat',
                                                     e_max=5e7)

    # Creating the source of electricity for heat pump
    elec_prod = VariableProductionUnit(time, 'elec_prod',
                                       operating_cost=hp_elec_cost, e_max=5e7,
                                       energy_type='Electrical')

    # --- HEAT NODES CREATION ---
    # Creating the heat node for the energy flows
    heat_node_bef_valve = EnergyNode(time, 'heat_node_bef_valve',
                                     energy_type='Heat')
    heat_node_aft_valve = EnergyNode(time, 'heat_node_aft_valve',
                                     energy_type='Heat')
    heat_node_aft_hp = EnergyNode(time, 'heat_node_aft_hp', energy_type='Heat')
    elec_node.connect_units(elec_prod, *heat_pump_elec_cons_list)

    # Connecting units to the nodes
    heat_node_bef_valve.connect_units(lncmi.heat_production_unit, dissipation)
    heat_node_bef_valve.export_to_node(
        heat_node_aft_valve)  # Export after the valve 1
    heat_node_aft_valve.connect_units(thermocline_storage,
                                      *heat_pump_cons_list)
    elec_node.connect_units(elec_prod, heat_pump.elec_consumption_unit)
    heat_node_aft_hp.connect_units(heating_network_return,
                                   *heat_pump_prod_list)
    # --- PMIN RECOVERY CONSTRAINT ---
    pmin_rec_cst_exp = '{0}_heat_prod_p[t] >= {1} * heat_node_bef_valve_is' \
                       '_exporting_to_heat_node_aft_valve[t]' \
        .format(lncmi.name, p_min_for_export)
    heat_node_bef_valve.export_min = \
        ExtDynConstraint(name='pmin_recovery', exp_t=pmin_rec_cst_exp,
                         t_range='for t in time.I',
                         parent=heat_node_bef_valve)

    # --- OBJECTIVE CREATION ---
    # Objective : Minimizing the part of the heat load covered by the heat
    # production plants
    # dissipation.minimize_consumption()
    heating_network_return.maximize_consumption()

    # --- ADDING ALL UNITS TO THE OPTIMIZATION MODEL ---
    model.add_nodes(heat_node_bef_valve, heat_node_aft_valve,
                    heat_node_aft_hp, elec_node)
    # Writing into lp file
    model.writeLP('optim_models\lncmi_return_recovery.lp')

    # --- SOLVING AND UPDATING ---
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update
    # values


def print_results():
    """
        *** This function print the optimisation results:
                - The heat injected on the district heating network
                - The LNCMI electrical consumption during the year
                - The heat from the LNCMI injected on the district heating
                network
                - The heat pump electricity & heat consumptions
                - The percentage of the LNCMI heat that is recovered

            And plot the power curves :
                - Figure 1 : The energy flows between the LNCMI heat
                production, the heat dissipation and the export to the waste
                heat recovery system
                - Figure 2 : The energy flows between the heat imported from
                the LNCMI, the thermocline storage and the heat pump heat
                consumption
                - Figure 3 : The energy flows between the heat pump heat
                production, the cciag heat production, and the district heat
                load

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District return heat recovered (kWh) =',
              heating_network_return.e_tot)
        print('LNMCI electrical consumption (GWh)= ',
              round(lncmi.elec_consumption_unit.e_tot.value / 1e6, 2))
        print('LNMCI heat injected on the district heating network by the '
              'Heat Pump (GWh)= ', round(
            heat_pump.heat_production_unit.e_tot.value / 1e6, 2))
        print('Heat pump electricity consumption = ',
              round(sum(elec_node._exports.value.values()) * DT))

        print("{0} % of the LNCMI heat is recovered".format(
            round(sum(
                heat_node_bef_valve.energy_export_to_heat_node_aft_valve
                    .value.values()) * DT /
                  lncmi.heat_production_unit.e_tot.value * 100)))
        # value is a results_dict, with time as a key, and power levels as
        # values.

        plot_node_energetic_flows(heat_node_bef_valve)
        plot_node_energetic_flows(heat_node_aft_valve)
        plot_node_energetic_flows(heat_node_aft_hp)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    # PARAMETERS #
    DT = 1  # Delta t (=1 for one hour ; 1/6 for 10min)
    START_DATE = "01/01/2018 00:00"  # starting date of the study, format
    # DD/MM/YYYY HH:MM
    END_DATE = "31/12/2018 23:50"  # ending date of the study
    WINTER_ONLY = False  # The waste can only be recovered in winter i.e. from
    # November to March
    LNCMI_CONS_CONFIG = 'fixed'  # Choose between 'free' and 'fixed'
    ILL_ON = False  # ILL in operation or not
    E_SET_LNCMI = 35430000  # Electrical power consumed by the LNCMI during a
    #  year [kWh]
    E0_STO = None
    HP_ELEC_COSTS = 0.0817  # 81,7 €/MWh
    HEAT_PUMP_NAME = ['Carrier CCIAG', 'Carrier LST', 'Johnson LST',
                      'Carrier 50deg']
    PMIN_EXPORT = [3000, 8500]
    LNCMI_POWER = [30]
    NAME = 'variousPAC'

    STORAGE_CAPA = [20]
    heat_pump_name = ['Carrier CCIAG', 'Carrier LST dT=20°C',
                      'Carrier LST dT=15°C', 'Johnson LST']
    for PM in PMIN_EXPORT:
        for P in LNCMI_POWER:
            results_dict = {}
            columns_index = ['Config']
            for HEAT_PUMP_CONFIG in [2, 3]:
                columns_index.append(HEAT_PUMP_NAME[HEAT_PUMP_CONFIG - 1])
                results_list = []
                lines_index = []
                for STO in STORAGE_CAPA:
                    for NB_HP in range(1, 5):
                        lines_index.append(str(STO) + 'MWh' + '{0}HP'
                                           .format(NB_HP))
                        main(DT, START_DATE, END_DATE, WINTER_ONLY,
                             LNCMI_CONS_CONFIG, STO,
                             HEAT_PUMP_CONFIG, ILL_ON,
                             e_lncmi=E_SET_LNCMI, p_min_for_export=PM,
                             lncmi_power=P, hp_elec_cost=HP_ELEC_COSTS,
                             nb_hp=NB_HP)

                        # --- RESULTS FILES ---
                        # Energy flows for each storage and heat pump configuration
                        file_name = \
                            'results\\return_recovery\\lncmi_return_storage_' \
                            'capa_{0}_{1}_hp_conf_{2}_lncmi_conf_{3}_{4}_' \
                            'MW_PminValo_{5}MW_ILL_OFF_{6}'.format(
                                STO, NB_HP, HEAT_PUMP_CONFIG,
                                LNCMI_CONS_CONFIG, P, PM / 1e3, NAME)
                        save_energy_flows(heat_node_bef_valve,
                                          heat_node_aft_valve,
                                          heat_node_aft_hp,
                                          elec_node, file_name=file_name,
                                          sep=';')

                        # Parametric result file
                        # Recovered heat (before heat pump)
                        results_list.append(round(sum(
                            heat_node_bef_valve._exports[
                                0].value.values()) / 1e6,
                                                  2))
                    results_dict['Config'] = lines_index
                    results_dict[
                        columns_index[1]] = results_list
                    # results_dict[
                    #     columns_index[HEAT_PUMP_CONFIG - 1]] = results_list

                df = pd.DataFrame(results_dict, columns=columns_index)
                df.to_csv(
                    'results\\return_recovery\\return_rec_{0}MW_PminValo_{1}MW_'
                    'ILL_OFF_{2}.csv'.format(P, PM / 1000, NAME),
                    header=True,
                    index=False, sep=';')

                # print_results()
