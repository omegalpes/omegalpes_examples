#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    LNCMI study case : recovering waste heat from the electrical consumption of
     the LNCMI, with a system of storage & a heat pump to increase the
     temperature and inject on the district heating network
    """

import pandas as pd
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit, HeatPump
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import ThermoclineStorage
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import TechnicalDynamicConstraint
from omegalpes.general.time import \
    TimeUnit
from omegalpes.general.utils.plots import plt, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.utils.input_data import select_csv_file_between_dates
from omegalpes_examples.various_examples.LNCMI_cases.lncmi_simplified_ecos import \
    lncmi_simplified


def main(dt: float=1, start_date='01/01/2018 00:00',
         end_date='31/12/2018 23:50',
         lncmi_cons_config: str='fixed', storage_capa: float=30,
         heat_pump_config: int=2,
         e_lncmi: float=35430000, cciag_pmax: float=2e4,
         p_thresh_reco: float=8500, lncmi_power: float=30, e_0_sto: float
         =None, hp_elec_cost: float=0.0817, winter_only: bool=False,
         dissip_constraint: bool=False, water_logic: bool=False):
    """
    :param dt: time step 1 = 1 hour, 1/6 = 10 min
    :type dt: float
    :param start_date : start date of the study horizon
    :type start_date: string date 'DD/MM/YYY HH:MM'
    :param end_date : end date of the study horizon
    :type end_date: string date 'DD/MM/YYY HH:MM'
    :param lncmi_cons_config: LNCMI consumption, can be: 'free', 'fixed' (
    i.e. power profiles for a given year) or 'rearranged' (i.e. managing the
    planning at a given time step with the OMEGAlpes model lncmi_simplified
    :type lncmi_cons_config: str
    :param storage_capa: storage capacity (MWh)
    :type storage_capa: int
    :param heat_pump_config: Should be set to 1, 2 or 3 for Carrier CCIAG,
    Carrier CEA or Johnson CEA
    :type heat_pump_config: int
    :param e_lncmi: Energy which has to be consumed by the LNCMI during the
    time range
    :type e_lncmi: int or float
    :param cciag_pmax: Maximal heating power delivered by the CCIAG to the
    district
    :type cciag_pmax: int or float
    :param p_thresh_reco: Minimal heating power required for the LNCMI to
    ensure constant temperature for the magnet heat
    :type p_thresh_reco: int or float
    :param lncmi_power: max power of the LNCMI magnets, either 24, 30 or 36
    (MW)
    :type lncmi_power: int
    :param e_0_sto: initial storage state of charge (kWh)
    :type e_0_sto: int or float
    :param hp_elec_cost: price of the electricity consumed by the heat_pump
    :type hp_elec_cost: float
    :param winter_only: if True, the heat injection on the network is only
    possible during the winter heating period
    :type winter_only: bool
    :param dissip_constraint: Indicates if there is a temperature
    constraint for the heat dissipation
    :type dissip_constraint: bool
    :param water_logic: water logic of the district heating network taken
    into accoun for the heat injection from the heat pump
    :type water_logic: bool

    """

    global time, lncmi, district_heat_load, heat_pump, heat_production, \
        heat_node_lncmi, heat_node_network, heat_node_recovery, \
        thermocline_storage, model, dissipation, elec_node, elec_prod

    # --- OPTIMIZATION MODEL CREATION ---
    # Creating the TimeUnit dedicated to time management
    time = TimeUnit(start=start_date, end=end_date, dt=dt)

    # Creating an empty model
    model = OptimisationModel(name='lncmi_model', time=time)

    # --- LOADING FILES ---
    # lncmi_cons_file : 1 hour data points --> dt=1
    # drac_temp_file : 10 min data points --> dt=1/6
    # district_consumption_file : 1 hour data points --> dt=1
    # heating_network_temp_file : 1 hour data points --> dt=1
    lncmi_cons_origin = select_csv_file_between_dates(
        "data/cons_lncmi_v7.csv", start_date, end_date)
    drac_temp = select_csv_file_between_dates(
        "data/Tdrac10min.csv", start_date, end_date)
    district_consumption = select_csv_file_between_dates(
        'data/district_heat_load_predicted.csv', start_date, end_date)
    heating_network_temp = select_csv_file_between_dates(
        "data/loi_eau_approx.csv", start_date, end_date)

    # 10min data points
    t_drac = []
    # --- ADAPTING DATA SETS TO THE TIME STEP ---
    if dt < 1:
        # ONLY WITH 10min data sets
        # for c in lncmi_cons:
        #     p_lncmi += [c] * int(1 / (6 * dt))
        for t in drac_temp:
            t_drac += [t] * int(1 / (6 * dt))
    elif dt == 1:
        k = 0
        while k < len(drac_temp):
            # calculating the mean value of the 10min points
            # p_lncmi.append(round(sum(lncmi_cons[k:k + 6]) / 6, 2))
            t_drac.append(round(sum(drac_temp[k:k + 6]) / 6, 2))
            k += 6
    else:
        raise ValueError("dt should be set between 1/6 and 1 but is set "
                         "to {0}".format(dt))
    # 1 hour data points
    p_heat_load = []
    for d in district_consumption:
        p_heat_load += [d] * int(1 / dt)

    t_heating_network = []
    for t in heating_network_temp:
        t_heating_network += [t] * int(1 / dt)

    # --- LNCMI CREATION ---
    # The elec to therm ratio represents the part of the electricity
    # consumed by the LNCMI that is dissipated as heat. This ratio actually
    # represents the part of the electricity of the LNCMI facility that is
    # consumed in the magnets, because all of the electricity in the magnets
    #  is dissipated as heat. The rest of the facility electricity
    # consumption comes from the pumps used in the cooling system as well as
    #  auxiliary electricity consumptions.

    # Ratio between the LNCMI facility electricity consumption, and the
    # magnets' consumption, i.e. the heat dissipated.
    elec_to_heat_ratio = 0.85
    lncmi_power_ratio = lncmi_power / 24  # in order to translate the
    # consumption from 24 MW to 30 or 36 MW for instance

    if lncmi_cons_config == 'fixed':
        lncmi_cons = [lncmi_power_ratio * c for c in lncmi_cons_origin]
        lncmi = ElectricalToThermalConversionUnit(time, 'lncmi',
                                                  elec_to_therm_ratio
                                                  =elec_to_heat_ratio,
                                                  p_in_elec=lncmi_cons)
    elif lncmi_cons_config == 'free':
        # Only the annual consumed energy is fixed : this is a fictitious
        # scenario, as if the LNCMI electricity consumption was only used
        # for district heating, without any constraint.
        lncmi = ElectricalToThermalConversionUnit(time, 'lncmi',
                                                  pmax_in_elec=lncmi_power * 1e3
                                                               + 2e3,
                                                  elec_to_therm_ratio=0.85)
        lncmi.elec_consumption_unit.add_energy_limits_on_time_period(
            e_min=e_lncmi)
    elif lncmi_cons_config == 'rearranged':
        # lncmi energy blocks are rearranged with the lncmi_simplified model
        rearranged_profile, simpl_model, simpl_heat_production, \
        simpl_district_heat_load, simpl_heat_pump, simpl_lncmi_energy_blocks, \
        simpl_dissipation = lncmi_simplified(dt=24 * 7, start_date=start_date,
                                             end_date=end_date,
                                             elec_to_heat_ratio=
                                                     elec_to_heat_ratio,
                                             p_thresh_reco=p_thresh_reco,
                                             p_max_recov=2520 + storage_capa
                                                         * 1e3 / 3)
        # # TEST with 8 hours replanning
        # rearranged_profile_file = open('results/p_8hours_replanif.csv')
        # rearranged_profile_raw = rearranged_profile_file.read()
        # rearranged_profile = rearranged_profile_raw.splitlines()

        lncmi_cons = [lncmi_power_ratio * float(p) for p in rearranged_profile]
        lncmi = ElectricalToThermalConversionUnit(time, 'lncmi',
                                                  elec_to_therm_ratio
                                                  =elec_to_heat_ratio,
                                                  p_in_elec=lncmi_cons)
    else:
        raise ValueError(
            "The lncmi consumption configuration should be free or fixed and "
            "is set to {0}".format(lncmi_cons_config))

    if dissip_constraint:
        max_dissip_power = dissipation_constraint(dt=dt, t_drac=t_drac,
                                                  isere_max_temp=29,
                                                  ill_on=False)
        dissipation = VariableConsumptionUnit(time, 'dissipation',
                                              p_max=max_dissip_power,
                                              energy_type='Thermal')
    else:
        dissipation = VariableConsumptionUnit(time, 'dissipation',
                                              p_max=lncmi_power * 1e3 + 2e3,
                                              energy_type='Thermal')

    # --- STORAGE CREATION ---
    # Storage configurations
    if storage_capa == 0 or None:
        pass
    else:
        capacity = storage_capa * 1e3
        # pc_max = 4e4
        pc_max = round(capacity / 3, 3)
        pd_max = pc_max

        # Creating the thermocline storage
        thermocline_storage = ThermoclineStorage(time, 'thermocline_storage',
                                                 pc_max=pc_max, pd_max=pd_max,
                                                 pc_min=0.001 * pc_max,
                                                 pd_min=0.001 * pd_max,
                                                 capacity=capacity,
                                                 e_0=e_0_sto, Tcycl=120)

        # --- DISTRICT HEAT LOAD CREATION ---
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=p_heat_load,
                                              energy_type='Thermal')

    # --- HEAT PUMP CREATION ---
    # Various heat pump models can be considered, each having its own
    # characteristics
    if heat_pump_config == 1:  # Carrier CCIAG
        cop_hp = 3.25
        t_hp = 85  # District heating injection temperature
        # losses_hp = 0.01
        pmax_elec = 1140
        pmin_elec = 0.17 * pmax_elec
        # storage_costs = [0, 0, 0, 0]  # No storage costs known for this
        # configuration
        # hp_cost = 0  # No heat pump cost known for this configuration
    elif heat_pump_config == 2:  # Carrier LST
        cop_hp = 3
        t_hp = 85  # District heating injection temperature
        # losses_hp  = 0.016
        pmax_elec = 1260
        pmin_elec = 0.17 * pmax_elec
        # storage_costs = [900, 1800, 2700, 3600]
        # hp_cost = 810362
    elif heat_pump_config == 3:  # Johnson LST
        cop_hp = 3.43
        t_hp = 90  # District heating injection temperature
        # losses_hp = 0.005
        pmax_elec = 1021
        pmin_elec = 0.2 * pmax_elec
        # storage_costs = [1350, 2700, 4050, 5400]
        # hp_cost = 780000
    elif heat_pump_config == 4:  # Carrier LST 50°C
        cop_hp = 4.29
        t_hp = 85  # District heating injection temperature
        # losses_hp  = 0.016
        # pmax_elec = 784.8
        pmax_elec = 881
        # pmin_elec = 0.17 * pmax_elec
        pmin_elec = 0.01 * pmax_elec

        # storage_costs = [900, 1800, 2700, 3600]
        # hp_cost = 810362
    else:
        raise ValueError("The heat pump configuration should be 1, 2 or 3 and "
                         "is set to {0}".format(heat_pump_config))

    # --- WATER LOGIC---
    # Depending on the network temperature, the injection of heat from the
    # heat pumps can be limited. As a matter of fact, if the network
    # temperature is higher than the heat pump output, the LNCMI heat will
    # not be able to fully heat the district network water. This partial
    # heating is considered through the temp_influence_ratio, taking into
    # account the heat_pump outlet temperature and the network two ways
    # temperatures
    if water_logic:
        t_return = 70  # supposed return temperature on the heating network
        pmax_injection=water_logic_constraint(t_return=t_return, t_hp=t_hp,
                                              t_heat_net=heating_network_temp,
                                              p_heat_load=p_heat_load)
         # Constraint for waste heat recovery in winter only can be considered
        if winter_only:
            for summer_time in range(2161 * int(1 / dt), 7296 * int(1 / dt)+1):
                # April  -->  October
                pmax_injection[summer_time] = 0
        # Creating the heat pump
        heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                             pmin_in_elec=pmin_elec,
                             pmax_in_elec=pmax_elec,
                             pmax_out_therm=pmax_injection)
    # , losses=losses_hp)
    else:
        pmax_injection = [pmax_elec*cop_hp]*time.LEN

        if winter_only:
            for summer_time in range(2161 * int(1 / dt), 7296 * int(1 / dt)+1):
                # April  -->  October
                pmax_injection[summer_time] = 0
        # Creating the heat pump
        heat_pump = HeatPump(time, 'heat_pump', cop=cop_hp,
                             pmin_in_elec=pmin_elec,
                             pmax_in_elec=pmax_elec,
                             pmax_out_therm=pmax_injection)

    # Creating the source of electricity for heat pump
    elec_prod = VariableProductionUnit(time, 'elec_prod',
                                       operating_cost=hp_elec_cost,
                                       energy_type='Electrical')

    # --- CCIAG PRODUCTION CREATION ---
    #  heat production plants
    heat_production = VariableProductionUnit(time, name='heat_production',
                                             energy_type='Thermal',
                                             p_max=cciag_pmax)  # Creating the

    # --- HEAT NODES CREATION ---
    # Creating the heat node for the energy flows
    # Heat node for the LNCMI current system
    heat_node_lncmi = EnergyNode(time, 'heat_node_lncmi',
                                 energy_type='Thermal')
    # Heat node for the Heat Pump and thermal storage recovery system
    heat_node_recovery = EnergyNode(time, 'heat_node_recovery',
                                    energy_type='Thermal')
    # Heat node for the district heating network
    heat_node_network = EnergyNode(time, 'heat_node_network',
                                   energy_type='Thermal')
    # Electricity node for the electrical consumption from the LNCMI and the
    #  heat pump
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')

    # Connecting units to the nodes
    heat_node_lncmi.connect_units(lncmi.thermal_production_unit,
                                  dissipation)
    heat_node_lncmi.export_to_node(
        heat_node_recovery)  # Export the heat to the recovery system
    if storage_capa == 0 or None:
        heat_node_recovery.connect_units(
            heat_pump.thermal_consumption_unit)
    else:
        heat_node_recovery.connect_units(
            heat_pump.thermal_consumption_unit,
            thermocline_storage)
    elec_node.connect_units(elec_prod, heat_pump.elec_consumption_unit)
    heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                    heat_production, district_heat_load)

    # --- P_THRESH RECOVERY CONSTRAINT ---
    # A constant temperature magnet outlet temperature, necessary for the
    # recovery system, can only be reached above a certain power threshold
    p_thresh_rec_cst_exp = '{0}_therm_prod_p[t] >= {1} * ' \
                           'heat_node_lncmi_is' \
                           '_exporting_to_heat_node_recovery[t]' \
        .format(lncmi.name, p_thresh_reco)
    heat_node_lncmi.export_min = \
        TechnicalDynamicConstraint(name='p_thresh_recovery',
                                   exp_t=p_thresh_rec_cst_exp,
                                   t_range='for t in time.I',
                                   parent=heat_node_lncmi)

    # --- OBJECTIVE CREATION ---
    # Objective : Minimizing the part of the heat load covered by the heat
    # production plants
    heat_production.minimize_production()

    # --- ADDING ALL UNITS TO THE OPTIMIZATION MODEL ---
    model.add_nodes(heat_node_lncmi, heat_node_recovery,
                    heat_node_network, elec_node)

    # Writing into lp file
    model.writeLP('optim_models\lncmi_study_case.lp')

    # --- SOLVING AND UPDATING ---
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update
    # values, either with CBC as a default solver, or with GUROBI


def print_results():
    """
        *** This function print the optimisation results:
                - The district heat consumption during the year
                - The LNCMI electrical consumption during the year
                - The CCIAG production during the year
                - The heat from the LNCMI injected on the district heating
                network
                - The heat pump electricity & heat consumptions
                - The percentage of the heat load covererd by the heat pump
                - The percentage of the LNCMI heat that is recovered

            And plot the power curves :
                - Figure 1 : The energy flows between the LNCMI heat
                production, the heat dissipation and the export to the waste
                heat recovery system
                - Figure 2 : The energy flows between the heat imported from
                the LNCMI, the thermocline storage (when there is one) and the
                heat pump heat consumption
                - Figure 3 : The energy flows between the heat pump heat
                production, the CCIAG heat production, and the district heat
                load

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} GWh.'.format(
            round(district_heat_load.e_tot.value / 1e6, 2)))
        print('LNMCI electrical consumption (GWh)= ',
              round(lncmi.elec_consumption_unit.e_tot.value / 1e6, 2))
        print('CCIAG production (GWh)= ', round(heat_production.e_tot.value
                                                / 1e6, 2))
        print('LNMCI heat injected on the district heating network by the '
              'Heat Pump (GWh)= ', round(
            heat_pump.thermal_production_unit.e_tot.value / 1e6, 2))
        print('Heat_pump heat consumption (GWh)= ',
              round(heat_pump.thermal_consumption_unit.e_tot.value / 1e6, 2))
        print('Heat pump electricity consumption (GWh)= ',
              round(heat_pump.elec_consumption_unit.e_tot.value / 1e6, 2))
        print("{0} % of the load coming from the heat pump".format(
            round(heat_pump.thermal_production_unit.e_tot.value /
                  district_heat_load.e_tot.value * 100)))
        # value is a dict, with time as a key, and power levels as values.

        print("{0} % of the LNCMI heat is recovered".format(
            round(sum(
                heat_node_lncmi.energy_export_to_heat_node_recovery
                    .value.values()) * DT /
                  lncmi.thermal_production_unit.e_tot.value * 100)))
        # value is a results_dict, with time as a key, and power levels as
        # values.

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


def plot_results():
    plot_node_energetic_flows(heat_node_lncmi)
    plot_node_energetic_flows(heat_node_recovery)
    plot_node_energetic_flows(heat_node_network)
    plt.show()


def dissipation_constraint(dt: float, t_drac: list,
                           isere_max_temp: float = 29.,
                           ill_on: bool = False):
    """
    :param dt: time step of the study
    :param t_drac: Drac river temperature
    :param isere_max_temp: maximal temperature of the reject to the Isère
    river (°C)
    :param ill_on: is the ILL institute working, constraining even more
    the temperature constraint of the heat dissipation
    :return: max_dissip_power : maximal power value of the heat dissipation
    """
    # --- HEAT DISSIPATION CREATION ---
    RHO = 998.30  # Water density at 20 °C [kg·m-3]
    cp = 4185.5  # Water specific heat at 15°C [J·kg-1·K-1]
    cp /= 1000  # Water specific heat at 15°C [kW.s·kg-1·K-1]
    max_flow_ill_on = 0.333  # Maximum water flow [m3.s-1] when the ILL is in
    #  operation (about 4 months a year)
    max_flow_ill_off = 0.5  # Maximum water flow [m3.s-1] when the ILL is not
    #  in operation.
    max_flow = max_flow_ill_on if ill_on else max_flow_ill_off

    max_dissip_power = [RHO * cp * max_flow * (isere_max_temp - temp) for
                        temp in t_drac]

    new_max_dissip_power = []
    for c in max_dissip_power:
        new_max_dissip_power += [c] * int(1 / dt)
    max_dissip_power = [round(elem, 2) for elem in new_max_dissip_power]

    return max_dissip_power


def water_logic_constraint(t_return: float, t_hp: float, t_heat_net: list,
                           p_heat_load: list):
    """

    :param t_return: return temperature of the district heating network
    :param t_hp: heat pump outlet temperature
    :param t_heat_net: considered heating network temperature
    :param p_heat_load: heat load power profile
    :return: pmax_injection
    """
    temp_influence_ratio = []
    for t_net in t_heat_net:
        temp_influence_ratio += [(t_hp - t_return) / (t_net - t_return)]

    pmax_injection = [round(h_l * t_i_r, 2) for h_l, t_i_r
                      in zip(p_heat_load, temp_influence_ratio)]
    return pmax_injection


if __name__ == "__main__":
    # --- PARAMETERS ---
    DT = 1  # Delta t (=1 for one hour ; 1/6 for 10min)
    START_DATE = "01/01/2018 00:00"  # starting date of the study, format
    # DD/MM/YYYY HH:MM
    END_DATE = "31/12/2018 23:50"  # ending date of the study
    WINTER_ONLY = False  # The waste can only be recovered in winter i.e. from
    # November to March
    LNCMI_CONS_CONFIG = ['free']  # Choose between 'free',
                         # 'fixed' and rearranged
    DISSIP_CS = False  # Dissipation constraint considered or not
    WATER_LOGIC = False
    E_SET_LNCMI = 13.77*1e6  # Electrical power consumed by the LNCMI during a
    #  year [kWh]
    CCIAG_PMAX = 2e4
    E0_STO = None
    HP_ELEC_COSTS = 0.0817  # 81,7 €/MWh
    HEAT_PUMP_NAME = ['Carrier CCIAG', 'Carrier LST', 'Johnson LST',
                      'Carrier 50deg']
    HP_CONFIG = 2
    PMIN_EXPORT = [8500]
    LNCMI_POWER = [24]
    STORAGE_CAPA = [10, 20, 30, 40]
    NAME = 'free_utopian'

    # --- PARAMETRIC RUN ---
    for PM in PMIN_EXPORT:
        for P in LNCMI_POWER:
            results_dict = {}
            columns_index = ['Config']
            for LNCMI_CONS_CONF in LNCMI_CONS_CONFIG:
                columns_index.append(LNCMI_CONS_CONF)
                results_list = []
                lines_index = []
                for STO_CAPA in STORAGE_CAPA:
                    lines_index.append(str(STO_CAPA) + 'MWh')
                    main(DT, START_DATE, END_DATE, LNCMI_CONS_CONF,
                         STO_CAPA, HP_CONFIG, cciag_pmax=CCIAG_PMAX,
                         e_lncmi=E_SET_LNCMI, p_thresh_reco=PM,
                         lncmi_power=P, hp_elec_cost=HP_ELEC_COSTS,
                         winter_only=WINTER_ONLY,
                         dissip_constraint=DISSIP_CS, water_logic=WATER_LOGIC)

                    # Short results
                    try:
                        print(
                            "{0} % of the LNCMI heat is recovered".format(
                                round(sum(
                                    heat_node_lncmi.energy_export_to_heat_node_recovery
                                        .value.values()) /
                                      lncmi.thermal_production_unit.e_tot.value *
                                      100)))

                        print(
                            "{0} GWh of the LNCMI heat is recovered".format(
                                round(sum(
                                    heat_node_lncmi.energy_export_to_heat_node_recovery
                                        .value.values()) / 1000) / 1000))
                    except:
                        pass

                    # --- RESULTS FILES ---
                    # Energy flows for each storage and heat pump configuration
                    file_name = \
                        'results\lncmi_flows_storage_capa_{0}_hp_' \
                        'conf_{1}_lncmi_conf_{2}_{3}_MW_PminValo_{4}MW_' \
                        '{5}' \
                            .format(STO_CAPA, HP_CONFIG,
                                    LNCMI_CONS_CONF, P, PM / 1e3, NAME)

                    save_energy_flows(heat_node_lncmi,
                                      heat_node_recovery, heat_node_network,
                                      elec_node, file_name=file_name, sep=';')
                    # Parametric result file
                    # Recovered heat (before heat pump)
                    results_list.append(round(sum(
                        heat_node_lncmi._exports[0].value.values()) / 1e6,
                                              2))
                    print_results()
                    # plot_results()

                results_dict['Config'] = lines_index
                results_dict[
                    columns_index[1]] = results_list
                # results_dict[
                #     columns_index[HEAT_PUMP_CONFIG - 1]] = results_list

            df = pd.DataFrame(results_dict, columns=columns_index)
            df.to_csv(
                'results\study_case_{0}MW_PminValo_{1}MW_'
                '{2}.txt'.format(P, PM / 1000, NAME),
                header=True,
                index=False, sep=';')
