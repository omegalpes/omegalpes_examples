#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    Simplified OMEGAlpes model for the LNCMI (French National Laboratory of
    High Magnetic Fields) for the energy planning of the laboratory
    depending on the potential for waste heat recovery. The aim is to rearrange
    the LNCMI power profile in new plannings for a higher correlation between
    the waste heat and the district heating consumption The time step is of
    7 days, i.e. the duration on which the laboratory staff knows the energy
    consumption in advance for a whole semester.
"""

import pandas as pd
import matplotlib as plt
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.conversion_units import HeatPump
from omegalpes.energy.units.production_units import ShiftableProductionUnit, \
    VariableProductionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.optimisation.elements import \
    TechnicalDynamicConstraint, Objective, Quantity
from omegalpes.general.time import \
    TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plt, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.utils.input_data import \
    select_csv_file_between_dates


def lncmi_simplified(dt=24 * 7, start_date='01/01/2018 00:00',
                     end_date='31/12/2018 23:50',
                     elec_to_heat_ratio=0.85, p_thresh_reco=8.5 * 1e3,
                     p_max_recov=(6.6 + 2.5) * 1e3):
    """
    :param dt: time step 1 ==> 1 hour, 24 ==> 1 day
    :type dt: float
    :param start_date : start date of the study horizon
    :type start_date: string date 'DD/MM/YYY HH:MM'
    :param end_date : end date of the study horizon
    :type end_date: string date 'DD/MM/YYY HH:MM'
    :param elec_to_heat_ratio: ratio between the LNCMI facility electrical
    consumption and its heat rejections
    :type elec_to_heat_ratio: float
    :param p_thresh_reco: power value above which a constant waste heat
    temperature can be ensured, and the recovery can be successful
    :type p_thresh_reco: float
    :param p_max_recov: maximal power value of the waste heat recovery
    system (either Heat Pump alone or Heat Pump + storage)
    :type p_max_recov: float
    """

    global time, lncmi, district_heat_load, heat_pump, heat_production, \
        heat_node_lncmi, heat_node_network, model, dissipation, elec_node, \
        elec_prod, e_values_lncmi, e_lncmi_rearranged, p_lncmi_rearranged

    # --- OPTIMIZATION MODEL CREATION ---
    # Creating the TimeUnit dedicated to time management
    time = TimeUnit(start=start_date, end=end_date, dt=dt)

    # Creating an empty model
    model = OptimisationModel(name='lncmi_simple_model', time=time)

    # --- LOADING FILES ---
    # lncmi_cons_file :  1 hour data points --> dt=1
    # drac_temp_file : 1 hour data points --> dt=1
    # district_consumption_file : 1 hour data points --> dt=1
    # heating_network_temp_file : 1 hour data points --> dt=1
    lncmi_cons_raw = select_csv_file_between_dates(
        "data/cons_lncmi_v7.csv", start_date, end_date)
    # NB : this data set is available in open data
    district_consumption = select_csv_file_between_dates(
        'data/district_heat_load_predicted.csv', start_date, end_date)
    # NB : these data are confidential and currently not shard in open data
    # heating_network_temp = select_csv_file_between_dates(
    #     "data/loi_eau_approx.csv", start_date, end_date)

    lncmi_cons = [round(p, 3) for p in lncmi_cons_raw.values]

    # Getting energy profiles for the LNCMI recoverable heat production and
    # the district heating 1 hour power profiles

    dict_lncmi = {}
    dict_lncmi = {}
    e_values_dh = []
    e_values_lncmi = []

    e_values_lncmi, e_values_dh, dict_lncmi = \
        power_to_energy_profile_lncmi(lncmi_profile_raw=lncmi_cons,
                                      dh_profile_raw=district_consumption,
                                      time_step=dt,
                                      elec_to_heat_ratio=elec_to_heat_ratio,
                                      p_thresh_reco=p_thresh_reco,
                                      p_max_recov=p_max_recov)
    # Equivalent maximum power that is here a maximum energy flow at the 7
    # days time step.
    pmax_eq = (max(max(e_values_dh), max(e_values_lncmi)) + 1e3)/time.DT
    print("pmax_eq = {}".format(pmax_eq))

    # --- LNCMI CREATION ---
    # Here each energy block of the LNCMI is modelled as a single Variable
    # Production Unit
    lncmi_energy_blocks = rearrange_profile(time, e_values_lncmi)

    # Constraint expression for runs not to occur at the same time
    u_exp = ''
    for e_block in lncmi_energy_blocks:
        u_exp += '{0}_u[t] + '.format(e_block.name)
    u_exp += '0 <= 1'

    dissipation = VariableConsumptionUnit(time, 'dissipation', p_max=pmax_eq,
                                          energy_type='Thermal')

    # --- DISTRICT HEAT LOAD CREATION ---
    p_dh = [e/time.DT for e in e_values_dh]
    district_heat_load = FixedConsumptionUnit(time, 'district_heat_load',
                                              p=p_dh,
                                              energy_type='Thermal')

    # --- HEAT PUMP CREATION ---
    # The heat pump is simplified due to the energy block consideration,
    # pmax_eq_hp is also a maximum energy flow at the 7 days time step,
    # with 3.5MW the maximal outlet power value of the heat pump

    pmax_hp = 3.5e3
    print("pmax_eq_hp = {}".format(pmax_hp))

    heat_pump = HeatPump(time, "heat_pump", cop=3,
                         pmax_out_therm=pmax_hp, pmax_in_elec=pmax_hp,
                         pmax_in_therm=pmax_hp)

    # Creating the source of electricity for heat pump
    elec_prod = VariableProductionUnit(time, 'elec_prod',
                                       energy_type='Electrical', p_max=pmax_eq)

    # --- DISTRICT HEATING PRODUCTION CREATION ---
    #  heat production plants
    heat_production = VariableProductionUnit(time, name='heat_production',
                                             energy_type='Thermal',
                                             p_max=pmax_eq)

    # --- HEAT NODES CREATION ---
    # Creating the heat node for the energy flows
    # Heat node for the LNCMI current system
    heat_node_lncmi = EnergyNode(time, 'heat_node_lncmi',
                                 energy_type='Thermal')
    # Heat node for the district heating network
    heat_node_network = EnergyNode(time, 'heat_node_network',
                                   energy_type='Thermal')
    # Electricity node for the electrical consumption from the LNCMI and the
    #  heat pump
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')

    heat_node_lncmi.non_sup_exp = TechnicalDynamicConstraint(u_exp,
                                                             name=
                                                             'non_sup_exp',
                                                             parent=
                                                             heat_node_lncmi)

    # Connecting all units on the same energy node
    heat_node_lncmi.connect_units(dissipation,
                                  heat_pump.thermal_consumption_unit,
                                  *lncmi_energy_blocks)
    # lncmi_single_block, lncmi_second_block)

    # Connecting units to the nodes
    elec_node.connect_units(elec_prod, heat_pump.elec_consumption_unit)
    heat_node_network.connect_units(heat_pump.thermal_production_unit,
                                    heat_production, district_heat_load)

    # --- OBJECTIVE CREATION ---
    # Objective 1 : Minimizing the part of the heat load covered by the heat
    # production plants
    heat_production.minimize_production()

    # Objective 2 : Minimising the difference between the district heat load
    # energy and the waste heat production from the heat pump at a given time
    # step, weighted with the district heat load energy value; for the energy
    # planning of the LNCMI to fit the district heating energy profiles.
    # This objective ensures to sort energy consumption and production
    # relatively at the same levels for the whole studied period.
    exp_obj = 'lpSum((district_heat_load_p[t]-heat_pump_therm_prod_p[t])' \
              '*district_heat_load_p[t] for t in time.I)'

    fitting_obj = Objective(name='fitting', exp=exp_obj,
                            parent=heat_production)

    setattr(heat_production, 'fitting', fitting_obj)

    # --- ADDING ALL UNITS TO THE OPTIMIZATION MODEL ---
    model.add_nodes(heat_node_lncmi, heat_node_network, elec_node)

    # Writing into lp file
    # model.writeLP('optim_models\lncmi_simple_case.lp')

    # --- SOLVING AND UPDATING ---
    model.solve_and_update(GUROBI_CMD())  # Run optimization and update
    # values, either with CBC as a default solver, or with GUROBI

    # Extracting the rearranged LNCMI energy profile
    e_lncmi_rearranged = []
    for p_d, p_hp in zip(dissipation.p.get_value(),
                         heat_pump.thermal_consumption_unit.p.get_value()):
        e_lncmi_rearranged.append(round(p_d*time.DT + p_hp*time.DT, 3))

    # Rebuilding the rearranged LNCMI power profile

    p_lncmi_rearranged = rebuild_profile(e_lncmi_rearranged, dict_lncmi)

    return p_lncmi_rearranged, model, heat_production, district_heat_load, \
           heat_pump, lncmi_energy_blocks, dissipation


def power_to_energy_profile_lncmi(lncmi_profile_raw: list,
                                  dh_profile_raw: list,
                                  time_step: float,
                                  elec_to_heat_ratio: float,
                                  p_thresh_reco: float, p_max_recov: float):
    """
    Intermediate tool for switching between power profiles and energy planning
    :param lncmi_profile_raw:
    :param dh_profile_raw:
    :param time_step:
    :param elec_to_heat_ratio:
    :param p_thresh_reco:
    :param p_max_recov:
    :return:
    - lncmi_recoverable_heat,
    - dh_profile_rescaled,
    - dict_lncmi
    """
    dict_lncmi = {}
    lncmi_recoverable_heat = []  # depending on the threshold recovery power
    # value
    dh_profile_rescaled = []  # Adapt target profile to time step

    start = 0
    stop = 0
    marker = 0

    while stop < len(lncmi_profile_raw):
        # a marker is put in place in order to differentiate power profiles
        # extract with same energy values, especially for the last days of
        # the year.
        #TODO: take the markers into account in the reconstruction
        marker += 0.001
        stop += time_step
        run_lncmi_profile = lncmi_profile_raw[start:stop]
        run_dh_profile = dh_profile_raw[start:stop]
        dh_profile_rescaled.append(sum(run_dh_profile))
        run_waste_profile = [elec_to_heat_ratio * p for p in run_lncmi_profile]
        run_recoverable_profile = []

        for w in run_waste_profile:
            if w < p_thresh_reco:
                recoverable = 0
            elif w > p_max_recov:
                recoverable = p_max_recov
            else:
                recoverable = w
            run_recoverable_profile.append(recoverable)

        run_recoverable_energy = round(sum(run_recoverable_profile) + marker,
                                       3)

        dict_lncmi[run_recoverable_energy] = run_lncmi_profile
        lncmi_recoverable_heat.append(run_recoverable_energy)

        start += time_step
    return lncmi_recoverable_heat, dh_profile_rescaled, dict_lncmi


def rearrange_profile(time, profile: list):
    rearranged_profile = []
    index = 1
    for value in profile:
        name = "block_nb_{}".format(index)
        cmd = name + " = VariableProductionUnit(time, '" + name + \
              "', p_min=value/time.DT, p_max=value/time.DT, " \
              "e_min=value, e_max=value)"
        try:
            exec(cmd)
            add_to_rearranged_profile = "rearranged_profile.append" \
                                        "(" + name + ")"
            exec(add_to_rearranged_profile)
        except ValueError:
            print(name + " wasn't added to the model.")
        index += 1
    return rearranged_profile


def rebuild_profile(energy_profile: list, dictionnary: dict):
    lncmi_profile_opt = []
    for ener_tot in energy_profile:
        one_step = dictionnary[ener_tot]
        for p in one_step:
            lncmi_profile_opt.append(p)
    return lncmi_profile_opt


def print_results():
    """
        *** This function print the optimisation results:
                - The district heat consumption during the year
                - The LNCMI electrical consumption during the year
                - The district heating network production during the year
                - The heat from the LNCMI injected on the district heating
                network
                - The heat pump electricity & heat consumptions
                - The percentage of the heat load covererd by the heat pump
                - The percentage of the LNCMI heat that is recovered

            And plot the power curves :
                - Figure 1 : printing both the lncmi energy blocks planning
                as it originally is as well as the new planning of the blocks
                - Figure 2 : The energy flows between the LNCMI heat
                production and the heat pump
                - Figure 3 : The energy flows between the heat pump heat
                production, the district heating heat production, and the
                district heat load
                - Figure 4 : plottingthe LNCMI rearranged power profile

    """

    # Print results
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('District consumption = {0} GWh.'.format(
            round(district_heat_load.e_tot.value / 1e6, 2)))
        # print('LNMCI electrical consumption (GWh)= ',
        #       round(lncmi.elec_consumption_unit.e_tot.value / 1e6, 2))
        print('District Heating production (GWh)= ', round(
            heat_production.e_tot.value/ 1e6, 2))
        print('LNMCI heat transferable on the district heating network by the '
              'Heat Pump (GWh)= ', round(
            heat_pump.thermal_production_unit.e_tot.value / 1e6, 2))
        print('Heat_pump heat consumption (GWh)= ',
              round(heat_pump.thermal_consumption_unit.e_tot.value / 1e6, 2))
        print('Heat pump electricity consumption (GWh)= ',
              round(heat_pump.elec_consumption_unit.e_tot.value / 1e6, 2))

        # PLOTS
        fig, ax = plt.subplots()
        width = 0.35
        raw_lncmi = ax.bar(time.I - width / 2, e_values_lncmi, width,
                           label='raw_LNCMI_e_profile')
        rearr_lncmi = ax.bar(time.I + width / 2, e_lncmi_rearranged,
                             width, label='rearranged_LNCMI_e_profile')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Energy levels (kWh)')
        ax.set_xlabel('Weeks of the year')
        ax.set_title('LNCMI raw and rearranged energy profiles')
        ax.set_xticks(time.I)
        ax.legend()
        plot_node_energetic_flows(heat_node_lncmi)
        plot_node_energetic_flows(heat_node_network)
        plt.figure()
        plt.plot(p_lncmi_rearranged)
        plt.title(label='LNCMI rearranged power profile')
        plt.xlabel(xlabel='Time (h)')
        plt.ylabel(ylabel='Power (kW)')
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    # --- PARAMETERS ---
    DT = 7*24  # Delta t (=1 for one hour ; 1/6 for 10min ; 7*24 for a week)
    START_DATE = "01/01/2018 00:00"  # starting date of the study, format
    # DD/MM/YYYY HH:MM
    END_DATE = "31/12/2018 23:50"  # ending date of the study
    NAME = 'simplified'

    # --- RUN ---
    P_REA, MODEL, HEAT_PRODUCTION, DISTRICT_HEAT_LOAD, HEAT_PUMP, \
    LNCMI_ENERGY_BLOCKS, DISSIPATION = lncmi_simplified(DT, START_DATE,
                                                        END_DATE)
    # Saving rearranged profile as .csv file
    df = pd.DataFrame(P_REA)
    df.to_csv('results\p_rearranged_lncmi_simplified_TS_{}_hours'.format(DT),
              header=True, index=False, sep=';')

    # print_results()
