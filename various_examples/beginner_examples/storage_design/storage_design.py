#! usr/bin/env python3
#  coding=utf-8 #

"""
    ** This Module is an example of storage capacity optimisation.**

        This example describes a simple microgrid with :
            - A load profile known in advance
            - An adjustable production unit, with a maximum power limit
            - A storage system with power in charge and power in discharge

        The objective consists on minimizing the storage capacity.

..

    Copyright 2018 G2ELab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pulp import LpStatus
import os

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"

# Install corresponding omegalpes version
os.system('pip install omegalpes==' + __version__)

# -----------------------------------------------------------------------------

from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_quantity_bar, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.time import TimeUnit

# -----------------------------------------------------------------------------

def storage_design(work_path, load_profile, production_pmax,
                   storage_pcharge_max, storage_pdischarge_max, write_lp):
    """
    :param work_path: Path to the working directoryin order to write .lp file
    :type work_path: string
    :param production_pmax: Maximal power delivered by the production unit [kW]
    :type production_pmax: float or int
    :param storage_pcharge_max: Maximal charging power for the storage [kW]
    :type storage_pcharge_max: float or int
    :param storage_pdischarge_max: Maximal discharging power for the storage
    [kW]
    :type storage_pdischarge_max: float or int
    :param load_profile: hourly load profile during a day
    :type load_profile: list of 24 float or int
    :param write_lp: parameter indicating to write a .lp file (True) or not (
    False)
    :type write_lp: boolean

    """

    # Create an empty model
    time = TimeUnit(periods=24, dt=1)  # Study on a day (24h), delta_t = 1h
    model = OptimisationModel(time=time, name='example')  # Optimisation model

    # Create the load - The load profile is known
    load = FixedConsumptionUnit(time, 'load', p=load_profile)

    # Create the production unit - The production profile is unknown
    production = VariableProductionUnit(time, 'production',
                                        p_max=production_pmax)

    # Create the storage
    storage = StorageUnit(time, name='storage', pc_max=storage_pcharge_max,
                          pd_max=storage_pdischarge_max, soc_min=0.1,
                          soc_max=0.9, self_disch=0.01, ef_is_e0=True)
    storage.minimize_capacity()  # Minimize the storage capacity

    # Create the energy node and connect units
    node = EnergyNode(time, 'energy_node')

    # Add the energy node to the model
    node.connect_units(load, production,
                       storage)  # Connect all units on the same energy node
    model.add_nodes(node)  # Add node to the model

    # Optimisation process
    if write_lp:
        model.writeLP(work_path + r'\optim_models\simple_storage_design.lp')
    model.solve_and_update()

    return model, time, load, production, storage, node


def print_results_storage(model, time, load, production, storage, node):
    """
        *** This function print the optimisation result:
                - The optimal capacity

            And plot the power curves :
            Figure 1 :
                - Power consumed by the storage, labelled 'Storage'
                - Power consumed by the load, labelled 'Load'
                - Power delivered by the production unit, labelled 'Production'
            Figure 2 :
                - The state of charge of the storage unit
    """

    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        print("The optimal storage capacity is {0} kWh".format(
            storage.capacity))

        # Show the graph
        # Power curves
        plot_node_energetic_flows(node)
        # SOC curve
        plot_quantity_bar(time=time, quantity=storage.e, title='State of '
                                                               'charge of '
                                                               'the storage '
                                                               '(kWh)')
        plt.xlabel('Time (h)')
        plt.ylabel('Energy (kWh)')
        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == '__main__':
    # OPTIMIZATION PARAMETERS #
    WORK_PATH = os.getcwd()

    # Load dynamic profile - one value per hour during a day
    LOAD_PROFILE = [4, 5, 6, 2, 3, 4, 7, 8, 13, 24, 18, 16, 17, 12, 20, 15, 17,
                    21, 25, 23, 18, 16, 13, 4]

    # Maximal power that can be delivered by the production unit
    PRODUCTION_P_MAX = 15

    # Storage maximal charging and discharging powers
    (STORAGE_PC_MAX, STORAGE_PD_MAX) = 20, 20

    # Run main
    MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE = \
        storage_design(work_path=WORK_PATH, load_profile=LOAD_PROFILE,
                       production_pmax=PRODUCTION_P_MAX,
                       storage_pcharge_max=STORAGE_PC_MAX,
                       storage_pdischarge_max=STORAGE_PD_MAX, write_lp=True)

    # Save energy flows into a CSV file
    save_energy_flows(NODE, file_name=WORK_PATH + r'\results\storage_design')

    # Show results
    print_results_storage(MODEL, TIME, LOAD, PRODUCTION, STORAGE, NODE)
