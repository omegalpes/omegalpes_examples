#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.energy_nodes import *
from omegalpes.general.time import *
from omegalpes.general.optimisation.model import *
from omegalpes.general.utils.plots import *
from omegalpes.general.utils.output_data import save_energy_flows
from pulp import LpStatus


def main(op_cost_a=None,
         op_cost_b=None,
         consumption_file=None):
    time = TimeUnit(periods=24, dt=1)

    building_cons_file = open(consumption_file, "r")
    electrical_load = [c for c in map(float, building_cons_file)]

    dwelling_consumption = FixedConsumptionUnit(time, name='dwelling_consumption', energy_type='Electrical', p = electrical_load)
    grid_production_a_ = VariableProductionUnit(
        time, name='grid_production_a_', energy_type='Electrical', operating_cost= op_cost_a)
    grid_production_b = VariableProductionUnit(
        time, name='grid_production_b', energy_type='Electrical', operating_cost=op_cost_b)

    elec_node = EnergyNode(time, name='elec_node', energy_type='Electrical')

    elec_node.connect_units(
        grid_production_b, grid_production_a_, dwelling_consumption)

    grid_production_a_.minimize_operating_cost()
    grid_production_b.minimize_operating_cost()

    model = OptimisationModel(time, name='optimization_model')
    model.add_nodes(elec_node)
    model.solve_and_update()

    return model, time, dwelling_consumption, grid_production_a_, grid_production_b, elec_node


def print_results(MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A,
                       GRID_PRODUCTION_B, ELEC_NODE):
    if LpStatus[MODEL.status] == 'Optimal':
        print('dwelling_consumption = {0} kWh.'.format(
            DWELLING_CONSUMPTION.e_tot))
        print('grid_production_a_ = {0} kWh.'.format(GRID_PRODUCTION_A.e_tot))
        print('grid_production_b = {0} kWh.'.format(GRID_PRODUCTION_B.e_tot))
        plot_node_energetic_flows(ELEC_NODE)
        plt.show()
    elif LpStatus[MODEL.status] == 'Infeasible':
        print('Sorry, the optimisation problem has no feasible solution !')
    elif LpStatus[MODEL.status] == 'Unbounded':
        print('The cost function of the optimisation problem is unbounded !')
    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exists). PuLP does not manage to interpret the solver's output,the infeasibility of the MILP problem may have beendetected during presolve")


if __name__ == '__main__':
    WORK_PATH = os.getcwd()

    # File with hourly data for the consumption profile during the day
    CONSUMPTION_PROFILE = WORK_PATH + "/data/building_consumption_day.txt"

    # Hourly operating costs for the first production unit
    OPERATING_COSTS_A = [41.1, 41.295, 43.125, 51.96, 58.275, 62.955, 58.08,
                         57.705, 59.94, 52.8, 53.865, 46.545, 41.4, 39,
                         36.87, 36.6, 39.15, 43.71, 45.195, 47.04, 44.28,
                         39.975, 34.815, 28.38]

    # Hourly operating costs for the second production unit
    OPERATING_COSTS_B = [58.82, 58.23, 51.95, 47.27, 45.49, 44.5, 44.5,
                         44.72, 44.22, 42.06, 45.7, 47.91, 49.57, 48.69,
                         46.91, 46.51, 46.52, 51.59, 59.07, 62.1, 56.26, 55,
                         56.02, 52]

    # *** RUN MAIN ***
    MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A, GRID_PRODUCTION_B, ELEC_NODE = main(op_cost_a=OPERATING_COSTS_A,
                                     op_cost_b=OPERATING_COSTS_B,
                                     consumption_file=CONSUMPTION_PROFILE)

    # *** SHOW RESULTS ***
    print_results(MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A,
                       GRID_PRODUCTION_B, ELEC_NODE)