#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""
    ** This Module is an example of decision support for electricity
    grid_productions **

    The electrical system operator needs to decide whether to provide
    electricity from the grid_production A or B depending on their operating
    costs.

    The module includes :
        - A dwelling load with a fixed consumption profile
        - Two electricity grid_productions with variable operating costs

     The objective consists in minimizing the electricity grid_productions
     operating costs

..

    Copyright 2018 G2ELab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

import os
from pulp import LpStatus
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.production_units import VariableProductionUnit
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.time import TimeUnit
from omegalpes.general.utils.plots import plot_quantity, plt

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------


def elec_syst_op(work_path, op_cost_a, op_cost_b, consumption_file, write_lp):
    """
    :param work_path: work path in order to write .lp file
    :type work_path: string
    :param op_cost_a: operating costs for the energy unit A [€]
    :type op_cost_a: list of 24 float or int
    :param op_cost_b: operating costs for the energy unit B [€]
    :type op_cost_b: list of 24 float or int
    :param consumption_file: link to the data file
    :type consumption_file: string
    :param write_lp: parameter indicating to write a .lp file (True) or not (
    False)
    :type write_lp: boolean

    """
    # Creating the unit dedicated to time management
    time = TimeUnit(periods=24, dt=1)

    # Creating an empty model
    model = OptimisationModel(time=time, name='elec_prod_simple_example')

    # Importing time-dependent data
    building_cons_file = open(consumption_file, "r")

    # Creating the dwelling consumption - the load profile is known
    electrical_load = [c for c in map(float, building_cons_file)]
    dwelling_consumption = FixedConsumptionUnit(time, 'dwelling_consumption',
                                                energy_type='Electrical',
                                                p=electrical_load)

    # Creating the production units - the production profile are unknown
    grid_production_a = VariableProductionUnit(time=time,
                                               name='grid_production_A',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_a)

    grid_production_b = VariableProductionUnit(time=time,
                                               name='grid_production_B',
                                               energy_type='Electrical',
                                               operating_cost=op_cost_b)

    # Objective : Minimizing the part of the electrical load covered by the
    # electrical production plants
    grid_production_a.minimize_operating_cost()
    grid_production_b.minimize_operating_cost()

    # Creating the energy nodes and connecting units
    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')
    elec_node.connect_units(dwelling_consumption, grid_production_a,
                            grid_production_b)

    # Adding the energy node to the model
    model.add_nodes(elec_node)

    # Optimisation process
    if write_lp:
        model.writeLP(work_path + r'\optim_models\elec_prod_simple_example.lp')
    model.solve_and_update()  # Run optimization and update values

    return model, time, dwelling_consumption, grid_production_a, \
           grid_production_b


def print_results_elec(model, time, dwelling_consumption, grid_production_a,
                       grid_production_b):
    """
        *** This function prints the optimisation result:
                - The dwelling consumption
                - The grid_production A
                - The grid_production B

            Then plots the power curves :
                - Consumption form the dwelling load, labelled 'dwelling
                consumption'
                - grid_productions A and B, labelled 'grid_production A' and
                'grid_production B'
    """
    if LpStatus[model.status] == 'Optimal':
        print("\n - - - - - OPTIMIZATION RESULTS - - - - - ")
        print('Dwelling consumption = {0} kWh.'.format(
            dwelling_consumption.e_tot.get_value()))
        print('grid_production A production = {0} kWh'.format(
            grid_production_a.e_tot.get_value()))
        print('grid_production B production = {0} kWh'.format(
            grid_production_b.e_tot.get_value()))

        # Plot the figures
        fig1 = plt.figure(1)
        ax1 = plt.axes()
        legend1 = []

        plot_quantity(time, dwelling_consumption.p, fig1, ax1)
        legend1 += ['Dwelling consumption']

        plt.legend(legend1)

        fig2 = plt.figure(2)
        ax2 = plt.axes()
        legend2 = []

        plot_quantity(time, grid_production_a.p, fig2, ax2)
        legend2 += ['grid production A']

        plot_quantity(time, grid_production_b.p, fig2, ax2)
        legend2 += ['grid production B']

        plt.legend(legend2)

        plt.show()

    elif LpStatus[model.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[model.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[model.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve")

    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == "__main__":
    WORK_PATH = os.getcwd()

    # *** OPTIMIZATION PARAMETERS ***
    # File with hourly data for the consumption profile during the day
    CONSUMPTION_PROFILE = "data/building_consumption_day.txt"

    # Hourly operating costs for the first production unit
    OPERATING_COSTS_A = [41.1, 41.295, 43.125, 51.96, 58.275, 62.955, 58.08,
                         57.705, 59.94, 52.8, 53.865, 46.545, 41.4, 39,
                         36.87, 36.6, 39.15, 43.71, 45.195, 47.04, 44.28,
                         39.975, 34.815, 28.38]

    # Hourly operating costs for the second production unit
    OPERATING_COSTS_B = [58.82, 58.23, 51.95, 47.27, 45.49, 44.5, 44.5,
                         44.72, 44.22, 42.06, 45.7, 47.91, 49.57, 48.69,
                         46.91, 46.51, 46.52, 51.59, 59.07, 62.1, 56.26, 55,
                         56.02, 52]

    # *** RUN MAIN ***
    MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A, \
    GRID_PRODUCTION_B = elec_syst_op(work_path=WORK_PATH,
                                     op_cost_a=OPERATING_COSTS_A,
                                     op_cost_b=OPERATING_COSTS_B,
                                     consumption_file=CONSUMPTION_PROFILE,
                                     write_lp=True)

    # *** SHOW RESULTS ***
    print_results_elec(MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A,
                       GRID_PRODUCTION_B)
