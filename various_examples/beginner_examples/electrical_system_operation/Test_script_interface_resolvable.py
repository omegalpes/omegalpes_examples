#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.energy_nodes import *
from omegalpes.general.time import *
from omegalpes.general.optimisation.model import *
from omegalpes.general.utils.plots import *
from omegalpes.general.utils.output_data import save_energy_flows
from pulp import LpStatus


def main():
    time = TimeUnit(periods=24, dt=1)

    dwelling_consumption = FixedConsumptionUnit(
        time, name='dwelling_consumption', energy_type='Electrical')
    grid_production_a_ = VariableProductionUnit(
        time, name='grid_production_a_', energy_type='Electrical')
    grid_production_b = VariableProductionUnit(
        time, name='grid_production_b', energy_type='Electrical')

    elec_node = EnergyNode(time, name='elec_node', energy_type='Electrical')

    elec_node.connect_units(
        grid_production_b, grid_production_a_, dwelling_consumption)

    grid_production_a_.minimize_operating_cost()
    grid_production_b.minimize_operating_cost()

    model = OptimisationModel(time, name='optimization_model')
    model.add_nodes(elec_node)
    model.solve_and_update()

    return model, time, dwelling_consumption, grid_production_a_, grid_production_b, elec_node


def print_results():
    if LpStatus[MODEL.status] == 'Optimal':
        print('dwelling_consumption = {0} kWh.'.format(
            DWELLING_CONSUMPTION.e_tot))
        print('grid_production_a_ = {0} kWh.'.format(GRID_PRODUCTION_A_.e_tot))
        print('grid_production_b = {0} kWh.'.format(GRID_PRODUCTION_B.e_tot))
        plot_node_energetic_flows(ELEC_NODE)
        plt.show()
    elif LpStatus[MODEL.status] == 'Infeasible':
        print('Sorry, the optimisation problem has no feasible solution !')
    elif LpStatus[MODEL.status] == 'Unbounded':
        print('The cost function of the optimisation problem is unbounded !')
    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exists). PuLP does not manage to interpret the solver's output,the infeasibility of the MILP problem may have beendetected during presolve")


if __name__ == '__main__':
    MODEL, TIME, DWELLING_CONSUMPTION, GRID_PRODUCTION_A_, GRID_PRODUCTION_B, ELEC_NODE = main()
    print_results()
