#! usr/bin/env python3
#  -*- coding: utf-8 -*-

"""

    The example can be found online at :

        https://cocalc.com/share/f34239ec-d317-420c-9e32-050b10689810/example
        \_BS.ipynb?viewer=share

        A scientific article presenting OMEGAlpes with the PV_self_consumption
    example is available here:

        https://hal.archives-ouvertes.fr/hal-02285954v1

..
    Copyright 2018 G2Elab / MAGE

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from various_examples.beginner_examples \
    .PV_self_consumption.utils.PV_self_consumption_utils \
    import *
import time as pytime
import os

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"

# Install corresponding omegalpes version
os.system('pip install omegalpes==' + __version__)

# -----------------------------------------------------------------------------

from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import VariableConsumptionUnit, \
    FixedConsumptionUnit, \
    ShiftableConsumptionUnit
from omegalpes.energy.units.conversion_units import \
    ElectricalToThermalConversionUnit
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.general.optimisation.elements import DynamicConstraint, \
    ActorDynamicConstraint
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.time import TimeUnit

# -----------------------------------------------------------------------------

def main(pmax_heater):
    ticks = pytime.time()
    # STEP 1: Set the time periods
    # 5 minutes time step: dt=1/12 hours
    # Daily study: periods = 24 hours (24*12 time steps)
    time = TimeUnit(periods=24 * 12, dt=1 / 12)

    # STEP 2: Create an empty optimization model
    model = OptimisationModel(time=time, name='example')

    # STEP 3: Create the household consumption units
    # Import the domestic hot water load profile
    dhw_load = import_domestic_hot_water_load_profile()
    # Import the lothes washing machine and dryer load profiles
    cl_wash_load, cl_dry_load = import_clothes_washer_and_dryer_load_profiles()

    # Create the consumption models
    dhw = FixedConsumptionUnit(time, 'dhw', energy_type='Thermal', p=dhw_load)

    clothes_washer = ShiftableConsumptionUnit(time, name='clothes_washer',
                                              power_values=cl_wash_load,
                                              energy_type='Electrical')

    clothes_washer._add_switch_off()

    clothes_dryer = ShiftableConsumptionUnit(time, name='clothes_dryer',
                                             power_values=cl_dry_load,
                                             energy_type='Electrical')

    # STEP 4: Create the PV panels and the grid imports and exports
    #  Import the electrical production from PV panels
    PV_prod_profile = import_PV_profile_5_min()

    # Create the PV production model
    PV_prod = FixedProductionUnit(time, name='PV_prod', p=PV_prod_profile)

    # Create the electrical grid imports as a production
    elec_grid_imports = VariableProductionUnit(time, 'elec_grid_imports',
                                               energy_type='Electrical')

    # Create the electrical grid exports as a consumption
    elec_grid_exports = VariableConsumptionUnit(time, 'elec_grid_exports',
                                                energy_type='Electrical')

    # STEP 5: Avoid importing and exporting at the same time
    # Create the dynamic constraint
    imp_exp = DynamicConstraint(name='imp_exp',
                                exp_t='elec_grid_imports_u[t] + '
                                      'elec_grid_exports_u[t] <= 1.5',
                                parent=elec_grid_imports)
    # Add the constraint to the elec_grid_imports model
    setattr(elec_grid_imports, 'imp_exp', imp_exp)

    # STEP 6: Create the water heater and the water tank
    water_heater = ElectricalToThermalConversionUnit(time, 'water_heater',
                                                     elec_to_therm_ratio=0.9,
                                                     pmax_in_elec=pmax_heater)

    # Define the tank capacity (6000 kWh)
    tank_capacity = 6000

    # Define a minimal state of charge for the storage (20%)
    SoC_min = 0.2

    # Define losses with a self-discharge parameter (5% per hour)
    self_disch = 0.05 * time.DT

    # Define if the state of charge of the storage has to be regained at the
    # end of the time period (yes)
    is_cycling = True

    water_tank = StorageUnit(time, 'water_tank', capacity=tank_capacity,
                             energy_type='Thermal', ef_is_e0=is_cycling,
                             self_disch=self_disch, soc_min=SoC_min)

    # STEP 7: Avoid that the dryer starts before the end of the washing
    # machine cycle
    # Create the constraint
    wait_to_dry = ActorDynamicConstraint(name='wait_to_dry',
                                         exp_t='clothes_dryer_start_up[t] <= '
                                         'lpSum(clothes_washer_switch_off[k] '
                                         'for k in range(0, t))',
                                         t_range='for t in time.I',
                                         parent=clothes_dryer)
    # Add the constraint to the dryer model
    setattr(clothes_dryer, 'wait_to_dry', wait_to_dry)

    # STEP 8: Define the objective
    # Maximizing the self-consumption means minimizing the imports from the
    # elctrical grid
    elec_grid_imports.minimize_production()

    # STEP 9: Create the energy nodes
    heat_node = EnergyNode(time, 'heat__node', energy_type='Thermal')
    heat_node.connect_units(dhw, water_tank,
                            water_heater.thermal_production_unit)

    elec_node = EnergyNode(time, 'elec_node', energy_type='Electrical')
    elec_node.connect_units(clothes_washer, clothes_dryer, elec_grid_imports,
                            elec_grid_exports, PV_prod,
                            water_heater.elec_consumption_unit)

    model.add_nodes(elec_node, heat_node)

    new_time = pytime.time()
    print('duration =', new_time - ticks)

    # STEP 10: Launch the optimization
    model.solve_and_update()

    return elec_node, heat_node


def print_results(nodes):
    """
        This function print the optimisation results
    """

    plot_node_energetic_flows(nodes[0])
    plot_node_energetic_flows(nodes[1])
    plt.show()


if __name__ == '__main__':
    # Maximal power that can 0be delivered by the production unit
    PRODUCTION_P_MAX = 15

    # Storage maximal charging and discharging powers
    (STORAGE_PC_MAX, STORAGE_PD_MAX) = 20, 20

    # Run main
    nodes = main(pmax_heater=6000)

    # Show results
    print_results(nodes)

    # Save energy flows into a CSV file
    save_energy_flows(nodes[0],
                      file_name=os.getcwd() + r'\results\study_case_BS_1')

    save_energy_flows(nodes[1],
                      file_name=os.getcwd() + r'\results\study_case_BS_2')
