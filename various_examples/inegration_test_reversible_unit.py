#! usr/bin/env python3
#  coding=utf-8 #


"""
    Testing module for reversible units
"""
import os
import unittest
from pulp import LpStatus, GUROBI_CMD
from omegalpes.energy.energy_nodes import EnergyNode
from omegalpes.energy.units.consumption_units import FixedConsumptionUnit, \
    VariableConsumptionUnit
from omegalpes.energy.units.production_units import FixedProductionUnit, \
    VariableProductionUnit
from omegalpes.energy.units.storage_units import StorageUnit
from omegalpes.energy.units.conversion_units import ElectricalConversionUnit
from omegalpes.energy.units.reversible_units import ReversibleUnit
from omegalpes.general.optimisation.model import OptimisationModel
from omegalpes.general.utils.plots import plt, plot_quantity, \
    plot_node_energetic_flows
from omegalpes.general.utils.output_data import save_energy_flows
from omegalpes.general.time import TimeUnit
from omegalpes.general.optimisation.elements import DynamicConstraint, \
    HourlyDynamicConstraint

# -----------------------------------------------------------------------------

# Module version
__version_info__ = (0, 3, 0)
__version__ = ".".join(str(x) for x in __version_info__)

# Documentation strings format
__docformat__ = "restructuredtext en"


# -----------------------------------------------------------------------------
def main():

    # Creating an empty model
    time = TimeUnit(periods=4, dt=1)  # Study on a day (24h)+1h,
    model = OptimisationModel(name='test_rev', time=time)

    rev = ReversibleUnit(time,'rev')
    fixed_cons_rev = ReversibleUnit(time, 'fixed_cons_rev', p_cons=[0, 1, 0,
                                                                    0])

    fixed_prod_rev = ReversibleUnit(time, 'fixed_cons_rev', p_prod=[0, 0, 10,
                                                                    8])

    right_fixed_rev = ReversibleUnit(time, 'fixed_cons_rev', p_prod=[10, 4, 0,
                                                                     0],
                                     p_cons=[0, 0, 2, 3])

    cons = FixedConsumptionUnit(time, 'fcu', p=[0, 0, 4, 3])
    var_cons = VariableConsumptionUnit(time, 'vcu')

    prod = FixedProductionUnit(time, 'fpu', p=[2, 1, 0, 0])
    var_prod= VariableProductionUnit(time, 'vpu')

    node_prod=EnergyNode(time, 'nodeprod')
    node_cons=EnergyNode(time, 'nodecons')

    # # CASE 1: Variable rev unit
    # todo: relevant cases with power values and associated assertRaise
    # node_prod.connect_units(cons, rev.production_unit)
    # node_cons.connect_units(prod, rev.consumption_unit)

    # CASE 2: fixed_prod_rev
    # node_cons.connect_units(fixed_prod_rev.consumption_unit, prod)
    # node_prod.connect_units(fixed_prod_rev.production_unit, var_cons)

    # CASE 3: fixed_cons_rev
    node_cons.connect_units(fixed_cons_rev.consumption_unit, var_prod)
    node_prod.connect_units(fixed_cons_rev.production_unit, cons)

    # CASE 4: wrong_fixed_rev
    # node_prod.connect_units(wrong_fixed_rev.production_unit, var_cons)

    # CASE 5: right_fixed_rev

    model.add_nodes(node_cons, node_prod)

    model.solve_and_update()

    return model, time, rev, cons, prod, node_prod, node_cons


def print_results():
    """
        *** This function print the optimisation result
    """

    if LpStatus[MODEL.status] == 'Optimal':
        print("\n - - - - - OPTIMISATION RESULTS - - - - - ")
        plot_node_energetic_flows(NODE_CONS)
        plot_node_energetic_flows(NODE_PROD)
        plt.show()

    elif LpStatus[MODEL.status] == 'Infeasible':
        print("Sorry, the optimisation problem has no feasible solution !")

    elif LpStatus[MODEL.status] == 'Unbounded':
        print("The cost function of the optimisation problem is unbounded !")

    elif LpStatus[MODEL.status] == 'Undefined':
        print("Sorry, a feasible solution has not been found (but may exist). "
              "PuLP does not manage to interpret the solver's output, "
              "the infeasibility of the MILP problem may have been "
              "detected during presolve.")
    else:
        print("Sorry, the optimisation problem has not been solved.")


if __name__ == '__main__':
    MODEL, TIME, REV, CONS, PROD, NODE_PROD, NODE_CONS = main()
    print_results()



