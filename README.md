# OMEGAlpes Examples and Notebooks
This repository gathers OMEGAlpes examples and notebooks. Examples need 
OMEGAlpes installation while notebooks can directly be used (see below).


## OMEGAlpes examples

Have a look at [OMEGAlpes examples documentation](https://omegalpes-examples.readthedocs.io "OMEGAlpes examples documentation on readthedocs")

Please install OMEGAlpes Lib with pip using the command prompt before launching the examples.

If you are admin on Windows or working on a virtual environment
    pip install omegalpes

If you want a local installation or you are not admin
    pip install --user omegalpes

If you are admin on Linux:
    sudo pip install omegalpes
    
For more information on OMEGAlpes, see [OMEGAlpes documentation](https://omegalpes.readthedocs.io/en/latest/ "OMEGAlpes documentation on readthedocs")  
A scientific article presenting OMEGAlpes with a detailed example (PV_self_consumption) is 
also [available on HAL](https://hal.archives-ouvertes.fr/hal-02285954v1)

## OMEGAlpes Notebooks

Jupyter notebooks are associated to OMEGAlpes Examples, and can be used directly.

You can run OMEGAlpes notebooks:

A. remotely on Binder  
    1. Follow the Binder link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fomegalpes%2Fomegalpes_examples/master "Link to the OMEGAlpes examples repository through mybinder")  
    2. Click on the Notebooks folder   
    3. Select the Notebook you want (.ipynb) and run it  
    +. If necessary, install the relevant version of the dependencies, thanks to a specific available requirement 
    `import os`
    `os.system('pip install -r requirements.txt')` 
    
B. locally thanks to Jupyter Notebook in Anaconda
    1. Install [Anaconda]((https://www.anaconda.com/distribution/))
    2. Use the following command in Anaconda prompt: 
    `conda create --name worklab` 
    and then `activate worklab`
    3. To use a conda environment in Jupyter notebooks: `conda install -c anaconda ipykernel` 
    and then `python -m ipykernel install --user --name=worklab`
    4. Before launching the notebook, install the relevant requirements (with the versions used in your notebook): 
    `pip install -r requirements.txt`
    5. Launch the notebook with `jupyter notebook` in the correct folder. 
    Once the notebook launched, do not forget to select the anaconda Kernel 
    with the tab *Kernel-->Change Kernel-->conda_name*.

C. locally thanks to Jupyter Notebook in your local environment
    1. install Jupyter  
    2. Download the following folder: 
    https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples   
    3. Open the command prompt  
    4. Install OMEGAlpes (ex: on the command prompt type 'pip install omegalpes'
    or have a look at [the documentation](https://omegalpes.readthedocs.io/en/latest/installation_requirements.html "OMEGAlpes documentation - installation requirements")  
    5. On the command prompt select the folder of the OMEGAlpes Examples   
    6. On the command prompt type: python -m notebook  
    7. Click on the notebooks folder    
    8. Select the Notebook you want (.ipynb) and run it


**For more HELP or information for installation or use**, 
please have a look to
https://omegalpes-examples.readthedocs.io/en/latest/jupyter.html#

**Web interface**
If you which to create new energy project model, a web interface is also available at 
[omegalpes-web](https://mhi-srv.g2elab.grenoble-inp.fr/OMEGAlpes-web-front-end/) to help you generate scripts

